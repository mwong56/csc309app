JAVASRC=TestTool309/test/tt/model/professor/admin/AdminTest.java \
		TestTool309/test/tt/model/professor/create/GenerateTestsTest.java \
		TestTool309/test/tt/model/professor/grading/gradetestTest.java \
		TestTool309/test/tt/model/professor/questions/QuestionBankTest.java \
		TestTool309/test/tt/model/professor/questions/QuestionTest.java \
		TestTool309/test/tt/model/professor/ProfessorDBTest.java \
		TestTool309/test/tt/model/professor/ProfessorTest.java \
		TestTool309/test/tt/model/student/take/AvailableTestsTest.java \
		TestTool309/test/tt/model/student/take/TestTest.java \
		TestTool309/test/tt/model/student/StudentDBTest.java \
		TestTool309/test/tt/model/student/StudentTest.java \
target:

test:
	java -classpath TestTool309/libs/junit-4.12.jar org.junit.runner.JUnitCore ${JAVASRC}