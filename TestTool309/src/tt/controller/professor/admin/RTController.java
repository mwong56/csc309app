package tt.controller.professor.admin;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.professor.create.Test;
import tt.model.professor.create.TestDB;
import tt.controller.professor.create.TestDBController;
import tt.controller.professor.create.ReleaseTestDBController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tt.model.professor.Professor;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ButtonType;
import tt.model.Course;
import tt.model.professor.admin.ReleaseTestSettings;

/****
 *
 * Class RTController provides methods to change the displayed screen based on
 * the buttons clicked on the Release Test screen, as well as save the current
 * release test settings.
 *
 *
 * @author Ryan Lee (rlee50@calpoly.edu)
 *
 */

public class RTController{
    @FXML
    private Button releaseButton;
    @FXML
    private Stage primaryStage;
    @FXML
    private BorderPane rootLayout;
    @FXML
    private MainApp mainApp;
    @FXML
    private Test selectedTest;
    @FXML
    private Stage dialogStage;
    @FXML
    private ListView<String> releaseList;
    @FXML
    private ComboBox<String> combo;
    
    private TestDB testDB;
    private TestDB releaseTestDB;
    private ReleaseTestDBController releaseTestDBController;
    private TestDBController testDBControl;

    private final ObservableList<String> releaseListItems = FXCollections.observableArrayList();
    private final ObservableList<String> dummyList = FXCollections.observableArrayList();
    private ArrayList<Test> tests;
    private String selectedCourse;
    private ArrayList<Course> courses = new ArrayList<Course>();
    private ArrayList<String> courseNames = new ArrayList<String>();
     
    /**
     * 
     * setMainApp(MainApp) sets this.mainApp equal to the input mainApp.
     *
       pre: mainApp != NULL;
     *
       post: this.mainApp == mainApp;
     *
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * 
     * start() sets up data to be displayed/used on screen at load.
     *
     */
    public void start() {
        courses.addAll(mainApp.getProfessor().getDistinctCourses());
        for(int i = 0; i < courses.size(); i++) {
            courseNames.add(courses.get(i).getCourseAbbr() + "-" + courses.get(i).getNumber());
        }
 
        combo.getItems().clear();
        combo.getItems().addAll(courseNames);
        // Handle ComboBox event.
        combo.setOnAction((event) -> {            
            //used to hold the name of the selected test
            selectedCourse = combo.getSelectionModel().getSelectedItem();
            showTests();
        });
        
        tests = new ArrayList<Test>();
        selectedTest = new Test();
        //testDBControl = new TestDBController();
        testDB = TestDBController.getDBController().testDB;
        showTests();
        releaseList.setItems(releaseListItems);
    }

    /**
     * 
     * selectTest(String) changes the selected test name to be released with the
     * input string.
     *
       pre: test.length() > 0;
      
       post: selectedTest.getClassName() == test;
     *
     */
    public void selectTest(String test) {
        System.out.println("In selectTest");
        selectedTest.setTitle(test);
    }
    
    @FXML
    public void handleMouseClick(MouseEvent arg0) {
        String testName = releaseList.getSelectionModel().getSelectedItem();
        selectedTest.setTitle(testName);
    }

    /**
     * 
     * handleReleaseButtonAction(ActionEvent) releases the selected test to be
     * viewable to students
     *
       pre: selectedTest.getClassName() != null;
      
       post: releasedTestDB.contains(selectedTest);
     *
     */
    @FXML
    protected void handleReleaseButtonAction(
            ActionEvent event) {
        Alert alert;
        if (selectedTest.getTitle().get() == null) {
            alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText("No test selected");

            alert.showAndWait();
        }
        else if(ReleaseTestSettings.getRTS().getType() == "") {
            alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText("No default release test settings");

            alert.showAndWait();
        }
        else {
            alert = new Alert(AlertType.CONFIRMATION);
            alert.initOwner(dialogStage);
            alert.setTitle("Release Test");
            alert.setHeaderText("Release Test");
            alert.setContentText("Are you sure you want to release test: " + selectedTest.getTitle().get() + "?");
            
            ButtonType yesButton = new ButtonType("Yes");
            ButtonType noButton = new ButtonType("No");
            
            alert.getButtonTypes().setAll(noButton, yesButton);
            
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == yesButton) {
                selectedTest.setRTS(ReleaseTestSettings.getRTS());
                releaseTestDB = ReleaseTestDBController.getDBController().testDB;
                releaseTestDB.addTest(selectedTest);
                //releaseTestDBController.setTestDB(releaseTestDB);
            }
            else {
                dialogStage.close();
            }
        }
    }

    /**
     *
     * handleRTButtonAction(ActionEvent) changes the screen to the Release Test
     * screen when the event is triggered.
     *
       pre: mainApp != NULL;
     
       post: mainApp.getPrimaryStage() == "view/AdminReleaseTest.fxml";
     *
     */
    @FXML
    protected void handleRTButtonAction(
            ActionEvent event) throws IOException {
        mainApp.showReleaseTest();
    }

    /**
     * 
     * handleDTSButtonAction(ActionEvent) changes the screen to the Default
     * Release Test Settings screen when the event is triggered.
     *
       pre: mainApp != NULL;
      
       post: mainApp.getPrimaryStage() == "view/AdminDefaultSettings.fxml";
     *
     */
    @FXML
    protected void handleDTSButtonAction(
            ActionEvent event) throws IOException {
        mainApp.showDefaultSettings();
    }
    
    @FXML
    protected void showTests() {
        releaseListItems.setAll(dummyList);
        testDB = TestDBController.getDBController().testDB;
        if(testDB != null) {
            int numTests = testDB.getSize();
            int ndx = 0;
            if(tests.isEmpty() != true)
                tests.clear();
            if(testDB.getTestDB().isEmpty() != true) {
                tests.addAll(testDB.getTestDB());
                while(ndx < numTests) {
                    if(tests.get(ndx).getCourse().get().equals(selectedCourse))
                        releaseListItems.add(tests.get(ndx).getTitle().get());
                    ndx++;
                }
            }
        }
    }
}