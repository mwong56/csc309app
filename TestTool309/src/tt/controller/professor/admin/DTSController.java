package tt.controller.professor.admin;

import java.io.IOException;
import java.util.Optional;

import tt.MainApp;
import tt.controller.professor.create.ReleaseTestDBController;
import tt.model.professor.admin.Admin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import tt.model.professor.admin.ReleaseTestSettings;

/****
 *
 * Class DTSController provides methods to change the displayed screen based on
 * the buttons clicked on the Default Release Test Settings screen, as well as
 * save the default release test settings.
 *
 *
 * @author Ryan Lee (rlee50@calpoly.edu)
 *
 */

public class DTSController {
    @FXML
    private Text actiontarget;
    @FXML
    private Stage primaryStage;
    @FXML
    private BorderPane rootLayout;
    @FXML
    private MainApp mainApp;
    @FXML
    private Admin admin;
    @FXML
    private Stage dialogStage;
    @FXML
    private ComboBox<String> combo; // Value injected by FXMLLoader
    @FXML
    private CheckBox closed;
    @FXML
    private CheckBox open;
    @FXML
    private TextField tries;
    @FXML
    private TextField time;
    
    private ReleaseTestSettings rts = new ReleaseTestSettings();

    /**
     * 
     * setMainApp(MainApp) sets this.mainApp equal to the input mainApp.
     *
       pre: mainApp != NULL;
     
       post: this.mainApp == mainApp;
     *
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * 
     * handleDTSButtonAction(ActionEvent) changes the screen to the Default
     * Release Test Settings screen when the event is triggered.
     *
       pre: mainApp != NULL;
     
       post: mainApp.getPrimaryStage() == "view/AdminDefaultSettings.fxml";
     *
     */
    @FXML
    protected void handleDTSButtonAction(
            ActionEvent event) throws IOException {
        mainApp.showDefaultSettings();
    }

    /**
     *
     * handleRTButtonAction(ActionEvent) changes the screen to the Release Test
     * screen when the event is triggered.
     *
       pre: mainApp != NULL;
     
       post: mainApp.getPrimaryStage() == "view/AdminReleaseTest.fxml";
     *
     */
    @FXML
    protected void handleRTButtonAction(
            ActionEvent event) throws IOException {
        mainApp.showReleaseTest();
    }

    /**
     * 
     * initialize() sets up data to be displayed/used on screen at load, as well
     * as actions available on current screen.
     *
     */
    @FXML
    // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert combo != null : "fx:id=\"combo\" was not injected: check your FXML file 'AdminDefaultSettings.fxml'.";

       // Initialize your logic here: all @FXML variables will have been injected
       combo.getItems().clear();
       combo.getItems().addAll("In Class","Take Home","Practice");
       
       admin = new Admin("In Class");
       
    // Handle ComboBox event.
       combo.setOnAction((event) -> {
    	   //used to tell what type of test is selected
    	   int typeNum;
    	   
    	   //used to hold the name of the selected test 
    	   String selectedItem = combo.getSelectionModel().getSelectedItem();
           admin.setType(selectedItem);
           typeNum = admin.typeClicked();
           
           //depending on test type, show corresponding screen
           if(typeNum == 0)
        	   mainApp.showInClass();
           else if(typeNum == 1)
        	   mainApp.showPractice();
           else
        	   mainApp.showTakeHome();
       });
   }
    
    /**
     *
     * handleSaveButton(ActionEvent) sets the default settings for released
     * tests.
     *
       pre: admin != NULL;
     
       post: 
     *
     */
    @FXML
    protected void handleSaveButton(
            ActionEvent event) throws IOException {
//        admin.saveSettings();
        String message = "";
        Alert alert;
        admin.setType(combo.getPromptText());
        rts.setType(admin.getType());
        if(open.isSelected() != true && closed.isSelected() != true)
            message += "No test type selected\n";
        else
            rts.setClosed(closed.isSelected());
        if(admin.getType().equals("Practice") || admin.getType().equals("Take Home")) {
            if(tries.getText().equals("") != true)
                try {
                    rts.setNumTries(Integer.parseInt(tries.getText()));
                }
                catch(NumberFormatException e) {
                    message += "Num tries is not a number\n";
                }
            else
                message += "No num tries provided\n";
        }
        if(admin.getType().equals("Take Home")) {
            if(time.getText().equals("") != true)
                try {
                    rts.setTimeLimit(Integer.parseInt(time.getText()));
                }
                catch(NumberFormatException e) {
                    message += "Time limit is not a number\n";
                }
            else
                message += "No time limit provided\n";
        }
        if(message.equals("")) {
            alert = new Alert(AlertType.CONFIRMATION);
            alert.initOwner(dialogStage);
            alert.setTitle("Default Release Test Settings");
            alert.setHeaderText("Default Release Test Settings");
            alert.setContentText("Are you sure you want to save settings?");
            
            ButtonType yesButton = new ButtonType("Yes");
            ButtonType noButton = new ButtonType("No");
            
            alert.getButtonTypes().setAll(noButton, yesButton);
            
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == yesButton) {
                ReleaseTestSettings.setRTS(rts);
            }
//            else {
//                dialogStage.close();
//            }
        }
        else {
            alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(message);
            
            alert.showAndWait();
        }
    }
    
    @FXML
    protected void handleClosedBook(
            ActionEvent event) throws IOException {
        if(open.isSelected())
            open.setSelected(false);
    }
    
    @FXML
    protected void handleOpenBook(
            ActionEvent event) throws IOException {
        if(closed.isSelected())
            closed.setSelected(false);
    }    
}