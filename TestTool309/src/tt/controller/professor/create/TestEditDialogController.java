package tt.controller.professor.create;

import java.util.ArrayList;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;
import tt.MainApp;
import tt.model.Course;
import tt.model.professor.Professor;
import tt.model.professor.create.Test;
import tt.model.professor.questions.Question;

/**
 * Dialog to edit details of a test.
 * 
 * @author Terrence Li
 */
public class TestEditDialogController {

    @FXML
    private TextField testNameField;
    @FXML
    private TableView<Question> questionTable;
    @FXML
    private TableColumn<Question, Number> numberColumn;
    @FXML
    private TableColumn<Question, Number> difficultyColumn;
    @FXML
    private TableColumn<Question, String> typeColumn;
    @FXML
    private TableColumn<Question, String> questionColumn;
    
    @FXML
    private ComboBox<String> combo;

    private Stage dialogStage;
    private Test test;
    private boolean okClicked = false;
    private MainApp mainApp;
    private String selectedCourse;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        numberColumn.setCellValueFactory(new Callback<CellDataFeatures<Question, Number>, ObservableValue<Number>>() {
          @Override public ObservableValue<Number> call(CellDataFeatures<Question, Number> p) {
            return new ReadOnlyObjectWrapper(questionTable.getItems().indexOf(p.getValue())+1 + "");
          }
        });   
        numberColumn.setSortable(false);
        
        questionColumn.setCellValueFactory(cellData ->
        cellData.getValue().promptProperty());
        
        difficultyColumn.setCellValueFactory(cellData ->
        cellData.getValue().difficultyProperty());
        
        typeColumn.setCellValueFactory(cellData ->
        cellData.getValue().typeProperty());
        
        combo.getItems().clear();
        //combo.getItems().addAll(courseNames);
        // Handle ComboBox event.
        combo.setOnAction((event) -> {            
            //used to hold the name of the selected test
            selectedCourse = combo.getSelectionModel().getSelectedItem();
        });
        
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        
        ArrayList<String> courseNames = new ArrayList<String>();
        ArrayList<Course> courses = mainApp.getProfessor().getDistinctCourses();
        Course currCourse;
        for(int i = 0; i < courses.size(); i++) {
            currCourse = courses.get(i);
            courseNames.add(currCourse.getCourseAbbr() + "-" + currCourse.getNumber());
        }
        
        ObservableList<String> classes = FXCollections.observableArrayList(courseNames);
        combo.setItems(classes);
    }

    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     * 
                 <pre>
            pre:
                //
                // dialogStage isn't null
                //
                dialogStage != null;
                
            post:
                //
                // this.dialogStage' isn't null and equals dialogStage
                //
                this.dialogStage' != null
                
                    &&
      this.dialogStage'.equals(dialogStage);
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the test to be edited in the dialog.
     * 
     * @param test
     * 
                 <pre>
            pre:
                //
                // qb isn't null
                //
                qb != null;
                
            post:
                //
                // this.questionBank' isn't null and equals qb
                //
                this.test' != null
                
                    &&
      this.test'.equals(test);
     */
    public void setTest(Test t) {
        this.test = t;
        
        combo.setValue(test
                .getCourse().getValue());
        testNameField.setText(test.getTitle().getValue());
        
        questionTable.setItems(test.getQuestionList());
    }
    
    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
      
              <pre>
         pre:
                 
         post:
      okClicked != null;
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * 
      <pre>
         pre:
             isInputValid() == true;
                 
         post:
      okClicked == true;
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            test.setCourse(selectedCourse);
            test.setTitle(testNameField
                    .getText());

            okClicked = true;
            dialogStage.close();
        }
    }
    
    @FXML
    private void handleAddQuestion() {
        Question q = mainApp.showQuestionBankDialogController();
        if (q != null) {
            test.addQuestion(q);
        }
    }
    
    @FXML
    private void handleDeleteQuestion() {
        Question selectedQuestion = questionTable
              .getSelectionModel()
              .getSelectedItem();
        if (selectedQuestion != null) {
            questionTable.getItems().remove(selectedQuestion);
        }
        else {
          // Nothing selected.
          Alert alert = new Alert(
                  AlertType.WARNING);
          alert.initOwner(mainApp
                  .getPrimaryStage());
          alert.setTitle("No Selection");
          alert.setHeaderText("No question Selected");
          alert.setContentText("Please select a question in the table.");
        
          alert.showAndWait();
        }
    }
    
    @FXML
    private void handleViewQuestion() {
//        Question selectedQuestion = questionTable
//                .getSelectionModel()
//                .getSelectedItem();
//        if (selectedQuestion != null) {
//            boolean okClicked = mainApp
//                    .showTestEditDialog(selectedQuestion);
//            if (okClicked) {
//                showTestDetails(selectedQuestion);
//            }
//
//        }
//        else {
//            // Nothing selected.
//            Alert alert = new Alert(
//                    AlertType.WARNING);
//            alert.initOwner(mainApp
//                    .getPrimaryStage());
//            alert.setTitle("No Selection");
//            alert.setHeaderText("No Test Selected");
//            alert.setContentText("Please select a test in the table.");
//
//            alert.showAndWait();
//        }
    }

    /**
     * Called when the user clicks cancel.
     *
      <pre>
           pre:
               //
                // dialogStage isn't null
                //
      dialogStage != null;
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     * 
              <pre>
            pre:
                //
                // classNameField isn't null and empty.
                //
                classNameField != null && classNameField.length() > 0;
                
                &&
      testNameField != null && classNameField.length() > 0;
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (testNameField.getText() == null
                || testNameField.getText()
                        .length() == 0) {
            errorMessage += "No valid test name!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        }
        else {
            // Show the error message.
            Alert alert = new Alert(
                    AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}