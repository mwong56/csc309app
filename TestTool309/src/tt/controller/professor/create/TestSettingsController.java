package tt.controller.professor.create;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.professor.create.Test;
import tt.model.professor.create.TestSettings;

/**
 * The overview for the test settings
 * 
 * @author Terrence Li
 *
 */
public class TestSettingsController {
    
    @FXML
    private TextField avgDiffField;
    @FXML
    private TextField diffRangeField;
    @FXML
    private TextField numMCField;
    @FXML
    private TextField numTFField;
    @FXML
    private TextField numSAField;
    @FXML
    private TextField numCodingField;
    @FXML
    private TextField numLAField;
    @FXML
    private TextField numMRField;
    @FXML
    private TextField numMField;
    @FXML
    private TextField numFBField;
    
    @FXML
    private TextField DavgDiffField;
    @FXML
    private TextField DdiffRangeField;
    @FXML
    private TextField DnumMCField;
    @FXML
    private TextField DnumTFField;
    @FXML
    private TextField DnumSAField;
    @FXML
    private TextField DnumCodingField;
    @FXML
    private TextField DnumLAField;
    @FXML
    private TextField DnumMRField;
    @FXML
    private TextField DnumMField;
    @FXML
    private TextField DnumFBField;
    @FXML
    private CheckBox dSettings;
    
    public static boolean checked = false;
    
    private TestSettings ts;
    private TestSettings dts;
    private Stage dialogStage;
    private MainApp mainApp;

    /**
     * The constructor. The constructor is called before the initialize()
     * method.
     */
    public TestSettingsController() {
        avgDiffField = new TextField();
        diffRangeField = new TextField();
        numMCField = new TextField();
        numTFField = new TextField();
        numSAField = new TextField();
        numCodingField = new TextField();
        numLAField = new TextField();
        numMRField = new TextField();
        numMField = new TextField();
        numFBField = new TextField();
        
        DavgDiffField = new TextField();
        DdiffRangeField = new TextField();
        DnumMCField = new TextField();
        DnumTFField = new TextField();
        DnumSAField = new TextField();
        DnumCodingField = new TextField();
        DnumLAField = new TextField();
        DnumMRField = new TextField();
        DnumMField = new TextField();
        DnumFBField = new TextField();
        
        dSettings = new CheckBox();
    }
    
    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     * 
                 <pre>
            pre:
                //
                // dialogStage isn't null
                //
                dialogStage != null;
                
            post:
                //
                // this.dialogStage' isn't null and equals dialogStage
                //
                this.dialogStage' != null
                
                    &&
      this.dialogStage'.equals(dialogStage);
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        dSettings.setSelected(checked);
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    /**
     * Called when the user clicks ok.
     * 
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            setFields();
            
            this.mainApp.showCreateOverview();
            
        }
    }
    
    /**
     * called when user selects default settings
     */
    @FXML
    public void checkDSettings() {
        checked = !checked;
        dSettings.setSelected(checked);
    }
    
    private void setFields() {
        this.ts.setAvgDiff(Integer.parseInt(avgDiffField.getText()));
        this.ts.setDiffRange(Integer.parseInt(diffRangeField.getText()));
        this.ts.setNumMC(Double.parseDouble(numMCField.getText()));
        this.ts.setNumTF(Double.parseDouble(numTFField.getText()));
        this.ts.setNumSA(Double.parseDouble(numSAField.getText()));
        this.ts.setNumCoding(Double.parseDouble(numCodingField.getText()));
        this.ts.setNumLA(Double.parseDouble(numLAField.getText()));
        this.ts.setNumMR(Double.parseDouble(numMRField.getText()));
        this.ts.setNumM(Double.parseDouble(numMField.getText()));
        this.ts.setNumFB(Double.parseDouble(numFBField.getText()));
        
        this.dts.setAvgDiff(Integer.parseInt(DavgDiffField.getText()));
        this.dts.setDiffRange(Integer.parseInt(DdiffRangeField.getText()));
        this.dts.setNumMC(Double.parseDouble(DnumMCField.getText()));
        this.dts.setNumTF(Double.parseDouble(DnumTFField.getText()));
        this.dts.setNumSA(Double.parseDouble(DnumSAField.getText()));
        this.dts.setNumCoding(Double.parseDouble(DnumCodingField.getText()));
        this.dts.setNumLA(Double.parseDouble(DnumLAField.getText()));
        this.dts.setNumMR(Double.parseDouble(DnumMRField.getText()));
        this.dts.setNumM(Double.parseDouble(DnumMField.getText()));
        this.dts.setNumFB(Double.parseDouble(DnumFBField.getText()));
    }
    
    /**
     * Sets the settings initially
     * @param ts
     * @param dts
     */
    public void setSettings(TestSettings ts, TestSettings dts) {
        this.ts = ts;
        this.dts = dts;

        avgDiffField.setText(ts.getAvgDiff().toString());
        diffRangeField.setText(ts.getDiffRange().toString());
        numMCField.setText(ts.getNumMC().toString());
        numTFField.setText(ts.getNumTF().toString());
        numSAField.setText(ts.getNumSA().toString());
        numCodingField.setText(ts.getNumCoding().toString());
        numLAField.setText(ts.getNumLA().toString());
        numMRField.setText(ts.getNumMR().toString());
        numMField.setText(ts.getNumM().toString());
        numFBField.setText(ts.getNumFB().toString());
        
        DavgDiffField.setText(dts.getAvgDiff().toString());
        DdiffRangeField.setText(dts.getDiffRange().toString());
        DnumMCField.setText(dts.getNumMC().toString());
        DnumTFField.setText(dts.getNumTF().toString());
        DnumSAField.setText(dts.getNumSA().toString());
        DnumCodingField.setText(dts.getNumCoding().toString());
        DnumLAField.setText(dts.getNumLA().toString());
        DnumMRField.setText(dts.getNumMR().toString());
        DnumMField.setText(dts.getNumM().toString());
        DnumFBField.setText(dts.getNumFB().toString());
        
        setFields();
    }
    
    public TestSettings returnTS() {
        return ts;
    }
    
    public TestSettings returnDTS() {
        return dts;
    }
    
    /**
     * Validates the user input in the text fields.
     * 
     * @return true if the input is valid
     * 
              <pre>
            pre:
                //
                // classNameField isn't null and empty.
                //
                classNameField != null && classNameField.length() > 0;
                
                &&
      testNameField != null && classNameField.length() > 0;
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (avgDiffField.getText() == null
                || avgDiffField.getText()
                        .length() == 0) {
            errorMessage += "No average field!\n";
        }
        if (diffRangeField.getText() == null
                || diffRangeField.getText()
                        .length() == 0) {
            errorMessage += "No difficulty range!\n";
        }
        if (numMCField.getText() == null
                || numMCField.getText()
                        .length() == 0) {
            errorMessage += "No multiple choice field!\n";
        }
        if (numTFField.getText() == null
                || numTFField.getText()
                        .length() == 0) {
            errorMessage += "No true or false field!\n";
        }
        if (numSAField.getText() == null
                || numSAField.getText()
                        .length() == 0) {
            errorMessage += "No short answer field!\n";
        }
        if (numCodingField.getText() == null
                || numCodingField.getText()
                        .length() == 0) {
            errorMessage += "No coding field!\n";
        }
        if (numCodingField.getText() == null
                || numCodingField.getText()
                        .length() == 0) {
            errorMessage += "No coding field!\n";
        }
        if (numLAField.getText() == null
                || numLAField.getText()
                        .length() == 0) {
            errorMessage += "No long answer field!\n";
        }
        if (numMRField.getText() == null
                || numMRField.getText()
                        .length() == 0) {
            errorMessage += "No multiple response field!\n";
        }
        if (numMField.getText() == null
                || numMField.getText()
                        .length() == 0) {
            errorMessage += "No matching field!\n";
        }
        if (numFBField.getText() == null
                || numCodingField.getText()
                        .length() == 0) {
            errorMessage += "No fill in the blank field!\n";
        }
        if ((Double.parseDouble(numMCField.getText()) + Double.parseDouble(numTFField.getText())
                + Double.parseDouble(numSAField.getText()) + Double.parseDouble(numCodingField.getText())) 
                + Double.parseDouble(numLAField.getText()) + Double.parseDouble(numMRField.getText())
                + Double.parseDouble(numMField.getText()) + Double.parseDouble(numFBField.getText()) > 100.0) {
            errorMessage += "Can't go over 100%!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        }
        else {
            // Show the error message.
            Alert alert = new Alert(
                    AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
