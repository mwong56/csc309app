package tt.controller.professor.create;

import tt.model.professor.create.TestDB;

/**
 * 
 * @author Ryan Lee
 *
 */

public class TestDBController {
    public TestDB testDB;
    
    private static TestDBController controller = null;
    
    public TestDBController() {
        if(testDB == null)
            testDB = new TestDB();
    }
    
    public static TestDBController getDBController() {
        if(controller == null)
            controller = new TestDBController();
        return controller;
    }
    
    public TestDB getTestDB() {
        if(testDB == null)
            testDB = new TestDB();
        return testDB;
    }
    
    public void setTestDB(TestDB testDBParam) {
        testDB.setTestDB(testDBParam.getTestDB());
    }
}