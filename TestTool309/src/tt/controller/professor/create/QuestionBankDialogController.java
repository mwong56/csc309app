package tt.controller.professor.create;

import tt.MainApp;
import tt.model.professor.questions.Question;
import tt.model.professor.questions.QuestionBank;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * QuestionBank controller dialog class
 * 
 * @author Terrence Li
 */
public class QuestionBankDialogController {

    /** A QuestionBank table */
    @FXML
    private TableView<Question> QuestionBankTable;

    /** A QuestionBank column called type */
    @FXML
    private TableColumn<Question, String> typeColumn;

    /** A QuestionBank column called question */
    @FXML
    private TableColumn<Question, String> questionPromptColumn;
    
    @FXML
    private TableColumn<Question, Number> difficultyPromptColumn;

    /** Reference to the main application. */
    private MainApp mainApp;
    private Stage dialogStage;
    public Question addQuestion;

    /** Reference to question bank */
    private QuestionBank questionBank;
    
    public static boolean addedQ = false;

    /**
     * Constructor
     */
    public QuestionBankDialogController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the question table with the three columns.
        typeColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .typeProperty());
        questionPromptColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .promptProperty());
        difficultyPromptColumn
        .setCellValueFactory(cellData -> cellData
                .getValue()
                .difficultyProperty());        

        // Listen for selection changes and show the person details when
        // changed.
        // personTable.getSelectionModel().selectedItemProperty().addListener(
        // (observable, oldValue, newValue) -> showPersonDetails(newValue));
    }
    
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp and questionBank isn't null
                //
                this.mainApp != null
                
                    &&
                
                this.questionBank != null;
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        this.questionBank = mainApp
                .getQuestionBank();
        addedQ = false;

        // Add observable list data to the table
        QuestionBankTable.setItems(questionBank
                .getQuestionData());
    }

    /**
     * Indicates that the main app should now show the Add Question dialog.
     * 
      <pre>
           post:
                //
                // The question is added iff okClicked
                //
                if (okClicked)
      questionBank'.contains(q);
     */
    @FXML
    private void handleAdd() {
        int selectedIndex = QuestionBankTable
                .getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            addQuestion = questionBank.get(selectedIndex);
            addedQ = true;
            dialogStage.close();
        }
        else {
            // Nothing selected.
            Alert alert = new Alert(
                    AlertType.WARNING);
            alert.initOwner(mainApp
                    .getPrimaryStage());

            if (questionBank.getQuestionData()
                    .size() > 0) {
                alert.setTitle("No Selection");
                alert.setHeaderText("No Question Selected");
                alert.setContentText("Please select a question in the table to add.");
            }
            else {
                alert.setTitle("No Questions");
                alert.setHeaderText("No Questions in Question Bank");
                alert.setContentText("Please create a question to add.");
            }
            alert.showAndWait();
        }
    }

    /**
     * Deletes the selected question from QuestionBankDB. The given question
     * must already be in the input db.
     * 
      <pre>
            pre:
                  //
                  // The question is contained in QuestionBankDB
                  //
                  questionBank.contains(q);
          
            post:
                  //
                  // A question is in the output data iff it isn't the 
                  // existing question to be deleted and it is in the 
                  // input data
                  //
                  forall ( Question q_other ;
                  questionBank'.contains(ur_other) iff
      !q_other.equals(q) && data.contains(q_other));
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
}
