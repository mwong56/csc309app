package tt.controller.professor.create;

//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import tt.MainApp;
import tt.model.professor.create.Test;
import tt.util.DateUtil;
import tt.model.professor.create.TestDB;
import tt.controller.professor.create.TestDBController;

/**
 * The overview for created tests when hitting "History"
 * 
 * @author Terrence Li
 *
 */
public class TestOverviewController {
    @FXML
    private TableView<Test> testTable;
    @FXML
    private TableColumn<Test, String> classNameColumn;
    @FXML
    private TableColumn<Test, String> testNameColumn;

    @FXML
    private Label classNameLabel;
    @FXML
    private Label testNameLabel;
    @FXML
    private Label dateCreatedLabel;
    @FXML
    private Label questionNumberLabel;
    
    private TestDB testDB;
    private TestDBController testDBControl;

    // Reference to the main application.
    private MainApp mainApp;

    /**
     * The constructor. The constructor is called before the initialize()
     * method.
     */
    public TestOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        classNameColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .getCourse());
        testNameColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .getTitle());
        
        testDBControl = new TestDBController();
        testDB = TestDBController.getDBController().testDB;

    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     * 
                 <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                //
                // this.mainApp' isn't null and equals mainApp
                //
                this.mainApp' != null
                
                    &&
      this.mainApp'.equals(mainApp);
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
        testTable.setItems(mainApp.getTestData());
    }

    /**
     * Fills all text fields to show details about the person. If the specified
     * person is null, all text fields are cleared.
     * 
     * @param person
     *            the person or null
     * 
                 <pre>
            pre: 
                test != null;
                
            post:
                classNameLabel != null;
                    
                    &&
                    
                testNameLabel != null;
                
                    &&
                    
                questionNumberLabel != null;
                
                    &&
      dateCreatedLabel != null;
     */
    private void showTestDetails(Test test) {
        if (test != null) {
            // Fill the labels with info from the person object.
            classNameLabel.setText(test
                    .getCourse().get());
            testNameLabel.setText(test
                    .getTitle().get());
            questionNumberLabel
                    .setText(Integer.toString(test
                            .getQuestionNumber()));
            dateCreatedLabel
                    .setText(DateUtil.format(test
                            .getDateCreated()));
        }
        else {
            // Person is null, remove all the text.
            classNameLabel.setText("");
            testNameLabel.setText("");
            questionNumberLabel.setText("");
            dateCreatedLabel.setText("");
        }
    }

    /**
     * Called when the user clicks on the delete button. pre:
     * 
      post:
     */
    @FXML
    private void handleDeleteTest() {
        int selectedIndex = testTable
                .getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            testDB = TestDBController.getDBController().testDB;
            testDB.removeTest(testTable.getItems().get(selectedIndex));
            testDBControl.setTestDB(testDB);
            testTable.getItems().remove(
                    selectedIndex);
        }
        else {
            // Nothing selected.
            Alert alert = new Alert(
                    AlertType.WARNING);
            alert.initOwner(mainApp
                    .getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Test Selected");
            alert.setContentText("Please select a test in the table.");

            alert.showAndWait();
        }
    }

    /**
     * Called when the user clicks the new button. Opens a dialog to edit
     * details for a new test.
     * 
      <pre>
           pre:
                   mainApp != null;
      post:
     */
    @FXML
    private void handleNewTest() {
        Test tempTest = new Test();
        boolean okClicked = mainApp
                .showTestEditDialog(tempTest);
        if (okClicked) {
            mainApp.getTestData().add(tempTest);
            testDB = TestDBController.getDBController().testDB;
            testDB.addTest(tempTest);
            testDBControl.setTestDB(testDB);
        }
    }

    /**
     * Called when the user clicks the edit button. Opens a dialog to edit
     * details for the selected person.
     * 
      <pre>
           pre:
                   mainApp != null;
      post:
     */
    @FXML
    private void handleEditTest() {
        Test selectedTest = testTable
                .getSelectionModel()
                .getSelectedItem();
        if (selectedTest != null) {
            boolean okClicked = mainApp
                    .showTestEditDialog(selectedTest);
            if (okClicked) {
                showTestDetails(selectedTest);
            }

        }
        else {
            // Nothing selected.
            Alert alert = new Alert(
                    AlertType.WARNING);
            alert.initOwner(mainApp
                    .getPrimaryStage());
            alert.setTitle("No Selection");
            alert.setHeaderText("No Test Selected");
            alert.setContentText("Please select a test in the table.");

            alert.showAndWait();
        }
    }
}