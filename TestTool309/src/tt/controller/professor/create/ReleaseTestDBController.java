package tt.controller.professor.create;

import tt.model.professor.create.TestDB;

/**
 * 
 * @author Ryan Lee
 *
 */

public class ReleaseTestDBController {
    public TestDB testDB;
    
    private static ReleaseTestDBController controller = null;
    
    public ReleaseTestDBController() {
        if(testDB == null)
            testDB = new TestDB();
    }
    
    public static ReleaseTestDBController getDBController() {
        if(controller == null)
            controller = new ReleaseTestDBController();
        return controller;
    }
    
    public TestDB getTestDB() {
        if(testDB == null)
            testDB = new TestDB();
        return testDB;
    }
    
    public void setTestDB(TestDB testDBParam) {
        testDB.setTestDB(testDBParam.getTestDB());
    }
}