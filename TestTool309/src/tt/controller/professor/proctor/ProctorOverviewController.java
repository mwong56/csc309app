package tt.controller.professor.proctor;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import tt.MainApp;
import tt.model.Course;

/**
 * Controller class to handle proctor overview
 * 
 * @author Dat Tran
 *
 */

public class ProctorOverviewController {

    /** Reference to the main application. */
    private MainApp mainApp;
    
    /** The table with courses */
    @FXML
    private TableView<Course> coursesTable;
    
    /** The column with course abbreviations */
    @FXML
    private TableColumn<Course, String> courseAbbrCol;

    /** The column with course numbers */
    @FXML
    private TableColumn<Course, Number> courseNumCol;

    /** The column with course sections */
    @FXML
    private TableColumn<Course, Number> courseSectCol;
    
    /** The column with course IDs */
    @FXML
    private TableColumn<Course, Number> courseIdCol;
    
    /** The course selected */
    protected Course course;
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize table with four columns
        courseAbbrCol
        .setCellValueFactory(cellData -> new SimpleStringProperty(cellData
                .getValue()
                .getCourseAbbr()));
        courseNumCol
        .setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData
                .getValue()
                .getNumber()));
        courseSectCol
        .setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData
                .getValue()
                .getSection()));
        courseIdCol
        .setCellValueFactory(cellData -> new SimpleIntegerProperty(cellData
                .getValue()
                .getId()));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp isn't null
                //
                this.mainApp != null
     * 
     * @param mainApp The mainApp to refer to
     */
    public void setMainApp(MainApp app) {
        mainApp = app;
        
        ObservableList<Course> courses = FXCollections.
                observableArrayList(mainApp.getProfessor().getCourses());
        coursesTable.setItems(courses);
    }
    
    /**
     * Indicates that the main app should now show the proctor course overview screen
     */
    @FXML
    private void showProctorCourseOverview() {
        int selectedIndex = coursesTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            course = coursesTable.getSelectionModel().getSelectedItem();
            mainApp.showProctorCourseOverview(course);
        } else {
            Alert alert = new Alert(
                    AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("No course selected");
            alert.setContentText("Please select a course in the table.");
     
            alert.showAndWait();
        }

    }
}
