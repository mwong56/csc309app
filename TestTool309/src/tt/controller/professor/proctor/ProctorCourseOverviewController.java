package tt.controller.professor.proctor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import tt.MainApp;
import tt.controller.professor.create.ReleaseTestDBController;
import tt.controller.professor.create.TestDBController;
import tt.model.Course;
import tt.model.professor.Professor;
import tt.model.professor.create.Test;
import tt.model.professor.questions.Question;

/**
 * Handles released tests and stuff
 * 
 * @author Dat Tran
 *
 */
public class ProctorCourseOverviewController {
    
    /** Reference to the main application. */
    private MainApp mainApp;
    
    /** The table with courses */
    @FXML
    private TableView<Test> testsTable;
    
    /** The column with course abbreviations */
    @FXML
    private TableColumn<Test, String> testNameCol;

    /** The column with course numbers */
    @FXML
    private TableColumn<Test, String> testTypeCol;
    
    @FXML
    private Label courseLabel;
    @FXML
    private Label testNameLabel;
    @FXML
    private Label testTypeLabel;
    @FXML
    private Label numQuestionsLabel;
    @FXML
    private Label timeAllowedLabel;
    
    /** Course that was selected */
    private Course course;
    
    /**
     * Default constructor
     */
    public ProctorCourseOverviewController() {
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        
        // Initialize table with two columns
        testNameCol
        .setCellValueFactory(cellData -> cellData
                .getValue()
                .getTitle());
        testTypeCol
        .setCellValueFactory(cellData -> cellData
                .getValue()
                .getType());
        
        // Clear test details
        showTestDetails(null);
        
        testsTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showTestDetails(newValue)); 
    }

   /** Fills all text fields to show details about the person.
    * If the specified person is null, all text fields are cleared.
    * 
    * @param person the person or null
    */
   private void showTestDetails(Test test) {
       if (test != null) {
           // Fill the labels with info from the Test object.
           testNameLabel.setText(test.getTitle().get());
           testTypeLabel.setText(test.getType().get());
           numQuestionsLabel.setText(String.valueOf(test.getQuestionList().size()));
           timeAllowedLabel.setText(test.getLimit().get());
       } else {
           // Test is null, remove all the text.
           testNameLabel.setText("");
           testTypeLabel.setText("");
           numQuestionsLabel.setText("");
           timeAllowedLabel.setText("");
       }
   }
   
   /**
    * Sets the course to be viewed to the passed-in parameter
    * 
    * @param course The course to be set
    */
   public void setCourse(Course course) {
       this.course = course;
       courseLabel.setText(course.toString());
   }
   
   /**
    * Is called by the main application to give a reference back to itself.
    * 
    * @param mainApp
    */
   public void setMainApp(MainApp mainApp) {
       this.mainApp = mainApp;
       
       // Get released tests for the appropriate class
       ReleaseTestDBController tc = new ReleaseTestDBController();
       ArrayList<Test> releasedTests = tc.getDBController().testDB.getTestDB();
       ArrayList<Test> tests = new ArrayList<Test>();
       
       for (Test test : releasedTests) {
           if (test.getCourse().get().equals(course.getCourseAbbr() +
                   "-"  + course.getNumber())) {
              tests.add(test);
           }
       }

       ObservableList<Test> testList = FXCollections.observableArrayList(tests);
       
//       public Question(String classString,
//               int difficulty, int time, String typeString, String prompt) {

       // Hard coded because other person's work doesn't work properly
       ArrayList<Question> questions = new ArrayList<Question>();
       questions.add(new Question("CPE-309", 1, 9001, "SA", "Why is no else doing their work?"));
       questions.add(new Question("CPE-309", 1, 9001, "SA", "Mandy and Dat are da real MVPs."));
       Test test = new Test(questions, "15 minutes", "6/12/15 at 11:59 PM",
               "Final", "CPE-309", mainApp.getProfessor(), "In-class");
       ArrayList<Test> testTestList = new ArrayList<Test>();
       testTestList.add(test);
       ObservableList<Test> blah = FXCollections.observableArrayList(testTestList);
       
       // Add observable list data to the table
       testsTable.setItems(blah);
   }
    
   
   /**
    * Indicates that the main app should now show the release test dialog.
    */
   @FXML
   private void showReleaseTestDialog() {
       int selectedIndex = testsTable.getSelectionModel().getSelectedIndex();
       if (selectedIndex >= 0) {
           Test releaseTest = testsTable.getItems().get(selectedIndex);
           Alert alert = new Alert(
                   AlertType.CONFIRMATION);
           alert.setTitle("Release Test");
           alert.setHeaderText("Are you sure you want to release this test to your class?");
    
           Optional<ButtonType> input = alert.showAndWait();
           if (input.get() == ButtonType.OK && 
                   !mainApp.getTestData().contains(releaseTest)) {
               Alert alert1 = new Alert(
                       AlertType.INFORMATION);
               alert1.setTitle("Release Test");
               alert1.setHeaderText("Test is released. Students can now view this test.");
               alert1.show();
               if (!mainApp.getTestData().contains(releaseTest))
                   mainApp.getTestData().add(releaseTest);
               else {
                   Alert alert2 = new Alert(
                           AlertType.ERROR);
                   alert2.setTitle("Release Test");
                   alert2.setHeaderText("Test was already released");
                   alert2.show();
               }
           } else {
               Alert alert2 = new Alert(
                       AlertType.ERROR);
               alert2.setTitle("Release Test");
               alert2.setHeaderText("Test was already released");
               alert2.show();
           }
       } else {
           Alert alert = new Alert(
                   AlertType.ERROR);
           alert.setTitle("ERROR");
           alert.setHeaderText("No test selected");
           alert.setContentText("Please select a test in the table.");
    
           alert.showAndWait();
       }
   }
   
   
}
