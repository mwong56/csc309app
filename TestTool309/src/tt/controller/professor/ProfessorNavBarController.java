package tt.controller.professor;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import tt.MainApp;

/**
 * Navigation bar controller
 * 
 * @author Dat Tran
 *
 */

public class ProfessorNavBarController {
    /** Reference to the main application. */
    private MainApp mainApp;

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp isn't null
                //
                this.mainApp != null
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Indicates that the main app should now show the professor overview
     * screen.
     */
    @FXML
    private void showProfessorOverview() {
        mainApp.showProfessorOverview();
    }

    /**
     * Indicates that the main app should now show the Question Bank screen.
     */
    @FXML
    private void showQuestionBank() {
        mainApp.showQuestionBank();
    }

    /**
     * Indicates that the main app should now show the create overview screen.
     */
    @FXML
    private void showCreateOverview() {
        mainApp.showCreateOverview();
    }

    /**
     * Indicates that the main app should now show the available tests screen.
     */
    @FXML
    private void showAvailableTests() {
        mainApp.showAvailableTests();
    }

    /**
     * Indicates that the main app should now show the proctoring screen.
     */
    @FXML
    private void showProctorTest() {
        mainApp.showProctorTest();
    }

    /**
     * Indicates that the main app should now show the release test screen.
     */
    @FXML
    private void showReleaseTest() {
        mainApp.showReleaseTest();
    }

    /**
     * Indicates that the main app should now show the admin screen.
     */
    @FXML
    private void showDefaultSettings() {
        mainApp.showInClass();
    }

    /**
     * Indicates that the main app should now show the review screen.
     */
    @FXML
    private void showGradingOverview() {
        mainApp.showGradingOverview();
    }
}
