package tt.controller.professor.grading;

import java.util.ArrayList;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.professor.create.Test;
import tt.model.professor.grading.gradetest;
import tt.model.professor.questions.QuestionBank;
import tt.model.student.take.FinishedTest;
import tt.model.student.take.TakeTestDB;

public class GradingOverviewController {
   
    FinishedTest selected;
    gradetest temp;
    @FXML
    private TableView<gradetest> resultTable;
    @FXML
    private TableView<FinishedTest> personTable;
    @FXML
    private TableColumn<FinishedTest, String> classes;
    @FXML
    private TableColumn<FinishedTest, String> test;
    
  
  
    // Reference to the main application.
    private MainApp mainApp;
    @FXML
    private Stage dialogStage;
    
    private TakeTestDB mytestdb;
    
    /**
     * The constructor. The constructor is called before the initialize()
     * method.
     */
    public GradingOverviewController() {
    }

    /**
     * Sets the stage of this dialog.
     * 
      <pre>
            pre:
                //
                // dialogStage isn't null
                //
                dialogStage != null;
                
            post:
                //
                // this.dialogStage' isn't null and equals dialogStage
                //
                this.dialogStage' != null
                
                    &&
               
                this.dialogStage'.equals(dialogStage);
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
   

    /**
     * Called when the user clicks cancel.
     * 
      <pre>
            pre:
                //
                // dialogStage isn't null
                //
      dialogStage != null;
     */
    @FXML
    private void handleCancel() {
        mainApp.getPrimaryStage().close();
        dialogStage.close();
    }

    /**
     * Called when the user clicks on the delete button.
     */
    @FXML
    private void handleClick() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
        selected = personTable.getItems().get(selectedIndex);
        if(selected.getStudent() != null)
        {
            //System.out.println("selected test " + selected.getStudent().getFirstName());
            gradetest.taken = selected;

        }
        if (selectedIndex >= 0) {
            gradetest.taken = selected;
            boolean okClicked = mainApp.showTestResult();
        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Error");
            alert.setHeaderText("No Test selected");
            alert.setContentText("Please select a test.");

            alert.showAndWait();
        }
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        classes.setCellValueFactory(cellData -> cellData.getValue().getTest().getCourse());
        test.setCellValueFactory(cellData -> cellData.getValue().getTest().getTitle());

    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        temp = new gradetest();
        personTable.setItems(temp.getTest());
    }
}
