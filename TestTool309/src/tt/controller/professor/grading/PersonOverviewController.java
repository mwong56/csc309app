package tt.controller.professor.grading;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.professor.grading.gradetest;
import tt.model.professor.questions.Question;
import tt.model.student.take.FinishedTest;

public class PersonOverviewController {

    ObservableList <StringProperty> checklist = FXCollections.observableArrayList();
    @FXML
    private TableColumn<Question, String> questions;
    @FXML
    private TableColumn<Question, String> answer;
    @FXML
    private TableView<Question> testTable;
    @FXML
    private TableView<StringProperty> stringTable;
    @FXML
    private TableColumn<StringProperty, String> str;
    // Reference to the main application.
    private MainApp mainApp;
    private Stage dialogStage;


    @FXML
    private void handleClose() {
        mainApp.getPrimaryStage().close();
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        questions.setCellValueFactory(cellData -> cellData.getValue().promptProperty());
        answer.setCellValueFactory(cellData -> cellData.getValue().answerProperty());
        testTable.setItems(gradetest.taken.getTest().getQuestionList());
        checklist.add(new SimpleStringProperty("Yes"));
        checklist.add(new SimpleStringProperty("No"));
        checklist.add(new SimpleStringProperty("Yes"));
        checklist.add(new SimpleStringProperty("No"));
        str.setCellValueFactory(cellData -> cellData.getValue());
        stringTable.setItems(checklist);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Add observable list data to the table
    }

   
    @FXML
    private void openGraderTest() {
        /************
         * TO FIX KA -- NEED METHOD IN MAINAPP
         ************/
        // mainApp.BigPanelOverview();
        mainApp.getPrimaryStage().close();
    }

}