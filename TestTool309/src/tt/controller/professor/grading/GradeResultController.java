package tt.controller.professor.grading;

import java.util.Observable;

import tt.MainApp;
import tt.model.professor.create.Test;
import tt.model.professor.grading.gradetest;
import tt.model.professor.questions.Question;
import tt.model.student.Student;
import tt.model.student.take.FinishedTest;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * Dialog to edit details of a person.
 * 
 * @author Ka Tong
 */
public class GradeResultController {
    
    ObservableList <FinishedTest> myListT = FXCollections.observableArrayList();
    ObservableList <StringProperty> scoreList = FXCollections.observableArrayList();
    @FXML
    private TableView<Student> studentTable;
    @FXML
    private TableView<FinishedTest> testTable;
    @FXML
    private TableView<StringProperty> stringTable;
    
    @FXML 
    private TableColumn<Student, String> studentF;
    @FXML 
    private TableColumn<Student, String> studentL;
  
    @FXML
    private TableColumn<FinishedTest, String> testTaken;
    @FXML
    private TableColumn<FinishedTest, String> testType;
    @FXML
    private TableColumn<StringProperty, String> score;
 // Reference to the main application.
    private MainApp mainApp;
    
    private Stage dialogStage;
    private boolean okClicked = false; 
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        gradetest temp = new gradetest();
        studentF.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        studentL.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        studentTable.setItems(temp.getStudent());
        
        for(int i = 0; i < temp.getStudent().size(); i++)
        {
            myListT.add(gradetest.taken);
          
        }
        scoreList.add(new SimpleStringProperty(new String("3/4")));
        scoreList.add(new SimpleStringProperty(new String("2/4")));
        scoreList.add(new SimpleStringProperty(new String("4/4")));
        scoreList.add(new SimpleStringProperty(new String("1/4")));
        scoreList.add(new SimpleStringProperty(new String("4/4")));
        scoreList.add(new SimpleStringProperty(new String("4/4")));
        testTaken.setCellValueFactory(cellData -> cellData.getValue().getTest().getTitle());
        testType.setCellValueFactory(cellData -> cellData.getValue().getTest().getType());
        testTable.setItems(myListT);
        
        score.setCellValueFactory(cellData -> cellData.getValue());
        stringTable.setItems(scoreList);
        
     }
  
    @FXML
    private void handleClick() {
        int selectedIndex = testTable.getSelectionModel().getSelectedIndex();
        
        if (selectedIndex >= 0) {
            mainApp.showReviewTest();
        } else {
            // Nothing selected.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Error");
            alert.setHeaderText("No Test selected");
            alert.setContentText("Please select a student's test to review.");

            alert.showAndWait();
        }
    }
    
    /**
     * Sets the stage of this dialog.
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

   
    

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }
   
    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
    
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

    }
      
}
