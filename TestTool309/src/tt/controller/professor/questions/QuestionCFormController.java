package tt.controller.professor.questions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import tt.model.professor.questions.CodeQuestion;

/**
 * Controls the Code question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a Code question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionCFormController {
    /** Question Prompt */
    @FXML
    private TextArea prompt;

    /** Language Type */
    @FXML
    private ComboBox<String> languageComboBox;
    
    /** The Code Script */
    @FXML
    private TextArea script;
    
    /** An observable list of language types; will not change */
    private ObservableList<String> languageTypes;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * Constructor for a Code question's form controller. It creates the 
     * language types that a code question can be in.
     */
    public QuestionCFormController() {
        languageTypes = FXCollections.observableArrayList();
        languageTypes.addAll("Java", "C/C++", "Python", "Javascript");
        
        languageComboBox = new ComboBox<String>();
    }
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. The language types for a code 
     * question is initialized in here.
     */
    @FXML
    public void initialize() {
        languageComboBox.setItems(languageTypes);
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a Code question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(CodeQuestion question) {
        setPrompt(question.getPrompt());
        setLanguage(question.getLanguage());
        setScript(question.getScript());
    }
    
    /**
     * For Question Overview. Sets the question prompt to p.
     * 
     * @param p The question prompt to be set
     */
    private void setPrompt(String p) {
        prompt.setText(p);
    }
    
    /**
     * For Question Overview. Sets the question's code language to lang.
     * 
     * @param lang The code language to be set
     */
    private void setLanguage(String lang) {
        for (int i = 0; i < languageTypes.size(); i++) {
            if (languageTypes.get(i).equals(lang)) {
                languageComboBox.getSelectionModel().select(i);
                break;
            }
        }
    }
    
    /**
     * For Question Overview. Sets the question's code script to s.
     * 
     * @param s The script to be set
     */
    private void setScript(String s) {
        script.setText(s);
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() {
        return prompt.getText();
    }
    
    /**
     * Gets the code language from the dialog.
     * 
     * @return The language selected in the form of a String
     */
    public String getLanguage() {
        return languageComboBox.getSelectionModel().getSelectedItem();
    }
    
    /**
     * Gets the code script from the dialog.
     * 
     * @return The code script in the form of a String
     */
    public String getScript() {
        return script.getText();
    }
}
