package tt.controller.professor.questions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.professor.questions.CodeQuestion;
import tt.model.professor.questions.FillInTheBlankQuestion;
import tt.model.professor.questions.LongAnswerQuestion;
import tt.model.professor.questions.MatchingQuestion;
import tt.model.professor.questions.MultipleChoiceQuestion;
import tt.model.professor.questions.MultipleResponseQuestion;
import tt.model.professor.questions.Question;
import tt.model.professor.questions.QuestionBank;
import tt.model.professor.questions.QuestionType;
import tt.model.professor.questions.ShortAnswerQuestion;
import tt.model.professor.questions.TrueFalseQuestion;

/**
 * Controls the Add Question dialog. This dialog is found in the Question 
 * Bank view. Professors can add questions to their question bank via this form.
 * If the created question does not meet requirements, an error is shown and 
 * the professor must fix it in order to add the question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class AddQuestionDialogController {

    /** ComboBox for classes */
    @FXML
    private ComboBox<String> classComboBox;

    /** Difficulty */
    @FXML
    private ToggleGroup difficulty;

    /** Estimated time field */
    @FXML
    private TextField estTime;

    /** ComboBox for time types */
    @FXML
    private ComboBox<String> timeTypesComboBox;

    /** ComboBox for question types */
    @FXML
    private ComboBox<String> typeComboBox;
    
    /** Question type's pane */
    @FXML
    private AnchorPane qTypePane;

    /** Reference to the main application. */
    private MainApp mainApp;

    /** Reference to the dialog stage */
    private Stage dialogStage;

    /** Reference to the question bank */
    private QuestionBank questionBank;

    /** Reference to the question that will be added */
    private Question question;

    /** An observable list of question types; will not change */
    private ObservableList<String> questionTypes = FXCollections
            .observableArrayList();

    /** Flag to indicate whether "Add" has been clicked */
    private boolean okClicked = false;
    
    /** TF controller */
    private QuestionTFFormController TFController;

    /** MC controller */
    private QuestionMCFormController MCController;
    
    /** MR controller */
    private QuestionMRFormController MRController;
    
    /** M controller */
    private QuestionMFormController MController;
    
    /** FB controller */
    private QuestionFBFormController FBController;
    
    /** SA controller */
    private QuestionSAFormController SAController;
    
    /** LA controller */
    private QuestionLAFormController LAController;
    
    /** C controller */
    private QuestionCFormController CController;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Handle classComboBox event.
        classComboBox.setOnAction((event) -> {
            String selectedClass = classComboBox
                    .getSelectionModel()
                    .getSelectedItem();
            if (selectedClass
                    .equals("Add a Class..."))
                mainApp.showAddClassDialog();
        });

        // Handle typeComboBox event.
        questionTypes.addAll("True/False",
                "Mult. Choice", "Mult. Response",
                "Matching", "Fill-in-the-Blank",
                "Short Answer", "Long Answer",
                "Code");
        typeComboBox.setItems(questionTypes);

        typeComboBox.setOnAction(this::typeComboBoxEvent);
    }
    
    private void typeComboBoxEvent(ActionEvent event) {
        QuestionType type = QuestionType
                .values()[typeComboBox
                .getSelectionModel()
                .getSelectedIndex()];

        MainApp.showQuestionForm(this, type);
    }
    
    /** 
     * Show a specific question type's pane
     * 
     * @param pane The AnchorPane to be set.
     */
    public void setQTypePane(AnchorPane pane) {
        qTypePane.getChildren().clear();
        qTypePane.getChildren().add(pane);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp The MainApp to be set.
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Sets the stage of this dialog.
     * 
      <pre>
            pre:
                //
                // dialogStage isn't null
                //
                dialogStage != null;
                
            post:
                //
                // this.dialogStage' isn't null and equals dialogStage
                //
                this.dialogStage' != null
                
                    &&
               
                this.dialogStage'.equals(dialogStage);
     * 
     * @param dialogStage The Stage that will be set.
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the question bank to be edited in the dialog.
     * 
      <pre>
            pre:
                //
                // qb isn't null
                //
                qb != null;
                
            post:
                //
                // questionBank' isn't null and equals qb
                //
                questionBank' != null
                
                    &&
               
                questionBank'.equals(qb);
     * 
     * @param qb The Question Bank to be set.
     */
    public void setQuestionBank(QuestionBank qb) {
        this.questionBank = qb;

        /** Setting items that need questionBank to be initialized first */
        classComboBox.setItems(questionBank
                .getClassData());
    }

    /**
     * Sets the question to be edited in the dialog.
     * 
      <pre>
            pre:
                //
                // q isn't null
                //
                q != null;
                
            post:
                //
                // question' isn't null and equals q
                //
                question' != null
                
                    &&
               
                question'.equals(q);
     * 
     * @param q The question to be set.
     */
    public void setQuestion(Question q) {
        this.question = q;
    }
    
    /**
     * Create an appropriate question type. It gathers all of the information 
     * in the dialog and attempts to create a question with it. If it can't 
     * create a specific question type, it will create a generic Question, 
     * which will throw an error later when checked.
     */
    private void createQuestion() {
        String className = classComboBox.getSelectionModel().getSelectedItem();
        RadioButton selectedDiff = (RadioButton) difficulty.getSelectedToggle();
        Integer difficulty = Integer.parseInt(selectedDiff.getText());
        String timeAlotted = estTime.getText();
        QuestionType type;
        
        try { 
            type = QuestionType
                .values()[typeComboBox
                .getSelectionModel()
                .getSelectedIndex()];
        
            switch(type) {
            case TF:
                question = new TrueFalseQuestion(className, difficulty, timeAlotted, type.toString(), TFController.getPrompt(), TFController.getCorrectAnswer());
                break;
            case C:
                question = new CodeQuestion(className, difficulty, timeAlotted, type.toString(), CController.getPrompt(), CController.getLanguage(), CController.getScript());
                break;
            case FB:
                question = new FillInTheBlankQuestion(className, difficulty, timeAlotted, type.toString(), FBController.getPrompt());
                break;
            case LA:
                question = new LongAnswerQuestion(className, difficulty, timeAlotted, type.toString(), LAController.getPrompt(), LAController.getAnswerNotes());
                break;
            case M:
                question = new MatchingQuestion(className, difficulty, timeAlotted, type.toString(), MController.getPrompt(), MController.getQuestions(), MController.getChoices(), MController.getCorrectAnswers());
                break;
            case MC:
                question = new MultipleChoiceQuestion(className, difficulty, timeAlotted, type.toString(), MCController.getPrompt(), MCController.getChoices(), MCController.getCorrectAnswerIndex());
                break;
            case MR:
                question = new MultipleResponseQuestion(className, difficulty, timeAlotted, type.toString(), MRController.getPrompt(), MRController.getChoices(), MRController.getCorrectAnswerIndices());
                break;
            case SA:
                question = new ShortAnswerQuestion(className, difficulty, timeAlotted, type.toString(), SAController.getPrompt(), SAController.getAnswerNotes());
                break;
            default:
                break;
            }
        } catch (java.lang.ArrayIndexOutOfBoundsException exc) {
            question = new Question(className, difficulty, timeAlotted, null, null);
        }
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return Whether or not the user pressed OK
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Method called when the user clicks OK. It creates a question and 
     * has it validate itself. If any part doesn't meet requirements, the 
     * question isn't added to the question bank and the user receives an 
     * alert, alerting them that their question doesn't meet certain 
     * requirements. It will give specifics on what was wrong with the 
     * question so that the user can fix it and attempt to add it to their 
     * question bank again.
     * 
      <pre>
            pre:
                //
                // question isn't null
                //
                question != null;
     */
    @FXML
    private void handleOk() {
        createQuestion();
        
        String errorMessage = question.validate();
        if (errorMessage.length() > 0) {
         // Show the error message.
            Alert alert = new Alert(
                    AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();
        } else {
            okClicked = true;
            questionBank.addQuestion(question);
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel. Closes the dialog without adding 
     * the question to the question bank.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionTFFormController controller) {
        TFController = controller;
    }

    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionMCFormController controller) {
        this.MCController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionMRFormController controller) {
        this.MRController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionMFormController controller) {
        this.MController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionFBFormController controller) {
        this.FBController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionSAFormController controller) {
        this.SAController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionLAFormController controller) {
        this.LAController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionCFormController controller) {
        this.CController = controller;
    }
}