package tt.controller.professor.questions;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.professor.questions.CodeQuestion;
import tt.model.professor.questions.FillInTheBlankQuestion;
import tt.model.professor.questions.LongAnswerQuestion;
import tt.model.professor.questions.MatchingQuestion;
import tt.model.professor.questions.MultipleChoiceQuestion;
import tt.model.professor.questions.MultipleResponseQuestion;
import tt.model.professor.questions.Question;
import tt.model.professor.questions.QuestionBank;
import tt.model.professor.questions.ShortAnswerQuestion;
import tt.model.professor.questions.TrueFalseQuestion;

/**
 * Dialog to view a particular question's details. The user can press the 
 * 'Edit' button to edit this particular question's information. However, 
 * the user can not change the question type, as that would make the question 
 * entirely different (and therefore, should make a new question instead).
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionOverviewController {
    /** ComboBox for classes */
    @FXML
    private ComboBox<String> classComboBox;

    /** Difficulty */
    @FXML
    private ToggleGroup difficulty;

    /** Estimated time field */
    @FXML
    private TextField estTime;

    /** ComboBox for time types */
    @FXML
    private ComboBox<String> timeTypesComboBox;

    /** Question type */
    @FXML
    private Label qType;
    
    /** Question type's pane */
    @FXML
    private AnchorPane qTypePane;

    /** Reference to the main application. */
    private MainApp mainApp;

    /** Reference to the dialog stage */
    private Stage dialogStage;

    /** Reference to the question bank */
    private QuestionBank questionBank;

    /** Reference to the question */
    private Question question;
    
    /** TF controller */
    private QuestionTFFormController TFController;

    /** MC controller */
    private QuestionMCFormController MCController;
    
    /** MR controller */
    private QuestionMRFormController MRController;
    
    /** M controller */
    private QuestionMFormController MController;
    
    /** FB controller */
    private QuestionFBFormController FBController;
    
    /** SA controller */
    private QuestionSAFormController SAController;
    
    /** LA controller */
    private QuestionLAFormController LAController;
    
    /** C controller */
    private QuestionCFormController CController;
    
    /** Indicated whether or not the question was updated */
    boolean updated;
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. It binds the function that checks 
     * for Adding a Class selection here.
     */
    @FXML
    private void initialize() {
     // Handle classComboBox event.
        classComboBox.setOnAction((event) -> {
            String selectedClass = classComboBox
                    .getSelectionModel()
                    .getSelectedItem();
            if (selectedClass
                    .equals("Add a Class..."))
                mainApp.showAddClassDialog();
        });
    }
    
    /**
     * For Question Overview. Sets the question whose information will be shown.
     * 
     * @param q The question to be shown
     */
    public void setQuestion(Question q) {
        question = q;
        
        updateReferences();
    }
    
    /**
     * Gets the long version of the question type name
     * 
     * @return The long version of the question type's name
     */
    private String getLongTypeName(String name) {
        switch (name) {
        case "TF":
            return "True/False";
        case "MC":
            return "Multiple Choice";
        case "MR":
            return "Multiple Response";
        case "M":
            return "Matching";
        case "FB":
            return "Fill-in-the-Blank";
        case "SA":
            return "Short Answer";
        case "LA":
            return "Long Answer";
        case "C":
            return "Code";
        default:
            return "";
        }
    }
    
    /**
     * Given a list of courses, return the index where the question's course 
     * was found.
     * 
     * @param courses The list of courses that a professor teaches
     * @return The index where the course was found in the list
     */
    private int findCourse(ObservableList<String> courses) {
        for (int i = 0; i < courses.size(); i++) {
            if (courses.get(i).equals(question.getClassName()))
                return i;
        }
        return 0;
    }
    
    /**
     * Sets the difficulty to what the question's difficulty is.
     * 
     * @param diff The difficulty of the question
     */
    private void setDifficulty(String diff) {
        ObservableList<Toggle> difficulties = difficulty.getToggles();
        for (int i = 0; i < difficulties.size(); i++) {
            RadioButton button = (RadioButton) difficulties.get(i);
            button.setSelected(button.getText().equals(diff));
        }
    }
    
    /**
     * Pulls the information out of question and displays it in the dialog. 
     * Updates the labels that will show the information.
     */
    private void updateReferences() {
        if (question != null) {
            ObservableList<String> courses = classComboBox.getItems();
            classComboBox.getSelectionModel().select(findCourse(courses));
            setDifficulty(question.getDifficulty());
            estTime.setText(question.getEstTime());
            qType.setText(getLongTypeName(question.getType()));
            
            mainApp.showQuestionTypeInformation(this, question.getType());
            
            switch (question.getType()) {
            case "TF":
                TFController.setQuestionInfo((TrueFalseQuestion) question);
                break;
            case "MC":
                MCController.setQuestionInfo((MultipleChoiceQuestion) question);
                break;
            case "MR":
                MRController.setQuestionInfo((MultipleResponseQuestion) question);
                break;
            case "M":
                MController.setQuestionInfo((MatchingQuestion) question);
                break;
            case "FB":
                FBController.setQuestionInfo((FillInTheBlankQuestion) question);
                break;
            case "SA":
                SAController.setQuestionInfo((ShortAnswerQuestion) question);
                break;
            case "LA":
                LAController.setQuestionInfo((LongAnswerQuestion) question);
                break;
            case "C":
                CController.setQuestionInfo((CodeQuestion) question);
                break;
            default:
                break;
            }
        }
    }
    
    /**
     * Returns whether or not the question's information was changed.
     * 
     * @return Whether or not a question was updated
     */
    public boolean isUpdated() {
        return updated;
    }
    
    /**
     * Create an appropriate question type.
     */
    private void createQuestion() {
        String className = classComboBox.getSelectionModel().getSelectedItem();
        RadioButton selectedDiff = (RadioButton) difficulty.getSelectedToggle();
        Integer difficulty = Integer.parseInt(selectedDiff.getText());
        String timeAlotted = estTime.getText();
        String type = question.getType();
        
        try {
            switch(type) {
            case "TF":
                question = new TrueFalseQuestion(className, difficulty, timeAlotted, type.toString(), TFController.getPrompt(), TFController.getCorrectAnswer());
                break;
            case "C":
                question = new CodeQuestion(className, difficulty, timeAlotted, type.toString(), CController.getPrompt(), CController.getLanguage(), CController.getScript());
                break;
            case "FB":
                question = new FillInTheBlankQuestion(className, difficulty, timeAlotted, type.toString(), FBController.getPrompt());
                break;
            case "LA":
                question = new LongAnswerQuestion(className, difficulty, timeAlotted, type.toString(), LAController.getPrompt(), LAController.getAnswerNotes());
                break;
            case "M":
                question = new MatchingQuestion(className, difficulty, timeAlotted, type.toString(), MController.getPrompt(), MController.getQuestions(), MController.getChoices(), MController.getCorrectAnswers());
                break;
            case "MC":
                question = new MultipleChoiceQuestion(className, difficulty, timeAlotted, type.toString(), MCController.getPrompt(), MCController.getChoices(), MCController.getCorrectAnswerIndex());
                break;
            case "MR":
                question = new MultipleResponseQuestion(className, difficulty, timeAlotted, type.toString(), MRController.getPrompt(), MRController.getChoices(), MRController.getCorrectAnswerIndices());
                break;
            case "SA":
                question = new ShortAnswerQuestion(className, difficulty, timeAlotted, type.toString(), SAController.getPrompt(), SAController.getAnswerNotes());
                break;
            default:
                break;
            }
        } catch (java.lang.ArrayIndexOutOfBoundsException exc) {
            question = new Question(className, difficulty, timeAlotted, null, null);
        }
    }
    
    /**
     * Called when the user clicks Update. Re-creates the question and 
     * re-validate the validity of the question. If the question doesn't meet 
     * requirements, it will throw an alert, letting the user know that there 
     * were problems updating the question. The question isn't updated and the 
     * user must fix the problems listed in order to update the question.
     */
    @FXML
    private void handleUpdate() {
        createQuestion();
        String errorMessage = question.validate();
        
        if (errorMessage.length() > 0) {
            // Show the error message.
           Alert alert = new Alert(
                   AlertType.ERROR);
           alert.initOwner(dialogStage);
           alert.setTitle("Invalid Fields");
           alert.setHeaderText("Please correct invalid fields");
           alert.setContentText(errorMessage);

           alert.showAndWait();
       } else {
           updated = true;
           dialogStage.close();
       }
    }
    
    /**
     * Called when the user clicks cancel. Closes the dialog without updating 
     * the question.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }
    
    /**
     * Returns the question the controller is displaying info about.
     * 
     * @return The question displayed
     */
    public Question getQuestion() {
        return question;
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    /**
     * Sets the stage of this dialog.
     * 
      <pre>
            pre:
                //
                // dialogStage isn't null
                //
                dialogStage != null;
                
            post:
                //
                // this.dialogStage' isn't null and equals dialogStage
                //
                this.dialogStage' != null
                
                    &&
               
                this.dialogStage'.equals(dialogStage);
     * 
     * @param dialogStage The stage the controller is controlling
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    /**
     * Sets the question bank to be edited in the dialog.
     * 
      <pre>
            pre:
                //
                // qb isn't null
                //
                qb != null;
                
            post:
                //
                // questionBank' isn't null and equals qb
                //
                questionBank' != null
                
                    &&
               
                questionBank'.equals(qb);
     * 
     * @param qb
     */
    public void setQuestionBank(QuestionBank qb) {
        this.questionBank = qb;

        /** Setting items that need questionBank to be initialized first */
        classComboBox.setItems(questionBank
                .getClassData());
    }
    
    /** 
     * Show specific question type's pane
     */
    public void setQTypePane(AnchorPane pane) {
        qTypePane.getChildren().clear();
        qTypePane.getChildren().add(pane);
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionTFFormController controller) {
        TFController = controller;
    }

    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionMCFormController controller) {
        this.MCController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionMRFormController controller) {
        this.MRController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionMFormController controller) {
        this.MController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionFBFormController controller) {
        this.FBController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionSAFormController controller) {
        this.SAController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionLAFormController controller) {
        this.LAController = controller;
    }
    
    /**
     * Sets the controller for a specific question type's view.
     * 
     * @param controller The controller for the specific question type
     */
    public void setQTypeController(
            QuestionCFormController controller) {
        this.CController = controller;
    }
}
