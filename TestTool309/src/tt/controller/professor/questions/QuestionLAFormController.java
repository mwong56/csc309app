package tt.controller.professor.questions;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import tt.model.professor.questions.LongAnswerQuestion;

/**
 * Controls the LA question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a LA question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionLAFormController {
    
    /** Question Prompt */
    @FXML
    private TextArea prompt;
    
    /** Answer */
    @FXML
    private TextArea answerNotes;
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a LA question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(LongAnswerQuestion question) {
        setPrompt(question.getPrompt());
        setAnswerNotes(question.getAnswerNotes());
    }
    
    /**
     * For Question Overview. Sets the question prompt to p.
     * 
     * @param p The question prompt to be set
     */
    private void setPrompt(String p) {
        prompt.setText(p);
    }
    
    /**
     * For Question Overview. Sets the answer notes to notes.
     * 
     * @param notes The answer notes to be set
     */
    private void setAnswerNotes(String notes) {
        answerNotes.setText(notes);
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() { return prompt.getText(); }
    
    /**
     * Gets the answer notes from the dialog.
     * 
     * @return The answer notes in the form of a String
     */
    public String getAnswerNotes() { return answerNotes.getText(); }
}