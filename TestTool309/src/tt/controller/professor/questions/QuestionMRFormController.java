package tt.controller.professor.questions;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import tt.model.professor.questions.MultipleResponseQuestion;
import tt.util.GridUtil;

/**
 * Controls the MR question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a MR question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionMRFormController {
    /** Question Prompt */
    @FXML
    private TextArea prompt;
    
    /** GridPane for answer list */
    @FXML
    private GridPane answerGrid;
    
    /** + Button */
    @FXML
    private Button addChoiceButton;
    
    /** Number of rows */
    private int rowSize;
    
    /** Number of columns */
    private final int columnSize = 3;
    
    /** Column index for choice text */
    private final int choiceColumnIndex = 1;
    
    /** Tracking check boxes */
    private ArrayList<CheckBox> choices;
    
    /** Tracking deleteButtons */
    private ArrayList<Button> deleteButtons;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. It initializes the list of choices 
     * and delete buttons. It also adds two default empty choices as a user 
     * should not be able to have less than two choices for a MR question.
     */
    @FXML
    private void initialize() {
        rowSize = 0;
        choices = new ArrayList<CheckBox>();
        deleteButtons = new ArrayList<Button>();
        
        addChoice();
        addChoice();
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Adds a row to the choice list. It will also bind the delete row button 
     * that is created with the row to the delete row function.
     */
    @FXML
    private void addChoice() {
        CheckBox checkbox = new CheckBox("");
        choices.add(checkbox);
        
        TextField text = new TextField();
        text.setPromptText("Enter Choice Text");
        
        Button deleteRowButton = new Button();
        deleteRowButton.setText("-");
        deleteRowButton.setOnAction(this::deleteRow);
        deleteButtons.add(deleteRowButton);
        
        checkDeleteButtons();
        
        answerGrid.add(checkbox, 0, rowSize);
        answerGrid.add(text, 1, rowSize);
        answerGrid.add(deleteRowButton, 2, rowSize++);
        addChoiceButton.setTranslateY(27 * rowSize);
    }
    
    /**
     * The function called when a user presses the delete row button. It 
     * deletes the row and moves subsequent rows up.
     * 
     * @param event The ActionEvent that occured
     */
    private void deleteRow(ActionEvent event) {
        Button b = (Button) event.getSource();
        int indexFound = -1;
        
        if (deleteButtons.size() > 2) {
            for (int i = 0; i < deleteButtons.size(); i++) {
                if (deleteButtons.get(i).equals(b) || indexFound != -1) {
                    // Remove to-be-deleted row
                    if (indexFound == -1) {
                        for (int j = 0; j < columnSize; j++) {
                            answerGrid.getChildren().remove(GridUtil.getNodeByRowColumnIndex(i, j, answerGrid));
                        }
                        
                        indexFound = i;
                        --rowSize;
                        addChoiceButton.setTranslateY(27 * rowSize);
                    }
    
                    // Shift lower rows up
                    for (int j = 0; j < columnSize && i < deleteButtons.size() - 1; j++) {
                        Node n = GridUtil.getNodeByRowColumnIndex(i + 1, j, answerGrid);
                        answerGrid.getChildren().remove(GridUtil.getNodeByRowColumnIndex(i + 1, j, answerGrid));
                        answerGrid.add(n, j, i);
                    }
                }
            }
            deleteButtons.remove(indexFound);
        } 
        checkDeleteButtons();
    }
    
    /**
     * Helper function to disable and re-enable delete buttons. It disables 
     * the delete buttons if there are only two choices, preventing the user 
     * from having less than two choices for a multiple response question. It 
     * re-enables them if there are more than two choices.
     */
    private void checkDeleteButtons() {
        if (deleteButtons.size() <= 2) {
            for (int i = 0; i < deleteButtons.size(); i++)
                deleteButtons.get(i).setDisable(true);
        } else if (deleteButtons.size() == 3) {
            for (int i = 0; i < deleteButtons.size(); i++)
                deleteButtons.get(i).setDisable(false);
        }
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a MR question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(MultipleResponseQuestion question) {
        setPrompt(question.getPrompt());
        setChoices(question.getChoices());
        setCorrectAnswer(question.getChoices(), question.getCorrectAnswerIndices());
    }
    
    /**
     * For Question Overview. Sets the question prompt to p.
     * 
     * @param p The question prompt to be set
     */
    private void setPrompt(String p) {
        prompt.setText(p);
    }
    
    /**
     * For Question Overview. Given a row index and the text for the choice, 
     * sets the specified row's choice text to the given one.
     * 
     * @param row The row index
     * @param choice The desired choice text
     */
    private void editRow(int row, String choice) {
        if (row < rowSize) {
            Node n = GridUtil.getNodeByRowColumnIndex(row, 1, answerGrid);
            TextField text = (TextField) n;
            text.setText(choice);
        }
    }
    
    /**
     * For Question Overview. Given a list of choices, populate the choice 
     * list with them.
     * 
     * @param choices The list of choices to be populated
     */
    private void setChoices(ArrayList<String> choices) {
        for (int i = 0; i < choices.size(); i++) {
            if (i+1 > rowSize)
                addChoice();
            
            editRow(i, choices.get(i));
        }
    }
    
    /**
     * For Question Overview. Given a list of choices and correct indices, 
     * matches each question with the correct letter answer.
     * 
     * @param choices The list of choices available
     * @param correctIndices The list of correct indices for each question
     */
    private void setCorrectAnswer(ArrayList<String> choices, ArrayList<Integer> correctIndices) {
        for (int i = 0; i < rowSize; i++) {
            Node n = GridUtil.getNodeByRowColumnIndex(i, 1, answerGrid);
            TextField text = (TextField) n;
            
            for (int j = 0; j < correctIndices.size(); j++) {
                String choice = choices.get(correctIndices.get(j));
                if (text.getText().equals(choice)) {
                    Node node = GridUtil.getNodeByRowColumnIndex(i, 0, answerGrid);
                    CheckBox button = (CheckBox) node;
                    
                    button.setSelected(true);
                }
            }
        }
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() {
        return prompt.getText();
    }
    
    /**
     * Gets the choices from the dialog.
     * 
     * @return The list of choices
     */
    public ArrayList<String> getChoices() {
        ArrayList<String> choicesText = new ArrayList<String>();
        
        for (int i = 0; i < rowSize; i++) {
            String choice = ((TextField) GridUtil.getNodeByRowColumnIndex(
                    i, choiceColumnIndex, answerGrid)).getText();
            choicesText.add(choice);
        }
        
        return choicesText;
    }

    /**
     * Gets the correct answer indices for each question from the dialog.
     * 
     * @return The list of correct answer indices
     */
    public ArrayList<Integer> getCorrectAnswerIndices() {
        ArrayList<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < choices.size(); i++) {
            if (choices.get(i).isSelected()) {
                indices.add(i);
            }
        }
        return indices;
    }
}