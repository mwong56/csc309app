package tt.controller.professor.questions;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import tt.model.professor.questions.MatchingQuestion;
import tt.util.GridUtil;

/**
 * Controls the M question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a M question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionMFormController {
    /** Choices label */
    @FXML
    private Text choicesLabel;
    
    /** Question prompt */
    @FXML
    private TextArea prompt;
    
    /** Question grid */
    @FXML
    private GridPane questionGrid;
    
    /** Answer grid */
    @FXML
    private GridPane choiceGrid;
    
    /** Question's '+' Button */
    @FXML
    private Button addQuestionButton;
    
    /** Answer's '+' Button */
    @FXML
    private Button addChoiceButton;
    
    /** Number of questions */
    private int numQuestions;
    
    /** Number of choices */
    private int numChoices;
    
    /** Question's number of columns */
    private final int questionColumnSize = 4;
    
    /** Choice's number of columns */
    private final int choiceColumnSize = 3;
    
    /** Tracking deleteQuestionButtons */
    private ArrayList<Button> deleteQuestionButtons;
    
    /** Tracking deleteChoiceButtons */
    private ArrayList<Button> deleteChoiceButtons;
    
    /** Tracking numbering */
    private Integer curNumber;
    
    /** Tracking choice letter */
    private char curLetter;
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. Initializes the list of delete 
     * buttons and the current number and letter for a question or choice. It 
     * also adds two default empty questions and choices because users should 
     * not be able to add less than that amount for each.
     */
    @FXML
    private void initialize() {
        numQuestions = numChoices = 0;
        
        deleteQuestionButtons = new ArrayList<Button>();
        deleteChoiceButtons = new ArrayList<Button>();
        
        curNumber = 1;
        curLetter = 'A';
        
        addQuestion();
        addQuestion();
        
        addChoice();
        addChoice();
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Adds a question to the question row. It also binds the delete row 
     * function with the delete row button that is being added and checks if 
     * the user can or cannot delete anymore rows.
     */
    @FXML
    private void addQuestion() {
        Label number = new Label((curNumber++) + ")");
        
        TextField text = new TextField();
        text.setPromptText("Enter Question");
        
        TextField ans = new TextField();
        ans.setOnKeyTyped(maxLength(1));
        
        Button deleteRowButton = new Button();
        deleteRowButton.setText("-");
        deleteRowButton.setOnAction(this::deleteRow);
        deleteQuestionButtons.add(deleteRowButton);
        
        checkDeleteButtons(deleteQuestionButtons);
        
        questionGrid.add(number, 0, numQuestions);
        questionGrid.add(text, 1, numQuestions);
        questionGrid.add(ans, 2, numQuestions);
        questionGrid.add(deleteRowButton, 3, numQuestions++);
        addQuestionButton.setTranslateY(27 * numQuestions);
        
        choicesLabel.setTranslateY(27 * numQuestions);
        choiceGrid.setTranslateY(27 * numQuestions);
        addChoiceButton.setTranslateY(27 * (numChoices + numQuestions));
    }
    
    /**
     * Adds a choice to the choice row. It also binds the delete row 
     * function with the delete row button that is being added and checks if 
     * the user can or cannot delete anymore rows.
     */
    @FXML
    private void addChoice() {
        Label letter = new Label((curLetter++) + ")");
        
        TextField text = new TextField();
        text.setPromptText("Enter Choice Text");
        
        Button deleteRowButton = new Button();
        deleteRowButton.setText("-");
        deleteRowButton.setOnAction(this::deleteRow);
        deleteChoiceButtons.add(deleteRowButton);
        
        checkDeleteButtons(deleteChoiceButtons);
        
        choiceGrid.add(letter, 0, numChoices);
        choiceGrid.add(text, 1, numChoices);
        choiceGrid.add(deleteRowButton, 2, numChoices++);
        addChoiceButton.setTranslateY(27 * (numChoices + numQuestions));
    }
    
    /**
     * Deletes the specified row from either question or choice list and shifts 
     * subsequence rows up. It also updates numbering or lettering to reflect 
     * the row's new position.
     * 
     * @param event The action event that occurred
     */
    private void deleteRow(ActionEvent event) {
        Button b = (Button) event.getSource();
        GridPane grid = (GridPane) b.getParent();
        boolean isQuestionGrid = grid.equals(questionGrid);
        ArrayList<Button> deleteButtons = (
                isQuestionGrid ? deleteQuestionButtons : deleteChoiceButtons);
        int columnSize = (
                isQuestionGrid ? questionColumnSize : choiceColumnSize);
        int indexFound = -1;
        
        if (deleteButtons.size() > 2) {
            for (int i = 0; i < deleteButtons.size(); i++) {
                if (deleteButtons.get(i).equals(b) || indexFound != -1) {
                    // Remove to-be-deleted row
                    if (indexFound == -1) {
                        for (int j = 0; j < columnSize; j++) {
                            grid.getChildren().remove(
                                    GridUtil.getNodeByRowColumnIndex(i, j, grid));
                        }
                        
                        indexFound = i;
                        if (isQuestionGrid) {
                            addQuestionButton.setTranslateY(27 * (--numQuestions));
                            choicesLabel.setTranslateY(27 * numQuestions);
                            choiceGrid.setTranslateY(27 * numQuestions);
                            --curNumber;
                        }
                        else {
                            --numChoices;
                            --curLetter;
                        }
                        
                        addChoiceButton.setTranslateY(27 * (numChoices + numQuestions));
                    }
    
                    // Shift lower rows up
                    for (int j = 0; j < columnSize && i < deleteButtons.size() - 1; j++) {
                        Node n = GridUtil.getNodeByRowColumnIndex(i + 1, j, grid);
                        
                        if (j == 0) {
                            ((Label) n).setText((isQuestionGrid ? 
                                    i+1 : String.valueOf((char) ('A'+i))) + ")");
                        }
                        grid.getChildren().remove(
                                GridUtil.getNodeByRowColumnIndex(i + 1, j, grid));
                        grid.add(n, j, i);
                    }
                }
            }
            deleteButtons.remove(indexFound);
        }
        checkDeleteButtons(deleteButtons);
    }
    
    /**
     * Helper function to disable and re-enable delete buttons. It disables 
     * the delete buttons if there are only two, preventing the user 
     * from having less than two rows for a matching question. It 
     * re-enables them if there are more than two rows.
     * 
     * @param deleteButtons The list of delete buttons to check
     */
    private void checkDeleteButtons(ArrayList<Button> deleteButtons) {
        if (deleteButtons.size() <= 2) {
            for (int i = 0; i < deleteButtons.size(); i++)
                deleteButtons.get(i).setDisable(true);
        } else if (deleteButtons.size() == 3) {
            for (int i = 0; i < deleteButtons.size(); i++)
                deleteButtons.get(i).setDisable(false);
        }
    }
    
    /**
     * Limits the textfield's text input to one letter.
     * 
     * @param i The max length of the text input
     * @return An event handler that consumes the input if the text's input is 
     * greater than one
     */
    public EventHandler<KeyEvent> maxLength(final Integer i) {
        return new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                String tx = ((TextField) arg0.getSource()).getText();
                if (tx.length() >= i) {
                    arg0.consume();
                }
            }
        };
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a M question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(MatchingQuestion question) {
        setPrompt(question.getPrompt());
        setChoices(question.getChoices());
        setQuestions(question.getQuestions(), question.getCorrectAnswer());
    }
    
    private void setPrompt(String p) {
        prompt.setText(p);
    }
    
    /**
     * For Question Overview. Sets the question prompt to p.
     * 
     * @param p The question prompt to be set
     */
    private String findAnswerLetter(ArrayList<String> answers, String ans) {
        for (int i = 0; i < answers.size(); i++) {
            if (answers.get(i).equals(ans))
                return String.valueOf(((char) ('A' + i)));
        }
        return "";
    }
    
    /**
     * For Question Overview. Given a row index, the text for the choice,
     * and the answer, sets the specified row inputs to the given 
     * information.
     * 
     * @param row The row index
     * @param question The desired question text
     * @param answer The correct answer for the question
     */
    private void editQuestionRow(int row, String question, String answer) {
        if (row < numQuestions) {
            Node n = GridUtil.getNodeByRowColumnIndex(row, 1, questionGrid);
            TextField text = (TextField) n;
            text.setText(question);
            
            n = GridUtil.getNodeByRowColumnIndex(row, 2, questionGrid);
            text = (TextField) n;
            text.setText(answer);
        }
    }
    
    /**
     * For Question Overview. Given a list of questions and answers, populate 
     * the question rows with them.
     * 
     * @param questions The list of questions to be set
     * @param answers The list of answers to be set
     */
    private void setQuestions(ArrayList<String> questions, ArrayList<String> answers) {
        for (int i = 0; i < questions.size(); i++) {
            if (i+1 > numQuestions)
                addQuestion();
            
            editQuestionRow(i, questions.get(i), findAnswerLetter(answers, answers.get(i)));
        }
    }
    
    /**
     * For Question Overview. Given a row index and the text for the choice, 
     * sets the specified row's choice text to the given one.
     * 
     * @param row The row index
     * @param choice The desired choice text
     */
    private void editChoiceRow(int row, String choice) {
        if (row < numChoices) {
            Node n = GridUtil.getNodeByRowColumnIndex(row, 1, choiceGrid);
            TextField text = (TextField) n;
            text.setText(choice);
        }
    }
    
    /**
     * For Question Overview. Given a list of choices, populate the choice 
     * list with them.
     * 
     * @param choices The list of choices to be populated
     */
    private void setChoices(ArrayList<String> choices) {
        for (int i = 0; i < choices.size(); i++) {
            if (i+1 > numChoices)
                addChoice();
            
            editChoiceRow(i, choices.get(i));
        }
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() {
        return prompt.getText();
    }
    
    /**
     * Gets the questions from the dialog.
     * 
     * @return The list of questions
     */
    public ArrayList<String> getQuestions() {
        ArrayList<String> questions = new ArrayList<String>();
        
        for (int i = 0; i < numQuestions; i++) {
            String question = ((TextField) GridUtil.getNodeByRowColumnIndex(i, 1, questionGrid)).getText();
            questions.add(question);
        }
        
        return questions;
    }
    
    /**
     * Gets the choices from the dialog.
     * 
     * @return The list of choices
     */
    public ArrayList<String> getChoices() {
        ArrayList<String> choices = new ArrayList<String>();
        
        for (int i = 0; i < numChoices; i++) {
            String choice = ((TextField) GridUtil.getNodeByRowColumnIndex(i, 1, choiceGrid)).getText();
            choices.add(choice);
        }
        
        return choices;
    }

    /**
     * Gets the correct answers for each question from the dialog
     * 
     * @return The list of correct answers
     */
    public ArrayList<String> getCorrectAnswers() {
        ArrayList<String> answers = new ArrayList<String>();
        
        for (int i = 0; i < numQuestions; i++) {
            String ans = ((TextField) GridUtil.getNodeByRowColumnIndex(i, 2, questionGrid)).getText();
            answers.add(ans);
        }
        
        return answers;
    }
}