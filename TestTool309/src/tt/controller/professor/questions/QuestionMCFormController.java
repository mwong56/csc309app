package tt.controller.professor.questions;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import tt.model.professor.questions.MultipleChoiceQuestion;
import tt.util.GridUtil;

/**
 * Controls the MC question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a MC question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionMCFormController {
    /** Question Prompt */
    @FXML
    private TextArea prompt;
    
    /** GridPane for answer list */
    @FXML
    private GridPane answerGrid;
    
    /** + Button */
    @FXML
    private Button addChoiceButton;
    
    /** Answer */
    @FXML
    private ToggleGroup answer;
    
    /** Number of rows */
    private int rowSize;
    
    /** Number of columns */
    private final int columnSize = 3;
    
    /** Tracking deleteButtons */
    private ArrayList<Button> deleteButtons;
    
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. It adds two default empty choices 
     * and initializes variables.
     */
    @FXML
    private void initialize() {
        answer = new ToggleGroup();
        rowSize = 0;
        deleteButtons = new ArrayList<Button>();
        
        addChoice();
        addChoice();
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Adds a row to the choice list. It will also bind the delete row button 
     * that is created with the row to the delete row function.
     */
    @FXML
    private void addChoice() {
        RadioButton button = new RadioButton();
        button.setToggleGroup(answer);
        
        TextField text = new TextField();
        text.setPromptText("Enter Choice Text");
        
        Button deleteRowButton = new Button();
        deleteRowButton.setText("-");
        deleteRowButton.setOnAction(this::deleteRow);
        deleteButtons.add(deleteRowButton);
        
        checkDeleteButtons();
        
        answerGrid.add(button, 0, rowSize);
        answerGrid.add(text, 1, rowSize);
        answerGrid.add(deleteRowButton, 2, rowSize++);
        addChoiceButton.setTranslateY(27 * rowSize);
    }
    
    /**
     * The function called when a user presses the delete row button. It 
     * deletes the row and moves subsequent rows up.
     * 
     * @param event The ActionEvent that occured
     */
    private void deleteRow(ActionEvent event) {
        Button b = (Button) event.getSource();
        int indexFound = -1;
        
        if (deleteButtons.size() > 2) {
            for (int i = 0; i < deleteButtons.size(); i++) {
                if (deleteButtons.get(i).equals(b) || indexFound != -1) {
                    // Remove to-be-deleted row
                    if (indexFound == -1) {
                        for (int j = 0; j < columnSize; j++) {
                            answerGrid.getChildren().remove(
                                    GridUtil.getNodeByRowColumnIndex(i, j, answerGrid));
                        }
                        
                        indexFound = i;
                        --rowSize;
                        addChoiceButton.setTranslateY(27 * rowSize);
                    }
    
                    // Shift lower rows up
                    for (int j = 0; j < columnSize && i < deleteButtons.size() - 1; j++) {
                        Node n = GridUtil.getNodeByRowColumnIndex(i + 1, j, answerGrid);
                        answerGrid.getChildren().remove(
                                GridUtil.getNodeByRowColumnIndex(i + 1, j, answerGrid));
                        answerGrid.add(n, j, i);
                    }
                }
            }
            deleteButtons.remove(indexFound);
        }
        checkDeleteButtons();
    }
    
    /**
     * Helper function to disable and re-enable delete buttons. It disables 
     * the delete buttons if there are only two choices, preventing the user 
     * from having less than two choices for a multiple choice question. It 
     * re-enables them if there are more than two choices.
     */
    private void checkDeleteButtons() {
        if (deleteButtons.size() <= 2) {
            for (int i = 0; i < deleteButtons.size(); i++)
                deleteButtons.get(i).setDisable(true);
        } else if (deleteButtons.size() == 3) {
            for (int i = 0; i < deleteButtons.size(); i++)
                deleteButtons.get(i).setDisable(false);
        }
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a MC question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(MultipleChoiceQuestion question) {
        setPrompt(question.getPrompt());
        setChoices(question.getChoices());
        setCorrectAnswer(question.getCorrectAnswer());
    }
    
    /**
     * For Question Overview. Sets the question prompt to p.
     * 
     * @param p The question prompt to be set
     */
    private void setPrompt(String p) {
        prompt.setText(p);
    }
    
    /**
     * For Question Overview. Given a row index and the text for the choice, 
     * sets the specified row's choice text to the given one.
     * 
     * @param row The row index
     * @param choice The desired choice text
     */
    private void editRow(int row, String choice) {
        if (row < rowSize) {
            Node n = GridUtil.getNodeByRowColumnIndex(row, 1, answerGrid);
            TextField text = (TextField) n;
            text.setText(choice);
        }
    }
    
    /**
     * For Question Overview. Given a list of choices, populate the choice 
     * list with them.
     * 
     * @param choices The list of choices to be populated
     */
    private void setChoices(ArrayList<String> choices) {
        for (int i = 0; i < choices.size(); i++) {
            if (i+1 > rowSize)
                addChoice();
            
            editRow(i, choices.get(i));
        }
    }
    
    /**
     * For Question Overview. Sets the correct answer to ans, which should be 
     * a letter.
     * 
     * @param ans The correct answer to be set
     */
    private void setCorrectAnswer(String ans) {
        for (int i = 0; i < rowSize; i++) {
            Node n = GridUtil.getNodeByRowColumnIndex(i, 1, answerGrid);
            TextField text = (TextField) n;
            if (text.getText().equals(ans)) {
                Node node = GridUtil.getNodeByRowColumnIndex(i, 0, answerGrid);
                RadioButton button = (RadioButton) node;
                
                button.setSelected(true);
            }
        }
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() {
        return prompt.getText();
    }
    
    /**
     * Gets the choices from the dialog.
     * 
     * @return The list of choices
     */
    public ArrayList<String> getChoices() {
        ArrayList<String> choices = new ArrayList<String>();
        
        for (int i = 0; i < rowSize; i++) {
            String choice = ((TextField) GridUtil.getNodeByRowColumnIndex(i, 1, answerGrid)).getText();
            choices.add(choice);
        }
        
        return choices;
    }

    /**
     * Gets the correct answer index.
     * 
     * @return The correct answer index
     */
    public int getCorrectAnswerIndex() {
        try {
            return answerGrid.getRowIndex((RadioButton) answer.getSelectedToggle());
        } catch(java.lang.NullPointerException npe) {
                return -1;
        }
    }
}