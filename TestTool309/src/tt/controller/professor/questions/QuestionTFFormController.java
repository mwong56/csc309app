package tt.controller.professor.questions;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import tt.model.professor.questions.TrueFalseQuestion;

/**
 * Controls the TF question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a TF question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionTFFormController {
    /** Question Prompt */
    @FXML
    private TextArea prompt;
    
    /** Answer */
    @FXML
    private ToggleGroup answer;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a TF question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(TrueFalseQuestion question) {
        setPrompt(question.getPrompt());
        setCorrectAnswer(question.getCorrectAnswer());
    }
    
    /**
     * For Question Overview. Sets the question prompt to p.
     * 
     * @param p The question prompt to be set
     */
    private void setPrompt(String p) {
        prompt.setText(p);
    }
    
    /**
     * For Question Overview. Sets the correct answer to ans.
     * 
     * @param ans The answer to be set
     */
    private void setCorrectAnswer(boolean ans) {
        ObservableList<Toggle> toggles = answer.getToggles();
        for (int i = 0; i < toggles.size(); i++) {
            RadioButton button = (RadioButton) toggles.get(i);
            
            if ((ans && button.getText().equals("True")) || 
                    (!ans && button.getText().equals("False")))
                button.setSelected(true);
        }
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() {
        return prompt.getText();
    }

    /**
     * Gets the correct answer from the dialog.
     * 
     * @return The correct answer
     */
    public String getCorrectAnswer() {
        try {
            return ((RadioButton) answer.getSelectedToggle()).getText();
        } catch(java.lang.NullPointerException npe) {
            return null;
        }
    }
}