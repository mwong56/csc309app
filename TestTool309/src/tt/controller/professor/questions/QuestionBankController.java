package tt.controller.professor.questions;

import tt.MainApp;
import tt.model.professor.questions.Question;
import tt.model.professor.questions.QuestionBank;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;

/**
 * Controls what goes on in the Question Bank view. It shows the professor's 
 * question bank via a list of saved questions. The professor can click on 
 * each row to see an overview of each question. They can also add or delete 
 * questions from the question bank here, or sort their question bank by 
 * class, question type, or question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class QuestionBankController {

    /** A QuestionBank table */
    @FXML
    private TableView<Question> QuestionBankTable;

    /** A QuestionBank column called class */
    @FXML
    private TableColumn<Question, String> classColumn;

    /** A QuestionBank column called type */
    @FXML
    private TableColumn<Question, String> typeColumn;

    /** A QuestionBank column called question */
    @FXML
    private TableColumn<Question, String> questionPromptColumn;

    /** Reference to the main application. */
    private MainApp mainApp;

    /** Reference to question bank */
    private QuestionBank questionBank;
    
    /** Counts how many times we enter the questionOverview */
    private int numEnters;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. The table's columns are 
     * initialized here and each column is linked to a specific information 
     * bit of a question.
     */
    @FXML
    private void initialize() {
        /** Initialize the question table with the three columns. */
        classColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .classNameProperty());
        typeColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .typeProperty());
        questionPromptColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .promptProperty());

        /**
         * Listen for selection changes and show the question overview when 
         * changed.
         */
         QuestionBankTable.getSelectionModel().selectedItemProperty().addListener(
         (observable, oldValue, newValue) -> showQuestionDetails(newValue));
         
         numEnters = 0;
    }
    
    /**
     * This shows the selected question's information. If the user decided to 
     * update (edit) the question, a new question will be returned and the 
     * question bank will be updated with the old question replaced by the new. 
     * 
     * @param question The selected question
     */
    private void showQuestionDetails(Question question) {
        numEnters++;

        /**
         * Need to perform this code later because of conflicting JavaFX 
         * ObservableList changes and modifying the list.
         */
        Platform.runLater(new Runnable() {
            @Override public void run() {
                /**
                 * For some reason, the question overview dialog shows up
                 * twice. I believe it's because I clear the selection, 
                 * thus changing the question selected and it shows again.
                 */
            if (numEnters % 2 == 1) {
                QuestionBankTable.getSelectionModel().clearSelection();
                Question new_question = mainApp.showQuestionDetails(question);
                
                if (new_question != null) {
                    questionBank.updateQuestion(question, new_question);
                }
            }
        }});
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp and questionBank isn't null
                //
                this.mainApp != null
                
                    &&
                
                this.questionBank != null;
     * 
     * @param mainApp The MainApp to be set
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        this.questionBank = mainApp
                .getQuestionBank();
        questionBank.setMainApp(mainApp);

        // Add observable list data to the table
        QuestionBankTable.setItems(questionBank
                .getQuestionData());
    }

    /**
     * Indicates that the main app should now show the Add Question dialog.
     * 
      <pre>
           post:
                //
                // The question is added iff okClicked
                //
                if (okClicked)
      questionBank'.contains(q);
     */
    @FXML
    private void showAddQuestionDialog() {
        mainApp.showAddQuestionDialog();
    }

    /**
     * Deletes the selected question from QuestionBankDB. The given question
     * must already be in the question bank. There also must be a question 
     * in the question bank and a question selected.
     * 
      <pre>
            pre:
                  //
                  // The question is contained in QuestionBankDB
                  //
                  questionBank.contains(q);
          
            post:
                  //
                  // A question is in the output data iff it isn't the 
                  // existing question to be deleted and it is in the 
                  // input data
                  //
                  forall ( Question q_other ;
                  questionBank'.contains(ur_other) iff
      !q_other.equals(q) && data.contains(q_other));
     */
    @FXML
    private void handleDelete() {
        int selectedIndex = QuestionBankTable
                .getSelectionModel()
                .getSelectedIndex();
        if (selectedIndex >= 0) {
            questionBank.delete(selectedIndex);
        }
        else {
            // Nothing selected.
            Alert alert = new Alert(
                    AlertType.WARNING);
            alert.initOwner(mainApp
                    .getPrimaryStage());

            if (questionBank.getQuestionData()
                    .size() > 0) {
                alert.setTitle("No Selection");
                alert.setHeaderText("No Question Selected");
                alert.setContentText("Please select a question in the table to delete.");
            }
            else {
                alert.setTitle("No Questions");
                alert.setHeaderText("No Questions in Question Bank");
                alert.setContentText("Please create a question to delete.");
            }
            alert.showAndWait();
        }
    }
}
