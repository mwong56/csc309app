package tt.controller.professor.questions;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import tt.model.professor.questions.QuestionBank;

/**
 * Controls the Add Class dialog. Users can add a class (course) from the Add 
 * Question dialog. Users must input a string in order to add a class. 
 * Otherwise, it will show an error.
 * 
 * This class is slightly deprecated. We now just fetch the courses using 
 * the Professor's data. Each professor has a list of courses they teach. 
 * However, we decided to keep this functionality in if they wanted to add 
 * a random course for the duration of the opened program.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class AddClassDialogController {

    /** Class name */
    @FXML
    private TextField classNameField;

    /** Reference to the dialog stage */
    private Stage dialogStage;

    /** Reference to the question bank */
    private QuestionBank questionBank;

    /** Flag to indicate whether "Add" has been clicked */
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     * 
      <pre>
            pre:
                //
                // dialogStage isn't null
                //
                dialogStage != null;
                
            post:
                //
                // this.dialogStage' isn't null and equals dialogStage
                //
                this.dialogStage' != null
                
                    &&
               
                this.dialogStage'.equals(dialogStage);
     * 
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the question bank.
     * 
      <pre>
            pre:
                //
                // qb isn't null
                //
                qb != null;
               
            post:
                //
                // this.questionBank' isn't null and equals qb
                //
                this.questionBank' != null
                
                    &&
               
                this.questionBank'.equals(qb);
      
     * @param qb
     */
    public void setQuestionBank(QuestionBank qb) {
        this.questionBank = qb;
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     * 
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     * 
      <pre>
            pre:
                //
                // questionBank's class data isn't null
                //
      questionBank.getClassData() != null;
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            questionBank.getClassData().add(
                    classNameField.getText());

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     * 
      <pre>
            pre:
                //
                // dialogStage isn't null
                //
      dialogStage != null;
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Checking if class name is valid
     * 
      <pre>
            pre:
                //
                // classNameField isn't null and empty.
                //
                classNameField != null && classNameField.length() > 0;
      
     * @return
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (classNameField.getText() == null
                || classNameField.getText()
                        .length() == 0) {
            errorMessage += "No valid class name!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        }
        else {
            /** Show the error message. */
            Alert alert = new Alert(
                    AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}