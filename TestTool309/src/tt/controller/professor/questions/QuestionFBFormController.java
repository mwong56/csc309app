package tt.controller.professor.questions;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import tt.model.professor.questions.FillInTheBlankQuestion;

/**
 * Controls the FB question's specific form. When a professor adds a question,
 * they must specify what kind of question should be made. If they selected in 
 * "Question Type" that they want to create a FB question, its question's
 * view will be shown in the Add Question dialog.
 * 
 * This controller is given to the Add Question dialog controller so that it 
 * can retrieve all the data from this one. Together, they form one full 
 * question.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class QuestionFBFormController {
    /** Question Prompt */
    @FXML
    private TextArea prompt;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }
    
    /***************************
     * SETTERS
     ***************************/
    
    /**
     * For Question Overview. Given a FB question, it shows the specific 
     * details of it.
     * 
     * @param question The question to be shown (by extracting information)
     */
    public void setQuestionInfo(FillInTheBlankQuestion question) {
        prompt.setText(this.parsePrompt(question));
    }
    
    /**
     * Parses question prompt using regex to find blanks. If there are any 
     * blanks, we fill it with the correct answer.
     * 
     * @param question The FB question to be parsed
     */
    private String parsePrompt(FillInTheBlankQuestion question) {
        int index = 0;
        String p = question.getPrompt();
        String regex = "_________";
        ArrayList<String> correctAnswers = question.getCorrectAnswers();

        while (index < correctAnswers.size())
            p = p.replaceFirst(regex, "[[" + correctAnswers.get(index++) + "]]");

        return p;
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the prompt from the dialog.
     * 
     * @return The prompt in the form of a String
     */
    public String getPrompt() {
        return prompt.getText();
    }
}
