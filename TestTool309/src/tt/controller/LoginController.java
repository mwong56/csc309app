package tt.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import tt.MainApp;
import tt.model.professor.ProfessorDB;
import tt.model.student.StudentDB;

/**
 * Controller class to handle login authentication
 * 
 * @author Dat Tran
 *
 */

public class LoginController {

    /** The username text field */
    @FXML
    private TextField username;

    /** The password text field */
    @FXML
    private TextField password;

    /** Reference to the main application. */
    private MainApp mainApp;

    /** Reference to the database of professors */
    private ProfessorDB profDB;

    /** Reference to the database of students */
    private StudentDB studDB;

    /** Flag to indicate whether the user selected to login as a student */
    private boolean selectedStudent;

    /**
     * Default constructor
     */
    public LoginController() {
        studDB = new StudentDB();
        profDB = new ProfessorDB();
        selectedStudent = true;
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp isn't null
                //
                this.mainApp != null
     * 
     * @param mainApp The mainApp to refer to
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Performs authentication
     * 
      <pre>
            pre:
                //
                // selectedStudent, studDB, and profDB aren't null
                //
                selectedStudent != null 
                        &&
                studDB != null
                        &&
                profDB != null
     * 
     * 
     */
    @FXML
    private void login() {
        if (selectedStudent) {
            if (studDB.authenticate(
                    username.getText(),
                    password.getText())) {
                mainApp.setStudent(studDB.getStudent(username.getText()));
                mainApp.showStudentOverview();
            }
            else {
                Alert alert = new Alert(
                        AlertType.ERROR);
                alert.setTitle("Incorrect Information");
                alert.setHeaderText("Incorrect username or password");

                alert.showAndWait();
            }
        }
        else {
            if (profDB.authenticate(
                    username.getText(),
                    password.getText())) {
                mainApp.setProfessor(profDB.getProfessor(username.getText()));
                mainApp.showProfessorOverview();
            }
            else {
                Alert alert = new Alert(
                        AlertType.ERROR);
                alert.setTitle("Incorrect Information");
                alert.setHeaderText("Incorrect username or password");

                alert.showAndWait();
            }
        }
    }

    /**
     * Turns on the flag if the user selects to login as a student
     * 
     * 
      <pre>
            pre:
                //
                // selectedStudent isn't null
                //
                selectedStudent != null;
                
            post:
                // 
                // selectedStudent is true
                //
                selectedStudent' == true;
     * 
     * 
     */
    public void isStudent() {
        selectedStudent = true;
    }

    /**
     * Turns off the flag if the user selects to login as a professor
     *
     * 
      <pre>
            pre:
                //
                // selectedStudent isn't null
                //
                selectedStudent != null;
                
            post:
                // 
                // selectedStudent is false
                //
                selectedStudent' == false;
     * 
     *
     */
    public void isProfessor() {
        selectedStudent = false;
    }

    /**
     * Attempts to authenticate whenever the enter key is pressed
     *
     * 
      <pre>
            pre:
                //
                // event isn't null
                //
                event != null;
     * 
     * @param event The event the indicates that a keystroke has occurred
     */
    @FXML
    private void handleEnterPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            login();
        }
    }
}
