package tt.controller.student;

import javafx.fxml.FXML;
import tt.MainApp;

/**
 * StudentOverview controller class
 * 
 * @author Dat Tran
 *
 */
public class StudentOverviewController {

    /** Reference to the main application. */
    private MainApp mainApp;

    /**
     * Default constructor
     */
    public StudentOverviewController() {
        mainApp = new MainApp();
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp and questionBank isn't null
                //
                this.mainApp != null
                
                    &&
                
                this.questionBank != null;
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

    }

    /**
     * Indicates that the main app should now show the review overview screen.
     */
    @FXML
    private void showGradingOverview() {
        mainApp.showGradingOverview();
    }

    /**
     * Indicates that the main app should now show the take overview screen.
     */
    @FXML
    private void showAvailableTests() {
        mainApp.showAvailableTests();
    }
}
