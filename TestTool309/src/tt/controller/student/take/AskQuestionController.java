package tt.controller.student.take;

import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import tt.MainApp;
import tt.model.Course;
import tt.model.professor.create.Test;
import tt.model.professor.proctor.StudentQuestion;

/**
 * This controller handles the question dialog when users want to ask a question to the professor.
 * @author Michael
 *
 */
public class AskQuestionController {

    // The question the user types.
    @FXML
    private TextField question;
    
    // The test that the user is taking
    private Test test;
    
    // Current dialog stage
    private Stage dialogStage;
    
    // Current mainApp reference.
    private MainApp mainApp;
    
    // Initialize this controller, do nothing.
    @FXML
    private void initiliaze() {
        return;
    }
    
    /*
     * Setter for test
     */
    public void setTest(Test test) {
        this.test = test;
    }
    
    /**
     * Setter for dialog stage.
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    
    /**
     * Setter for mainApp.
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    /**
     * Method to handle the OK button.
     */
    @FXML
    public void handleOk() {
        ArrayList<Course> courses = mainApp.getStudent().getCourses();
        Course course = new Course(null, 0, 0, 0);
        for (Course c : courses) {
            String course1 = c.getCourseAbbr() + c.getNumber();
            if (test.getCourse().get().equals(course1))
                course = c;
        }
            
        StudentQuestion toSubmit = new StudentQuestion(question.getText(), course, test, "");
        toSubmit.setTest(test);
        
        mainApp.getStudent().getQuestions().add(toSubmit);
        dialogStage.close();
    }
    
    /**
     * method to handle the Cancel button.
     */
    @FXML
    public void handleCancel() {
        dialogStage.close();
    }
    
}
