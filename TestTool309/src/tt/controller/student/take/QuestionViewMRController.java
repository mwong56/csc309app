package tt.controller.student.take;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import tt.model.professor.questions.MultipleResponseQuestion;
import tt.model.professor.questions.Question;

public class QuestionViewMRController implements
        QuestionViewController {

    @FXML
    private GridPane answerGrid;

    /** Number of rows */
    private int rowSize;

    private MultipleResponseQuestion mrQuestion;

    /** Tracking check boxes */
    private ArrayList<CheckBox> choices;

    private ArrayList<Label> textFields;

    @FXML
    private void initialize() {
        rowSize = 0;
        choices = new ArrayList<CheckBox>();
        textFields = new ArrayList<Label>();
    }

    private void addChoice(String choice,
            Boolean selected) {
        CheckBox checkbox = new CheckBox("");
        choices.add(checkbox);
        checkbox.setSelected(selected);

        Label text = new Label();
        text.setText(choice);
        textFields.add(text);

        answerGrid.add(checkbox, 0, rowSize);
        answerGrid.add(text, 1, rowSize);
        rowSize++;
    }

    @Override
    public void setAnswer(Question question) {
        if (!(question instanceof MultipleResponseQuestion)) {
            System.err
                    .println("Error: expected mutlipe choice question");
            System.exit(0);
        }

        mrQuestion = (MultipleResponseQuestion) question;
        JSONArray array = null;
        try {
            if (question.getAnswer() != null) {
                array = new JSONArray(
                        question.getAnswer());
            }
        }
        catch (JSONException e) {
            System.out
                    .println("No answers saved from question.");
        }

        for (String choice : mrQuestion
                .getChoices()) {
            boolean selected = false;
            if (array != null) {
                for (int i = 0; i < array
                        .length(); i++) {
                    try {
                        if (array.getString(i)
                                .equals(choice)) {
                            selected = true;
                            break;
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            addChoice(choice, selected);
        }
    }
    
    @Override
    public String getAnswer() {
        JSONArray array = new JSONArray();
        for (int i = 0; i < choices.size(); i++) {
            if (choices.get(i).isSelected()) {
                array.put(textFields.get(i)
                        .getText());
            }
        }
        return array.toString();
    }

}
