package tt.controller.student.take;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import tt.MainApp;
import tt.model.professor.create.Test;

/**
 * Controller for the available tests view.
 * 
 * @author Michael
 *
 */
public class AvailableTestsController {

    @FXML
    private TableView<Test> availableTests;
    @FXML
    private TableColumn<Test, String> classColumn;
    @FXML
    private TableColumn<Test, String> profColumn;
    @FXML
    private TableColumn<Test, String> testColumn;
    @FXML
    private TableColumn<Test, String> typeColumn;
    @FXML
    private TableColumn<Test, String> closeColumn;
    @FXML
    private TableColumn<Test, String> limitColumn;
    @FXML
    private TableColumn<Test, String> takeColumn;

    private MainApp mainApp;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        classColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue().getCourse());
        profColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue()
                        .getProfessor());
        testColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue().getTitle());
        typeColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue().getType());
        closeColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue().getClose());
        limitColumn
                .setCellValueFactory(cellData -> cellData
                        .getValue().getLimit());

        availableTests
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue,
                                newValue) -> openTest(newValue));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     * 
                 pre: mainApp != null;
     * 
                 post: this.mainApp == mainApp;
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        availableTests.setItems(mainApp
                .getTestData());
    }

    /**
     * Opens the test selected
     * 
      pre: test != null;
     * 
      post: this.test == test;
     */
    private void openTest(Test test) {
        mainApp.showTest(test);
    }

}
