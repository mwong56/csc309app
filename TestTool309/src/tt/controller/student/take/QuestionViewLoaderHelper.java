package tt.controller.student.take;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import tt.MainApp;
import tt.model.professor.questions.CodeQuestion;
import tt.model.professor.questions.LongAnswerQuestion;
import tt.model.professor.questions.MatchingQuestion;
import tt.model.professor.questions.MultipleChoiceQuestion;
import tt.model.professor.questions.MultipleResponseQuestion;
import tt.model.professor.questions.Question;
import tt.model.professor.questions.ShortAnswerQuestion;
import tt.model.professor.questions.TrueFalseQuestion;

public class QuestionViewLoaderHelper {

    /**
     * Show QuestionForm for specific question type
     */
    public static void showQuestionView(
            TestController parentController,
            Question question) {
        try {
            // Loading Form
            FXMLLoader loader = new FXMLLoader();
            AnchorPane pane = new AnchorPane();

            if (question instanceof TrueFalseQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionTF.fxml"));
                pane = loader.load();
                QuestionViewTrueFalseController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            else if (question instanceof MultipleChoiceQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionMC.fxml"));
                pane = loader.load();
                QuestionViewMCController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            else if (question instanceof MultipleResponseQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionMR.fxml"));
                pane = loader.load();
                QuestionViewMRController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            // TODO: MATCHING QUESTIONS.
            else if (question instanceof MatchingQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/QuestionMForm.fxml"));
                pane = loader.load();
            }
            else if (question instanceof ShortAnswerQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionFillIn.fxml"));
                pane = loader.load();
                QuestionViewFillInController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            else if (question instanceof LongAnswerQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionFillIn.fxml"));
                pane = loader.load();
                QuestionViewFillInController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            else if (question instanceof CodeQuestion) {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionFillIn.fxml"));
                pane = loader.load();
                QuestionViewFillInController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            else {
                loader.setLocation(MainApp.class
                        .getResource("view/TestViewQuestionFillIn.fxml"));
                pane = loader.load();
                QuestionViewFillInController controller = loader
                        .getController();
                parentController
                        .setQuestionController(controller);
            }
            parentController
                    .setQuestionPane(pane);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
