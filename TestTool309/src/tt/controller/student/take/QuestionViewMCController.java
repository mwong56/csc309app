package tt.controller.student.take;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import tt.model.professor.questions.MultipleChoiceQuestion;
import tt.model.professor.questions.Question;

public class QuestionViewMCController implements
        QuestionViewController {

    @FXML
    private GridPane answerGrid;

    /** Number of rows */
    private int rowSize;

    private MultipleChoiceQuestion mcQuestion;

    /** Answer */
    @FXML
    private ToggleGroup answer;

    @FXML
    private void initialize() {
        answer = new ToggleGroup();
        rowSize = 0;
    }

    private void addChoice(String choice,
            Boolean selected) {
        RadioButton button = new RadioButton();
        button.setToggleGroup(answer);
        button.setSelected(selected);

        Label text = new Label();
        text.setText(choice);

        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(1);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(99);
        answerGrid.getColumnConstraints().addAll(
                col1, col2);

        answerGrid.add(button, 0, rowSize);
        answerGrid.add(text, 1, rowSize);
        rowSize++;
    }

    @Override
    public void setAnswer(Question question) {
        if (!(question instanceof MultipleChoiceQuestion)) {
            System.err
                    .println("Error: expected mutlipe choice question");
            System.exit(0);
        }
        mcQuestion = (MultipleChoiceQuestion) question;
        for (String choice : mcQuestion
                .getChoices()) {
            addChoice(choice,
                    choice.equals(question
                            .getAnswer()));
        }
    }

    @Override
    public String getAnswer() {
        if (getCorrectAnswerIndex() < 0) {
            return this.hashCode() + "";
        }
        return mcQuestion.getChoices().get(
                getCorrectAnswerIndex());
    }

    public int getCorrectAnswerIndex() {
        try {
            return GridPane
                    .getRowIndex((RadioButton) answer
                            .getSelectedToggle());
        }
        catch (java.lang.NullPointerException npe) {
            return -1;
        }

    }

}
