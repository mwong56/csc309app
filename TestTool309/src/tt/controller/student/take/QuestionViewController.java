package tt.controller.student.take;

import tt.model.professor.questions.Question;

public interface QuestionViewController {
    
    public void setAnswer(Question question);
    
    public String getAnswer();
}
