package tt.controller.student.take;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import tt.model.professor.questions.Question;

public class QuestionViewFillInController implements
        QuestionViewController {

    @FXML
    private TextArea answerText;

    @FXML
    private void initialize() {
    }
    
    @Override
    public void setAnswer(Question question) {
        answerText.setText(question.getAnswer());
    }

    @Override
    public String getAnswer() {
        return answerText.getText();
    }

}
