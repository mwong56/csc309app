package tt.controller.student.take;

import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import tt.model.professor.questions.Question;
import tt.model.professor.questions.TrueFalseQuestion;

public class QuestionViewTrueFalseController implements QuestionViewController {
    
    @FXML
    private RadioButton choiceTrue;
    
    @FXML
    private RadioButton choiceFalse;
    
    @FXML
    private void initialize() {
        choiceTrue.setSelected(true);
        choiceFalse.setSelected(false);
    }
    
    @Override
    public void setAnswer(Question question) {
        if (question.getAnswer().equals(TrueFalseQuestion.FALSE)) {
            choiceFalse.setSelected(true);
            choiceTrue.setSelected(false);
        } else {
            choiceTrue.setSelected(true);
            choiceFalse.setSelected(false);
        }
    }
    
    @FXML
    private void onTrueSelected() {
        choiceFalse.setSelected(false);
    }
    
    @FXML
    private void onFalseSelected() {
        choiceTrue.setSelected(false);
    }

    @Override
    public String getAnswer() {
        return choiceTrue.isSelected() ? TrueFalseQuestion.TRUE:TrueFalseQuestion.FALSE;
    }
    
}
