package tt.controller.student.take;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import tt.MainApp;
import tt.model.professor.create.Test;
import tt.model.professor.questions.Question;
import tt.model.student.take.TakeTestDB;

/**
 * 
 * Controller for the question view.
 * 
 * @author Michael
 *
 */
public class TestController {

    private MainApp mainApp;
    private Test test;
    private int iter;
    private QuestionViewController questionController;

    @FXML
    private TextArea questionTextArea;

    @FXML
    private TextArea answerTextArea;
    
    @FXML
    private Button nextButton;
    
    @FXML
    private Button prevButton;
    
    @FXML
    private Button questionButton;
    
    @FXML
    private AnchorPane anchorPane;

    @FXML
    private void initialize() {
        iter = 0;
    }

    /**
     * Sets the current view to this text.
     * 
     * @param test
     * 
                 pre: test != null;
     * 
                 post: this.test == test;
     * 
     */
    public void setTest(Test test) {
        this.test = test;
        changeQuestion(iter);
    }
    
    public void setQuestionController(QuestionViewController controller) {
        this.questionController = controller;
    }
    
    /**
     * Changes the question to position <iter> in list.
     * @param iter The position of the question to pull from the test's question list.
     
     pre: test != null && iter > 0
     
     post:  
         questionTextArea.getText().equals(question.getPrompt()) 
         &&   answerTextArea.getText().equals(question.getAnswer());

     */
    private void changeQuestion(int iter) {
        Question question = this.test.getQuestionList().get(iter);
        QuestionViewLoaderHelper.showQuestionView(this, question);
        questionTextArea.setText(question.getPrompt());
        questionController.setAnswer(question);
    }
    
    /**
     * Saves the current question answer we are on.
     
     pre:
         test != null;
         
         post: this.test.getQuestionList().get(iter).getAnswer().equals(answerTextArea.getText())
     */
    public void saveQuestion() {
        this.test.getQuestionList().get(iter).setAnswer(questionController.getAnswer());
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     * 
                 pre: mainApp != null;
     * 
                 post: this.mainApp == mainApp;
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    /**
     * Handles the back button
     */
    private void handleBack() {
        mainApp.showAvailableTests();
    }
    
    @FXML
    /**
     * Handles the next button.
     */
    private void handleNext() {
        saveQuestion();
        if (iter < test.getQuestionList().size() - 1) {
            changeQuestion(++iter);
            if (iter == test.getQuestionList().size() - 1) {
                nextButton.setText("Submit");
            }
        } else {
            TakeTestDB.getInstance().addTest(test, MainApp.stud);
            mainApp.testData.remove(test);
            handleBack();
        }
    }
    
    @FXML
    /**
     * Handles the next button.
     */
    private void handlePrev() {
        saveQuestion();
        if (iter > 0) {
            changeQuestion(--iter);
            if (iter < test.getQuestionList().size() - 1) {
                nextButton.setText("Next");
            }
        }
    }
    
    @FXML
    private void handleQuestion() {
        mainApp.showAskQuestionDialog(test);
    }
    
    public void setQuestionPane(AnchorPane pane) {
        anchorPane.getChildren().clear();
        anchorPane.getChildren().add(pane);
    }
}
