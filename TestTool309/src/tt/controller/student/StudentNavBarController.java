package tt.controller.student;

import javafx.fxml.FXML;
import tt.MainApp;

/**
 * Student Navigation bar controller
 * 
 * @author Dat Tran
 *
 */

public class StudentNavBarController {

    /** Reference to the main application. */
    private MainApp mainApp;

    /**
     * Default constructor
     */
    public StudentNavBarController() {
        mainApp = new MainApp();
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
      <pre>
            pre:
                //
                // mainApp isn't null
                //
                mainApp != null;
                
            post:
                // 
                // Class's mainApp isn't null
                //
                this.mainApp != null
                
     *      
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Indicates that the main app should now show the Question Bank screen.
     */
    @FXML
    private void showStudentOverview() {
        mainApp.showStudentOverview();
    }

    /**
     * Indicates that the main app should now show the available tests screen.
     */
    @FXML
    private void showAvailableTests() {
        mainApp.showAvailableTests();
    }

    /**
     * Indicates that the main app should now show the review screen.
     */
    @FXML
    private void showGradingOverview() {
        mainApp.showGradingOverview();
    }
}
