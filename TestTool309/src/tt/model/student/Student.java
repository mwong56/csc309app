package tt.model.student;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import tt.model.Course;
import tt.model.professor.create.Test;
import tt.model.professor.proctor.StudentQuestion;

/**
 * Student model class to represent a student user
 * 
 * @author Dat Tran
 *
 */

public class Student {
    /** Student's first name */
    protected String firstName;

    /** Student's last name */
    protected String lastName;

    /** Student's username */
    protected String username;

    /** Student's password */
    protected String password;

    /** Student's id */
    protected long id;
    
    /** List of courses students are taking */
    protected ArrayList<Course> courses;
    
    /** List of student questions asked */
    protected ArrayList<StudentQuestion> questions;

    /**
     * Constructor without courses parameter
     * 
     * @param firstName
     *            The first name to be given to a Student
     * @param lastName
     *            The last name to be given to a Student
     * @param username
     *            The username to be given to a Student
     * @param password
     *            The password to be given to a Student
     * @param id
     *            The password to be given to a Student
     */
    public Student(String firstName,
            String lastName, String username,
            String password, long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.id = id;
        courses = new ArrayList<Course>();
        questions = new ArrayList<StudentQuestion>();
    }
    
    
    /**
     * Default constructor
     * 
     * @param firstName
     *            The first name to be given to a Student
     * @param lastName
     *            The last name to be given to a Student
     * @param username
     *            The username to be given to a Student
     * @param password
     *            The password to be given to a Student
     * @param id
     *            The password to be given to a Student
     */
    public Student(String firstName,
            String lastName, String username,
            String password, long id, ArrayList<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.id = id;
        this.courses = courses;
        questions = new ArrayList<StudentQuestion>();
    }
    
    /**
     * Asks the professor a question
     * 
     * @return Returns the StudentQuestion asked
     */
    public StudentQuestion askQuestion(String question, Course course, Test test) {
        StudentQuestion pregunta = new StudentQuestion(question, course, test, "");
        questions.add(pregunta);
        return pregunta;
    }
    
    /**
     * Returns the student's first name
     * 
     * @return Returns the student's first name
     */
    public String getFirstName()
    {
        return firstName;
    }
    
    /**
     * Returns the student's last name
     * 
     * @return Returns the student's last name
     */
    public String getLastName()
    {
        return lastName;
    }
    
    public StringProperty firstNameProperty()
    {
        return new SimpleStringProperty(firstName);
    }
    
    public StringProperty lastNameProperty()
    {
        return new SimpleStringProperty(lastName);
    }

    /**
     * Returns the student's username
     * 
     * 
     * @return Returns the student's username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the student's id
     * 
     * @return Returns the student's id
     */
    public long getId() {
        return id;
    }


    /**
     * Returns the list of courses the student is taking
     * 
     * @return Returns the list of courses the student is taking
     */
    public ArrayList<Course> getCourses() {
        return courses;
    }

    /**
     * Returns the list of questions the student asked 
     * 
     * @return Returns the list of questions the student asked
     */
    public ArrayList<StudentQuestion> getQuestions() {
        return questions;
    }
}
