package tt.model.student;

import java.util.*;

/**
 * Model class of database of students
 * 
 * @author Dat Tran
 *
 */

public class StudentDB {
    /** Database of students */
    ArrayList<Student> students;

    /**
     * Default constructor
     */
    public StudentDB() {
        students = new ArrayList<Student>();
        students.add(new Student("Dat", "Tran",
                "dattran", "abcde12345", 12345));
        students.add(new Student("easy", 
                "access", "a", "", 0));
    }

    /**
     * Returns true if successfully authenticated and false otherwise
     * 
     <pre>
            pre:
                //
                // students, username, and password aren't null
                //
                students != null
                       &&
                username != null
                       &&
                password != null;
                
     * 
     * @param username
     *              The username entered by the user to be checked
     * @param password
     *              The password the user entered to be checked
     * @return Returns true if successfully authenticated and false otherwise
     */
    public boolean authenticate(String username,
            String password) {
        for (Student std : students)
            if (std.username.equals(username)
                    && std.password
                            .equals(password))
                return true;
        return false;
    }
    
    /**
     * Gets the database of students
     * 
     * @return Returns database of students
     */
    public ArrayList<Student> getStudents() {
        return students;
    }

    /**
     * Gets the Student with the corresponding username 
     * 
      <pre>
            pre:
                //
                // students and id aren't null
                //
                students != null
                       &&
                id != null;
                
     * 
     * 
     * @param username The username of the student to get
     * @return The professor with the passed-in id
     */
    public Student getStudent(String username) {
        for (Student stud: students) {
            if (stud.username.equals(username))
                return stud;
        }
        throw new NoSuchElementException();
    }
}
