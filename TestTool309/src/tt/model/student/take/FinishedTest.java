package tt.model.student.take;

import tt.model.professor.create.Test;
import tt.model.student.Student;

// A finished test is a test finished and ready to be graded.
public class FinishedTest {

    // A copy of the test to be graded
    private Test test;
    
    // The student who took this test.
    private Student student;

    // Constructor that accepts a test and student.
    public FinishedTest(Test test, Student student) {
        this.test = test;
        this.student = student;
    }

    // Returns the test
    public Test getTest() {
        return this.test;
    }

    // Returns the student
    public Student getStudent() {
        return this.student;
    }

}
