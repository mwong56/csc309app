package tt.model.student.take;

import java.util.ArrayList;

import tt.model.professor.create.Test;
import tt.model.student.Student;

/**
 * Singleton class that holds finished tests.
 * @author Michael
 *
 */
public class TakeTestDB {

    // List of finished tests.
    private ArrayList<FinishedTest> finishedTests;
    
    // Single instance of the class
    private static TakeTestDB instance = null;

    private TakeTestDB() {
        finishedTests = new ArrayList<FinishedTest>();
    }

    // Returns an instance of this db. 
    public static TakeTestDB getInstance() {
        if (instance == null) {
            instance = new TakeTestDB();
        }
        return instance;
    }

    // Takes in a test and a student and creates a new Finished Test and adds that to the list.
    public void addTest(Test test, Student student) {
        finishedTests.add(new FinishedTest(test,
                student));
    }

    // Returns an arraylist of Finished Tests.
    public ArrayList<FinishedTest> getTests() {
        return this.finishedTests;
    }

}
