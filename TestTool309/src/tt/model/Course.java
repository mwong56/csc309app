package tt.model;

import java.util.ArrayList;

/**
 * Class to represent a course
 * 
 * @author Dat Tran
 *
 */
public class Course {
    /**  Course abbreviation */
    protected String courseAbbr;
    /** Course number */
    protected int number;
    /** Course section */
    protected int section;
    /** Course id */
    protected int id;
    

    /**
     * Course constructor
     * 
     * @param courseAbbr
     * @param number
     * @param section
     * @param id
     */
    public Course(String courseAbbr, int number, int section, int id) {
        this.courseAbbr = courseAbbr;
        this.number = number;
        this.section = section;
        this.id = id;
    }

    /**
     * Returns the course abbreviation
     * 
     * @return Returns a String representing the course abbreviation
     */
    public String getCourseAbbr() {
        return courseAbbr;
    }

    /**
     * Returns the course abbreviation
     * 
     * @return Returns an int representing the course number
     */
    public int getNumber() {
        return number;
    }


    /**
     * Returns the course section
     * 
     * @return Returns an int reprenting the course section
     */
    public int getSection() {
        return section;
    }
    
    /**
     * Returns the course ID
     * 
     * @return Returns an int representing the course ID
     */
    public int getId() {
        return id;
    }
    
    /**
     * Returns a string representation of the course
     * 
     * @return Returns a string representation of the course
     */
    @Override
    public String toString() {
        return courseAbbr + " " + number + "-" + section + " (" + id + ")";
    }
}
