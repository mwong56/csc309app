package tt.model.professor.proctor;

import tt.model.Course;
import tt.model.professor.create.Test;

/**
 * Model class representing a student question
 * 
 * @author Dat Tran
 *
 */
public class StudentQuestion {
    /** The question asked */
    private String question;
    
    /** The course relevant to the question */
    private Course course;
    
    /** The test relevant to the question */
    private Test test;
    
    /** Professor's answer */
    private String answer;
    
    /**
     * Default constructor
     */
    public StudentQuestion(String question, Course course, 
            Test test, String answer) {
        this.question = question;
        this.course = course;
        this.test = test;
        this.answer = answer;
    }
    
    
    
    /**
     * Returns the question asked
     * 
     * @return Returns the question asked
     */
    public String getQuestion() {
        return question;
    }
    
    /**
     * Sets question
     * 
     * @param question The question to be set
     */
    public void setQuestion(String question) {
        this.question = question;
    }
    
    /**
     * Returns the course relevant to the question
     * 
     * @return Returns the course relevant to the question
     */
    public Course getCourse() {
        return course;
    }
    
    /**
     * Sets course
     * 
     * @param course The course to be set
     */
    public void setCourse(Course course) {
        this.course = course;
    }
    
    /**
     * Returns the test relevant to the question
     * 
     * @return Returns the test relevant to the question
     */
    public Test getTest() {
        return test;
    }
    
    /**
     * Sets test
     * 
     * @param test The test to be set
     */
    public void setTest(Test test) {
        this.test = test;
    }
    
    /**
     * Returns the professor's answer
     * 
     * @return Returns the professor's answer
     */
    public String getAnswer() {
        return answer;
    }
    
    /**
     * Sets answer
     * 
     * @param answer The answer to be set
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }
}