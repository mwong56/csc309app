package tt.model.professor.questions;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tt.MainApp;
import tt.model.Course;

/**
 * Model class for a QuestionBank. Creates a question bank by loading the 
 * question data from a serialized file that is unique to each professor.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class QuestionBank {
    /** The question data as an observable list of Questions */
    protected ObservableList<Question> questionData = FXCollections
            .observableArrayList();

    /** The class data as an observable list of Strings */
    protected ObservableList<String> classData = FXCollections
            .observableArrayList();
    
    /** A reference back to the main app */
    protected MainApp mainApp;
    
    /** The path to put (or load) the serialized file in */
    protected String path;

    /**
     * Constructor. Slightly deprecated because this was for a general question 
     * bank that would have been shared by all the professors.
     * 
     * @throws IOException If it can't find a file or create it
     */
    @SuppressWarnings("unchecked")
    public QuestionBank() {
        path = null;
        try
        {
            path = MainApp.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "questionBank.ser";
            File f = new File(path);
            if (f.createNewFile()) {
                questionData = FXCollections.observableArrayList();
            } else {
                
                try {
                    FileInputStream fileIn = new FileInputStream(path);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    questionData = FXCollections.observableArrayList((ArrayList<Question>) in.readObject());
                    in.close();
                    fileIn.close();
                } catch (EOFException e) {
                    questionData = FXCollections.observableArrayList();
                }
            }
        } catch(IOException i)
        {
           i.printStackTrace();
           return;
        } catch(ClassNotFoundException c)
        {
           System.out.println("Question class not found");
           c.printStackTrace();
           return;
        }
    }
    
    /**
     * Constructor. Loads the professor's unique question bank or creates one 
     * if it cannot find the file where the program is running.
     * 
     * @throws IOException If the file isn't found and cannot be created
     */
    @SuppressWarnings("unchecked")
    public QuestionBank(String username) {
        path = null;
        try
        {
            path = MainApp.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "qb_" + username + ".ser";
            File f = new File(path);
            if (f.createNewFile()) {
                questionData = FXCollections.observableArrayList();
            } else {
                
                try {
                    FileInputStream fileIn = new FileInputStream(path);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    questionData = FXCollections.observableArrayList((ArrayList<Question>) in.readObject());
                    in.close();
                    fileIn.close();
                } catch (EOFException e) {
                    questionData = FXCollections.observableArrayList();
                }
            }
        } catch(IOException i)
        {
           i.printStackTrace();
           return;
        } catch(ClassNotFoundException c)
        {
           System.out.println("Question class not found");
           c.printStackTrace();
           return;
        }
    }

    /**
     * Adds the question to the Question Bank. It adds it to the array list and 
     * then serializes the list to the file.
     * 
     * @param q The question to be added
     */
    public void addQuestion(Question q) {
        questionData.add(q);
        
        try
        {
           FileOutputStream fileOut =
           new FileOutputStream(path);
           ObjectOutputStream out = new ObjectOutputStream(fileOut);
           ArrayList<Question> qData = new ArrayList<Question>(questionData);
           out.writeObject(qData);
           out.close();
           fileOut.close();
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }
    
    /**
     * Updates the old question with the new question. The new question will 
     * replace the old question's place in the array list.
     * 
     * @param old_question The old question to be replaced
     * @param new_question The new question that will replace it
     */
    public void updateQuestion(Question old_question, Question new_question) {
        for (int i = 0; i < questionData.size(); i++) {
            if (questionData.get(i).equals(old_question)) {
                questionData.set(i, new_question);
                break;
            }
        }
        
        try
        {
           FileOutputStream fileOut =
           new FileOutputStream(path);
           ObjectOutputStream out = new ObjectOutputStream(fileOut);
           ArrayList<Question> qData = new ArrayList<Question>(questionData);
           out.writeObject(qData);
           out.close();
           fileOut.close();
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }
    
    /**
     * Sets the reference to the mainApp
     * 
     * @param app The main app to be referenced
     * */
    public void setMainApp(MainApp app) {
        mainApp = app;
        classData.clear();
        ArrayList<Course> courses = mainApp.getProfessor().getDistinctCourses();
        
        for (int i = 0; i < courses.size(); i++)
            classData.add(courses.get(i).getCourseAbbr() + courses.get(i).getNumber());

        classData.add(new String("Add a Class..."));
    }

    /**
     * Returns the data as an observable list of Questions
     * 
     * @return The list of questions contained in the question bank
     */
    public ObservableList<Question> getQuestionData() {
        return questionData;
    }

    /**
     * Returns an observable list of class names
     * 
     * @return The list of courses that a professor teaches
     */
    public ObservableList<String> getClassData() {
        return classData;
    }

    /**
     * Deletes the question given the index of it.
     * 
     * @param index The index of where the question is located
     */
    public void delete(int index) {
        questionData.remove(index);
        
        try
        {
           FileOutputStream fileOut =
           new FileOutputStream(path);
           ObjectOutputStream out = new ObjectOutputStream(fileOut);
           ArrayList<Question> qData = new ArrayList<Question>(questionData);
           out.writeObject(qData);
           out.close();
           fileOut.close();
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }
    
    /**
     * Gets the question given the index.
     * 
     * @param index The index where the question resides
     * @return The question to be retrieved
     */
    public Question get(int index) {
        return questionData.get(index);
    }
}
