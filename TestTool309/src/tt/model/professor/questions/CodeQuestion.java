package tt.model.professor.questions;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where professors write a prompt in which students must answer it 
 * by writing code.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class CodeQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /** Coding Language */
    String language;
    
    /** The Code Script */
    String script;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a Code Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt
     * @param language The coding language
     * @param script The professor's script to run
     */
    public CodeQuestion(String className, int difficulty, String time, String type, String prompt, String language, String script) {
        super(className, difficulty, time, type, prompt);
        this.language = language;
        this.script = script;
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Validates a code question type. It checks the general question parts 
     * as well as its own specific parts. It checks whether a language was 
     * chosen or not. A script is not required.
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();

        // Checking language validity
        if (language == null || language.equals(""))
            errorMessage += "No language selected!\n";
        
        return errorMessage;
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the coding language.
     * 
     * @return The coding language
     */
    public String getLanguage() {
        return language;
    }
    
    /**
     * Gets the professor's script.
     * @return The professor's coding script
     */
    public String getScript() {
        return script;
    }
}