package tt.model.professor.questions;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where students must fill in the blanks.
 * 
 * Ex: Fill in the ______! (answer: blank)
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class FillInTheBlankQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    
    /** The answers for each blank */
    final ArrayList<String> correctAnswers;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a FB Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt (with answers included inside)
     */
    public FillInTheBlankQuestion(String className, int difficulty, String time, String type, String prompt) {
        super(className, difficulty, time, type, prompt);
        correctAnswers = new ArrayList<String>();
        parsePrompt();
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Parse question prompt using regex to find 'blanks'. It will get the 
     * 'blanks' with answers inside them and store it in a list of answers. 
     * Then, this will replace the 'blanks' with underscores.
     */
    private void parsePrompt() {
        String regex = "(\\[\\[)(.*?)(\\]\\])";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(this.getPrompt());
        
        while (matcher.find()) {
            String answer = matcher.group().replaceAll(regex, "$2");
            correctAnswers.add(answer);
        }
        
        this.setPrompt(matcher.replaceAll("_________"));
    }
    
    /**
     * Validates a FB question type. It checks the general question parts 
     * as well as its own specific parts.
     * 
     * It checks if there is at least one 'blank' and whether the 'blanks' 
     * were empty (and had no correct answers in them). This also checks if 
     * there are any stray brackets (that were supposed to indicate a blank).
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();
        Pattern underlines = Pattern.compile("(_+)(\\s*)");
        Pattern startBrackets = Pattern.compile("\\[\\[");
        Pattern endBrackets = Pattern.compile("\\]\\]");
        Matcher underlineMatcher = underlines.matcher(this.getPrompt().trim());
        Matcher startBracketMatcher = startBrackets.matcher(this.getPrompt());
        Matcher endBracketMatcher = endBrackets.matcher(this.getPrompt());
        String qPrompt = underlineMatcher.replaceAll("");
        
        /** Check question prompt to make sure it isn't just all underlines */
        if (!this.getPrompt().trim().equals("") && qPrompt.equals(""))
            errorMessage += "No question prompt!\n";

        /** Check if there are any answers */
        if (correctAnswers.size() < 1)
            errorMessage += "No blanks!\n";
        
        /** Check if there is an answer inside brackets */
        for (int i = 0; i < correctAnswers.size(); i++) {
            if (correctAnswers.get(i) == null || correctAnswers.get(i).equals(""))
                errorMessage += "No answer inside brackets for #" + i + " blank!\n";
        }
        
        /** Check if there is anymore stray double brackets */
        if (startBracketMatcher.find() || endBracketMatcher.find())
            errorMessage += "Stray '[[' or ']]' found!\n";
        
        return errorMessage;
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the correct answers.
     * 
     * @return The list of correct answers
     */
    public ArrayList<String> getCorrectAnswers() {
        return correctAnswers;
    }
}