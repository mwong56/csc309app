package tt.model.professor.questions;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where students must match several questions to a pool of choices.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class MatchingQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /** List of questions */
    ArrayList<String> questions;
    
    /** List of choices */
    ArrayList<String> choices;
    
    /** The indices of the correct choices */
    ArrayList<String> correctAnswers;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a M Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt
     * @param questions The list of questions to be matched with a choice
     * @param choices The list of choices to be matched with a question
     * @param answer The list of correct answers
     */
    public MatchingQuestion(String className, int difficulty, String time, String type, String prompt, ArrayList<String> questions, ArrayList<String> choices, ArrayList<String> answer) {
        super(className, difficulty, time, type, prompt);
        this.questions = questions;
        this.choices = choices;
        correctAnswers = answer;
    }
    
    /**************************
     * HELPER FUNCTIONS
     **************************/
    
    /**
     * Validates a M question type. It checks the general question parts 
     * as well as its own specific parts.
     * 
     * This checks if a row was partially filled out, whether or not a non-empty 
     * correct answer is a valid letter and if the list of non-empty questions 
     * or choices is less than two.
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();
        
        // Check if a row has a filled question, but empty answer (and vice versa)
        if (!removeEmptyQandARows())
            errorMessage += "Missing question or correct answer in a row!\n";
        
        // Check if a non-empty correct answer is a valid letter
        if (!checkAlphaValidity())
            errorMessage += "Invalid correct answer choice!\n";
        
        if (questions.size() < 2)
            errorMessage += "Not enough questions!\n";
        
        removeEmptyChoices();
        if (choices.size() < 2)
            errorMessage += "Not enough choices!\n";
        
        return errorMessage;
    }
    
    /**
     * This is a helper function to help map the answer to the choice. If there 
     * are any correct answers that are out of range of the choice list, it 
     * will return false, indicating an error mapping the answer to a choice.
     * 
     * @return Whether or not every answer maps to a choice.
     */
    private boolean mapAnswerToChoice() {
        String answer;
        int index;

        for (int i = 0; i < correctAnswers.size(); i++) {
            answer = correctAnswers.get(i).toUpperCase();
            index = -("A".compareTo(answer));

            if (index < 0 || index >= choices.size())
                return false;
            else
                correctAnswers.set(i, choices.get(index));
        }
        
        return true;
    }
    
    /**
     * This is a helper function that checks whether the correct answers are 
     * one letter and in the alphabet. If they aren't, this function returns 
     * false to indicate an error.
     * 
     * @return Returns whether or not all correct answers are one letter long 
     * and in the alphabet.
     */
    private boolean checkAlphaValidity() {
        String regex = "[a-zA-Z]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher;
        
        for (int i = 0; i < correctAnswers.size(); i++) {
            matcher = pattern.matcher(correctAnswers.get(i));
            if (isEmptyData(correctAnswers.get(i)) || !matcher.matches())
                return false;
        }
        
        if (!mapAnswerToChoice())
            return false;
        
        return true;
    }
    
    /**
     * General helper function. Checks whether or not the string is null or 
     * empty.
     * 
     * @param str The string to be checked
     * @return Returns whether or not the string is null or empty
     */
    private boolean isEmptyData(String str) {
        return str == null || str.equals("");
    }
    
    /**
     * This is a helper function. It goes through each row, checking to see if 
     * there are any that are partially filled out. If there is, return false 
     * to indicate an error.
     * 
     * @return Whether or not any row is partially filled out
     */
    private boolean removeEmptyQandARows() {
        for (int i = 0; i < questions.size(); i++) {
            if (isEmptyData(questions.get(i))) {
                if (isEmptyData(correctAnswers.get(i))) {
                    questions.remove(i);
                    correctAnswers.remove(i);
                } else {
                    return false;
                }
            } else if (isEmptyData(correctAnswers.get(i)))
                return false;
        }
        
        return true;
    }
    
    /**
     * This is a helper function. This removes any empty choices that the 
     * user didn't fill out.
     */
    private void removeEmptyChoices() {
        for (int i = 0; i < choices.size(); i++)
            if (choices.get(i) == null || choices.get(i).equals("")) {
                choices.remove(i--);
        }
    }
    
    /**************************
     * GETTERS
     **************************/
    
    /**
     * Gets the list of questions for this matching question.
     * 
     * @return The list of questions
     */
    public ArrayList<String> getQuestions() { return questions; }
    
    /**
     * Gets the list of choices for this matching question.
     * 
     * @return The list of choices
     */
    public ArrayList<String> getChoices() { return choices; }
    
    /**
     * Gets the correct answers for this matching question.
     * 
     * @return The list of correct answers
     */
    public ArrayList<String> getCorrectAnswer() {
        return correctAnswers;
    }
}