package tt.model.professor.questions;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Model class for a Question. Each question type has various specific data, 
 * but they also have these default information as well. Every question type 
 * inherits from this class.
 * 
 * Each question type has a course associated with it, as well as a difficulty 
 * level, an estimated time for the question to be completed, a question type, 
 * and a question prompt.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /** The class name */
    String className;

    /** The question difficulty */
    int difficulty;

    /** The estimated time for the question to be completed */
    int time;
    
    /** String version of time to be validated */
    String timeString;

    /** The question type */
    String type;

    /** The question prompt */
    String prompt;
    
    /** The question prompt */
    protected String answer;

    /**
     * Old Constructor. Slightly deprecated, as it was necessary to use a 
     * String version of time. However, it is fine to use this constructor.
     * 
     * @param classString The course name the question is associated with
     * @param time The expected time it will take to finish this question
     * @param difficulty The question's difficulty
     * @param typeString The type of question in String format
     * @param prompt The question prompt
     */
    public Question(String classString,
            int difficulty, int time, String typeString, String prompt) {
        this.className = classString;
        this.type = typeString;
        this.difficulty = difficulty;
        this.time = time;
        this.prompt = prompt;
        this.answer = "";
    }
    
    /**
     * Question constructor. Creates a question with these parameters.
     * 
     * @param classString The course name the question is associated with
     * @param time The expected time it will take to finish this question
     * @param difficulty The question's difficulty
     * @param typeString The type of question in String format
     * @param prompt The question prompt
     */
    public Question(String classString,
            int difficulty, String time, String typeString, String prompt) {
        this.className = classString;
        this.type = typeString;
        this.difficulty = difficulty;
        this.time = -1;
        this.timeString = time;
        this.prompt = prompt;
        this.answer = "";
    }
    
    /**
     * Validates the general question inputs. Returns an error message if there 
     * is any part of the question that didn't meet requirements.
     * 
     * @return An error message if it didn't meet requirements.
     */
    public String validate() {
        return isGeneralInputValid();
    }
    
    /**
     * Helper function for validation. Checks whether or not the time string 
     * can be parsed into an integer. Basically, it checks if the user's time 
     * input is numeric. Otherwise, return false to indicate an error.
     * 
      <pre>
            pre:
                //
                // str isn't null
                //
      str != null
      
     * @return Whether or not the time input is numeric
     */
    protected static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        }
        catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    
    /**
     * Checks if the general question inputs are valid.
     * 
     * This checks if there is a course selected, if the time is numeric and 
     * filled in, if there was a difficulty selected, and if the user selected 
     * a question type to create. It checks if the question prompt also is 
     * filled in.
     * 
     * If they aren't valid, an error message is returned.
     * 
     * @return An error message indicating that the question didn't meet 
     * requirements
     */
    public String isGeneralInputValid() {
        String errorMessage = "";

        // Checking Class validity
        if (className == null) {
            errorMessage += "No class selected!\n";
        }

        // Checking Time validity
        if (timeString == null || !isNumeric(timeString)) {
            errorMessage += "No valid estimated time!\n";
        } else {
            time = Integer.parseInt(timeString);
        }

        if (difficulty < 0 || difficulty > 5)
            errorMessage += "No difficulty selected!\n";

        // Checking Question Type validity
        if (type == null) {
            errorMessage += "No question type selected!\n";
        }
        
        if (prompt == null || prompt.trim().equals(""))
            errorMessage += "No question prompt!\n";
        
        return errorMessage;
    }
    
    /**
     * Gets the class name.
     * 
      <pre>
            pre:
                //
                // className isn't null
                //
                className != null;
     * 
     * @return String format of class name
     */
    public String getClassName() {
        return className;
    }

    /**
     * Sets the class name.
     * 
      <pre>
            pre:
                //
                // className isn't null
                //
                className != null;
                
            post:
                //
                // this.className isn't null and equals className
                //
                this.className != null
                
                    &&
               
                this.className.equals(className);
     *      
     * @param className The class name to be set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * Get class name in StringProperty format.
     * 
      <pre>
            pre:
                //
                // className isn't null
                //
                className != null;
      
     * @return StringProperty format of class name
     */
    public StringProperty classNameProperty() {
        return new SimpleStringProperty(className);
    }

    /**
     * Set the question difficulty.
     * 
      <pre>
            pre:
                //
                // diff isn't null
                //
                diff != null;
                
            post:
                //
                // difficulty' isn't null and equals diff
                //
                difficulty' != null
                
                    &&
               
                difficulty'.equals(diff);
     * 
     * @param diff
     */
    public void setDifficulty(int diff) {
        difficulty = diff;
    }
    
    /**
     * Gets the difficulty of the question in IntegerProperty format
     * 
     * @return The difficulty of the question in IntegerProperty format
     */
    public IntegerProperty difficultyProperty() {
        return new SimpleIntegerProperty(difficulty);
    }
    
    /**
     * Gets the difficulty of the question.
     * 
     * @return The difficulty of the question in String format
     */
    public String getDifficulty() {
        return String.valueOf(difficulty);
    }

    /**
     * Sets the question's estimated time
     * 
      <pre>
            pre:
                //
                // t isn't null
                //
                t != null;
                
            post:
                //
                // time' isn't null and equals t
                //
                time' != null
                
                    &&
               
                time'.equals(t);
     * 
     * @param t The time to be set
     */
    public void setTime(String time) {
        timeString = time;
    }
    
    /**
     * Gets the estimated time
     * 
     * @return The estimated time in String format
     */
    public String getEstTime() {
        return String.valueOf(time);
    }

    /**
     * Get question type.
     * 
      <pre>
            pre:
                //
                // Question type isn't null
                //
                type != null;
     * 
     * @return String format of question type
     */
    public String getType() {
        return type;
    }

    /**
     * Set question type.
     * 
      <pre>
            pre:
                //
                // type isn't null
                //
                type != null;
                
            post:
                //
                // this.type' isn't null and equals type
                //
                this.type' != null
                
                    &&
               
                this.type'.equals(type);
           
     * @param type
     * String question type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get question type in the form of StringProperty.
     * 
      <pre>
            pre:
                //
                // Question type isn't null
                //
                type != null;
     * 
     * @return StringProperty format for question type
     */
    public StringProperty typeProperty() {
        return new SimpleStringProperty(type);
    }

    /**
     * Get question prompt.
     * 
      <pre>
            pre:
                //
                // Question prompt isn't null
                //
                prompt != null;
      
     * @return String format of question prompt
     */
    public String getPrompt() {
        return prompt;
    }

    /**
     * Set question prompt.
     * 
      <pre>
            pre:
                //
                // question isn't null
                //
                question != null;
                
            post:
                //
                // prompt' isn't null and equals question
                //
                prompt' != null
                
                    &&
               
                prompt'.equals(question);
     *      
     * @param question
     * String question prompt
     */
    public void setPrompt(String question) {
        prompt = question;
    }

    /**
     * Get question prompt in StringProperty format.
     * 
      <pre>
            pre:
                //
                // Question prompt isn't null
                //
                prompt != null;
     * 
     * @return StringProperty format of question prompt
     */
    public StringProperty promptProperty() {
        return new SimpleStringProperty(prompt);
    }
    
    /**
     * Get question answer.
     * 
      <pre>
            pre:
                //
                // question isn't null
                //
                question != null;
                
            post:
                //
                // answer' isn't null and equals question
                //
                answer' != null
                
                    &&
               
                answer'.equals(answer);
     *      
     * @param question
     * String question answer
     */
    public String getAnswer() {
        return answer;
    }
    
    /**
     * Set question answer.
     * 
      <pre>
            pre:
                //
                // question isn't null
                //
                question != null;
                
            post:
                //
                // answer' isn't null and equals question
                //
                answer' != null
                
                    &&
               
                answer'.equals(answer);
     *      
     * @param question
     * String question answer
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    /**
     * Get question answer in StringProperty format.
     * 
      <pre>
            pre:
                //
                // Question answer isn't null
                //
                answer != null;
     * 
     * @return StringProperty format of question answer
     */
    public StringProperty answerProperty() {
        return new SimpleStringProperty(answer);
    }
}