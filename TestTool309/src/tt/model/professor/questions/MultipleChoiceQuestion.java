package tt.model.professor.questions;

import java.util.ArrayList;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where students must select the correct choice out of a list of 
 * choices.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class MultipleChoiceQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /** List of choices */
    final ArrayList<String> choices;
    
    /** The index of the correct choice */
    final int correctAnswerIndex;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a MC Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt
     * @param choices The list of choices
     * @param answer The correct answer index
     */
    public MultipleChoiceQuestion(String className, int difficulty, String time, String type, String prompt, ArrayList<String> choices, int answer) {
        super(className, difficulty, time, type, prompt);
        this.choices = choices;
        correctAnswerIndex = answer;
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Validates a MC question type. It checks the general question parts 
     * as well as its own specific parts.
     * 
     * This checks if there was a correct choice selected and if the it has 
     * some sort of text inside it. It also checks if there are enough filled-
     * in choices.
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();
        String correctChoice;
        
        if (correctAnswerIndex < 0 || correctAnswerIndex > choices.size())
            errorMessage += "No correct answer selected!\n";
        else {
            correctChoice = choices.get(correctAnswerIndex);
            if (correctChoice == null || correctChoice.equals(""))
                errorMessage += "No choice text!\n";
        }
        
        removeEmptyChoices();
        if (choices.size() < 2)
            errorMessage += "Not enough choices!\n";
        
        return errorMessage;
    }
    
    private void removeEmptyChoices() {
        for (int i = 0; i < choices.size(); i++)
            if ((choices.get(i) == null || choices.get(i).equals("")) && i != correctAnswerIndex) {
                choices.remove(i--);
        }
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the list of choices for this multiple choice question.
     * 
     * @return The list of choices
     */
    public ArrayList<String> getChoices() { return choices; }
    
    /**
     * Gets the correct answer index.
     * 
     * @return The correct answer index
     */
    public int getCorrectAnswerIndex() { return correctAnswerIndex; }
    
    /**
     * Gets the correct answer text
     * 
     * @return The correct answer text
     */
    public String getCorrectAnswer() { return choices.get(correctAnswerIndex); }
    
    
}