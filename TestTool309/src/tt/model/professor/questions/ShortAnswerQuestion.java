package tt.model.professor.questions;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where students must write a short response to the question prompt.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class ShortAnswerQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    
    /** Answer notes */
    final String answerNotes;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a LA Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt
     * @param notes The answer notes
     */
    public ShortAnswerQuestion(String className, int difficulty, String time, String type, String prompt, String notes) {
        super(className, difficulty, time, type, prompt);
        answerNotes = (notes == null ? "" : notes);
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Validates a code question type. It checks the general question parts 
     * as well as its own specific parts. The answer notes are not required.
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();
        
        return errorMessage;
    }

    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the answer notes.
     * 
     * @return The professor's answer notes
     */
    public String getAnswerNotes() { return answerNotes; }
}