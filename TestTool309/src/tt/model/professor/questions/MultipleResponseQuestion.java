package tt.model.professor.questions;

import java.util.ArrayList;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where students must select at least one choice for the question. 
 * However, there could be many valid choices they must select to be fully 
 * correct.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class MultipleResponseQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /** List of choices */
    final ArrayList<String> choices;
    
    /** The indices of the correct choices */
    final ArrayList<Integer> correctAnswerIndices;
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a MR Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt
     * @param choices The list of choices
     * @param answer The correct answer indices
     */
    public MultipleResponseQuestion(String className, int difficulty, String time, String type, String prompt, ArrayList<String> choices, ArrayList<Integer> answer) {
        super(className, difficulty, time, type, prompt);
        this.choices = choices;
        correctAnswerIndices = answer;
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Validates a MR question type. It checks the general question parts 
     * as well as its own specific parts.
     * 
     * This checks if there is a correct answer, if there is an invalid answer 
     * index, and if there is no text in the correct choices.
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();
        String correctChoice;
        
        if (correctAnswerIndices.size() < 1) {
            errorMessage += "No correct answer selected!\n";
        }
        
        for (int i = 0; i < correctAnswerIndices.size(); i++) {
            int index = correctAnswerIndices.get(i);
            if (index < 0 || index > choices.size()) {
                errorMessage += "Invalid answer index: " + index + "\n";
            } else {
                correctChoice = choices.get(index);
                if (correctChoice == null || correctChoice.equals(""))
                    errorMessage += "No choice text for index " + index + "!\n";
            }
        }
        
        removeEmptyChoices();
        if (choices.size() < 2)
            errorMessage += "Not enough choices!\n";
        
        return errorMessage;
    }
    
    /**
     * This is a helper function. This removes empty choices from the list of 
     * available choices, unless it is selected as a correct answer. If it 
     * is a correct choice with no choice text, the error will be found in the 
     * validation method.
     */
    private void removeEmptyChoices() {
        for (int i = 0; i < choices.size(); i++)
            if (choices.get(i) == null || (choices.get(i).equals("") && !correctAnswerIndices.contains(i))) {
                choices.remove(i--);
        }
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the list of choices for this multiple choice question.
     * 
     * @return The list of choices
     */
    public ArrayList<String> getChoices() { return choices; }
    
    /**
     * Gets the list of correct answer indices.
     * 
     * @return The list of correct answer indices
     */
    public ArrayList<Integer> getCorrectAnswerIndices() { return correctAnswerIndices; }
}