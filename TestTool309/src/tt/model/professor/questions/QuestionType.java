package tt.model.professor.questions;

/**
 * The QuestionType enum helps identify which question type the user has
 * selected.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public enum QuestionType implements java.io.Serializable {
    TF("TF"), MC("MC"), MR("MR"), M("M"), FB("FB"), SA(
            "SA"), LA("LA"), C("C");

    private final String name;

    /**
     * QuestionType constructor. Constructs the question type and puts the 
     * String version of it together with it.
     * 
     * @param s The String version of the enumerator
     */
    private QuestionType(String s) {
        name = s;
    }

    /**
     * Overrides toString() to return abbreviated question type
     */
    @Override
    public String toString() {
        return name;
    }
}
