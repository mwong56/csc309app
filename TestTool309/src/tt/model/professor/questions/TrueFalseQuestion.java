package tt.model.professor.questions;

/**
 * This class inherits Question and is a question type. This type of 
 * question is where students must answer whether the question prompt is true 
 * or false.
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 *
 */
public class TrueFalseQuestion extends Question implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    
    /** The String version of the correct answer */
    String strAnswer;
    
    /** The boolean version of the correct answer */
    boolean correctAnswer;

    public static final String TRUE = "True";
    public static final String FALSE = "False";
    
    /**************************
     * CONSTRUCTORS
     **************************/
    
    /**
     * The constructor for a TF Question
     * 
     * @param className What course this question assigned to
     * @param difficulty The difficulty of the question
     * @param time The expected time it will take for a student to finish this question
     * @param type The type of question
     * @param prompt The question prompt
     * @param answer The String version of the correct answer
     */
    public TrueFalseQuestion(String className, int difficulty, String time, String type, String prompt, String answer) {
        super(className, difficulty, 0, type, prompt);
        this.setTime(time);
        strAnswer = answer;
    }
    
    /***************************
     * HELPER FUNCTIONS
     ***************************/
    
    /**
     * Validates a TF question type. It checks the general question parts 
     * as well as its own specific parts.
     * 
     * This checks if there was a correct answer selected.
     * 
     * This returns an error message to the controller to show if there 
     * is any errors.
     */
    public String validate() {
        String errorMessage = this.isGeneralInputValid();
        
        if (!strToBool())
            errorMessage += "No correct answer selected!\n";
        
        return errorMessage;
    }
    
    /**
     * This is a helper function. It changes the String version of the 
     * correct answer to the boolean version. So "True" becomes the boolean 
     * equivalent of true and the same for false.
     * 
     * @return Returns whether or not there was a correct answer selected
     */
    protected boolean strToBool() {
        correctAnswer = false;
        
        if (strAnswer == null)
            return false;
        else if (strAnswer.equals("True"))
            correctAnswer = true;
        
        return true;
    }
    
    /***************************
     * GETTERS
     ***************************/
    
    /**
     * Gets the correct answer.
     * 
     * @return The correct answer in the form of a boolean
     */
    public boolean getCorrectAnswer() { return correctAnswer; }
}
