package tt.model.professor;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import tt.model.Course;

/**
 * Model class of database of professors
 * 
 * @author Dat Tran
 *
 */

public class ProfessorDB {
    /** Database of professors */
    protected ArrayList<Professor> professors;

    /**
     * Default constructor
     */
    public ProfessorDB() {
        professors = new ArrayList<Professor>();
        ArrayList<Course> fisher = new ArrayList<Course>();
        fisher.add(new Course("CPE", 308, 1, 69));
        fisher.add(new Course("CPE", 308, 2, 70));
        fisher.add(new Course("CPE", 309, 1, 71));
        
        ArrayList<Course> mammen = new ArrayList<Course>();
        mammen.add(new Course("CPE", 102, 1, 50));
        mammen.add(new Course("CPE", 102, 2, 51));
        mammen.add(new Course("CPE", 357, 1, 75));
        
        professors.add(new Professor("Gene",
                "Fisher", "gfisher",
                "team1isthebest", 1, fisher));
        professors.add(new Professor("Kurt",
                "Mammen", "kmammen",
                "ILOVEBEINGANASSHOLE", 2, mammen));
        professors.add(new Professor("easy", 
                "access", "a", "", 3, fisher));
    }

    /**
     * Returns true if successfully authenticated and false otherwise
     * 
      <pre>
            pre:
                //
                // professors, username, and password aren't null
                //
                professors != null
                       &&
                username != null
                       &&
                password != null;
     *           
     * 
     * 
     * @param username
     *            The username entered by the user to be checked
     * @param password
     *            The password the user entered to be checked
     * @return Returns true if successfully authenticated and false otherwise
     */
    public boolean authenticate(String username,
            String password) {
        for (Professor prof : professors)
            if (prof.username.equals(username)
                    && prof.password
                            .equals(password))
                return true;
        return false;
    }
    
    /**
     * Gets the Professor with the corresponding username 
     * 
       <pre>
            pre:
                //
                // professors and id aren't null
                //
                professors != null
                       &&
                id != null;
     *           
     * 
     * 
     * @param id The id of the professor to get
     * @return The professor with the passed-in id
     */
    public Professor getProfessor(String username){
        for (Professor prof: professors) {
            if (prof.username.equals(username))
                return prof;
        }
        throw new NoSuchElementException();
    }
}
