package tt.model.professor.grading;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import tt.MainApp;
import tt.model.professor.Professor;
import tt.model.professor.create.Test;
import tt.model.professor.questions.Question;
import tt.model.student.Student;
import tt.model.student.take.FinishedTest;
import tt.model.student.take.TakeTestDB;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class gradetest implements Serializable{
    
    ObservableList<FinishedTest> myTest = FXCollections
            .observableArrayList();
    ObservableList<Student> sList = FXCollections.observableArrayList();
    private TakeTestDB mytestdb;
    public static FinishedTest taken = null;
    
    public gradetest()
    {
        mytestdb = TakeTestDB.getInstance();
        ArrayList<FinishedTest> arr = mytestdb.getTests();
        for(int i = 0; i < arr.size(); i++)
        {
            myTest.add(arr.get(i));
        }
        sList.add(new Student("Ka", "Tong", "katong", "ichael21215", 12345));
        sList.add(new Student("Terrence", "Li", "terrenceLi", "ichael21215", 12345));
        sList.add(new Student("Dat", "Tran", "dtran", "ichael21215", 12345));
        sList.add(new Student("Ryan", "Li", "ryanLi", "ichael21215", 12345));
        sList.add(new Student("Michael", "Wong", "Michaelwong", "ichael21215", 12345));
        sList.add(new Student("Mandy", "Chan", "mandychan", "ichael21215", 12345));
        String path = null;
    }
    
    public ObservableList<FinishedTest> getTest()
    {
        return myTest;
    }
    public ObservableList<Student> getStudent()
    {
        return sList;
    }

   


}
