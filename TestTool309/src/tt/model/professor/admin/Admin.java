package tt.model.professor.admin;

//import javafx.beans.property.IntegerProperty;
//import javafx.beans.property.ObjectProperty;
//import javafx.beans.property.SimpleIntegerProperty;
//import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/****
 *
 * Class Admin provides methods to handle actions that deal with tests, such as
 * saving the default release test settings, and getting the test type.
 *
 *
 * @author Ryan Lee (rlee50@calpoly.edu)
 *
 */

public class Admin {
    private final StringProperty testType;

    /**
     * 
     * Admin() initializes the Admin object.
     *
       pre: testType == NULL;
     
       post: testType == NULL;
     *
     */
    public Admin() {
        this.testType = new SimpleStringProperty();
    }

    /**
     * 
     * Admin(String) initializes the Admin object and sets the test type to the
     * input string.
     *
       pre: type.length() > 0;
     
       post: testType.get() == type;
     *
     */
    public Admin(String type) {
        this.testType = new SimpleStringProperty(
                type);
    }

    /**
     * 
     * setType(String) sets the test type to the input string.
     *
       pre: type.length() > 0;
     
       post: testType.get() == type;
     *
     */
    public void setType(String type) {
        testType.set(type);
    }

    /**
     * 
     * getType() returns the test type in the form of a string.
     *
       pre: testType != NULL;
     
       post: return value == testType.get();
     *
     */
    public String getType() {
        return testType.get();
    }

    /**
     *
     * typeClicked() checks for the testType and returns a number corresponding
     * to the test.
     *
       pre: testType != NULL;
     
       post: if(testType.get() == "In Class") return value == 0; else
       if(testType.get() == "Practice") return value == 1; else return value ==
       2;
     *
     */
    public int typeClicked() {
        int typeNum = -1;
        if (testType.get() == "In Class") {
            typeNum = 0;
        }
        else if (testType.get() == "Practice") {
            typeNum = 1;
        }
        else if(testType.get() == "Take Home") {
            typeNum = 2;
        }

        return typeNum;
    }

    /**
     * 
     * saveSettings() sets the current settings to be the default release test
     * settings.
     *
       pre: closedBook == true || openBook == true;
      
       post: defaultSettings == currentSettings;
     *
     */
    public void saveSettings() {
//        System.out.println("Settings saved");
    }
}