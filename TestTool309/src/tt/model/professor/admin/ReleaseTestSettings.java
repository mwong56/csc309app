package tt.model.professor.admin;

import tt.controller.professor.create.TestDBController;

public class ReleaseTestSettings {
    private String type;
    private boolean closedBook;
    private int numTries;
    private int timeLimit;
    
    private static ReleaseTestSettings rts = null;
    
    public ReleaseTestSettings() {
        this("", true, 0, 0);
    }
    
    public ReleaseTestSettings(String type, boolean closedBook, int numTries, int timeLimit) {
        this.type = type;
        this.closedBook = closedBook;
        this.numTries = numTries;
        this.timeLimit = timeLimit;
    }
    
    public static ReleaseTestSettings getRTS() {
        if(rts == null)
            rts = new ReleaseTestSettings();
        return rts;
    }
    
    public static void setRTS(ReleaseTestSettings settings) {
        rts = settings;
    }
    
    public String getType() {
        return type;
    }
    
    public boolean getClosed() {
        return closedBook;
    }
    
    public int getNumTries() {
        return numTries;
    }
    
    public int getTimeLimit() {
        return timeLimit;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public void setClosed(boolean closedBook) {
        this.closedBook = closedBook;
    }
    
    public void setNumTries(int numTries) {
        this.numTries = numTries;
    }
    
    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }
}
