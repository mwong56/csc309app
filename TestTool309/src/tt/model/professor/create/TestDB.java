package tt.model.professor.create;

import tt.model.professor.create.Test;
import java.util.ArrayList;

/**
 * 
 * @author Ryan Lee
 *
 */

public class TestDB {
    ArrayList<Test> testDB;
    
    public TestDB() {
        testDB = new ArrayList<Test>();
    }
    
    public TestDB(ArrayList<Test> testDB) {
        this.testDB = testDB;
    }
    
    public void addTest(Test test) {
        testDB.add(test);
    }
    
    public void removeTest(Test test) {
        if(testDB.contains(test))
            testDB.remove(test);
    }
    
    public ArrayList<Test> getTestDB() {
        return testDB;
    }
    
    public void setTestDB(ArrayList<Test> testDB) {
        this.testDB.clear();
        this.testDB.addAll(testDB);
    }
    
    public void clearTestDB() {
        testDB.clear();
    }
    
    public int getSize() {
        return testDB.size();
    }
}
