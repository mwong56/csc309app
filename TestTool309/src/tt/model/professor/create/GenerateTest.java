package tt.model.professor.create;

import tt.model.professor.questions.Question;
import tt.model.professor.questions.QuestionBank;
import java.util.ArrayList;

/**
 * 
 * @author Terrence Li
 *
 */
public class GenerateTest {
    TestSettings ts;
    Test t;
    int totQ;
    String courseName;
    String testName;
    QuestionBank qb = new QuestionBank();
    ArrayList<Question> questions = new ArrayList<Question>();
    
    public GenerateTest() {
    }
    
    public GenerateTest(TestSettings ts, int totalQuestion, String courseName, String testName, QuestionBank qb) {
        this.ts = ts;
        this.totQ = totalQuestion;
        this.courseName = courseName;
        this.testName = testName;
        this.qb = qb;
    }
    
    public Test generateTest() {
        t = new Test(courseName, testName);
        questions.addAll(qb.getQuestionData());
        int ndx = 0;
        int selected = 0;
        
        for(int i = 0; i < ts.getNumMC()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "MC") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumTF()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "TF") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumSA()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "FB") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumCoding()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "C") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumLA()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "LA") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumMR()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "M") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumM()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "MR") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        ndx = 0;
        for(int i = 0; i < ts.getNumFB()/100.0 * totQ; i++) {
            while(ndx < questions.size() && selected == 0) {
                if(questions.get(ndx).getType() == "SA") {
                    t.addQuestion(questions.get(ndx));
                    selected = 1;
                }
                ndx++;
            }
            selected = 0;
        }
        
        return t;
    }
}
