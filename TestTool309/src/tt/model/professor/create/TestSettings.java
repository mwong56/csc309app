package tt.model.professor.create;

/**
 * 
 * @author Terrence Li
 *
 */
public class TestSettings {
    private Integer avgDiff;
    private Integer diffRange;
    private Double numMC;
    private Double numTF;
    private Double numSA;
    private Double numCoding;
    private Double numLA;
    private Double numMR;
    private Double numM;
    private Double numFB;
    
    public TestSettings() {
        
    }
    
    
    public TestSettings(Integer avgDiff, Integer diffRange, Double numMC, 
            Double numTF, Double numSA, Double numCoding, Double numLA,
            Double numMR, Double numM, Double numFB) {
        this.avgDiff = avgDiff;
        this.diffRange = diffRange;
        this.numMC = numMC;
        this.numTF = numTF;
        this.numSA = numSA;
        this.numCoding = numCoding;
        this.numLA = numLA;
        this.numMR = numMR;
        this.numM = numM;
        this.numFB = numFB;
    }
    
    /**
     * get average difficulty
     * @return avgDiff
     */
    public Integer getAvgDiff() {
        return avgDiff;
    }
    
    /**
     * set average difficulty
     * @param avgDiff
     */
    public void setAvgDiff(Integer avgDiff) {
        this.avgDiff = avgDiff;
    }
    
    /**
     * get difficulty range
     * @return diffRange
     */
    public Integer getDiffRange() {
        return diffRange;
    }
    
    /**
     * set difficulty range
     * @param diffRange
     */
    public void setDiffRange(Integer diffRange) {
        this.diffRange = diffRange;
    }
    
    /**
     * get number of M/C
     * @return numMC
     */
    public Double getNumMC() {
        return numMC;
    }
    
    /**
     * set number of M/C
     * @param numMC
     */
    public void setNumMC(Double numMC) {
        this.numMC = numMC;
    }
    
    /**
     * get number of T/F
     * @return numTF
     */
    public Double getNumTF() {
        return numTF;
    }
    
    /**
     * set number of T/F
     * @param numTF
     */
    public void setNumTF(Double numTF) {
        this.numTF = numTF;
    }
    
    /**
     * get the number of fill in
     * @return numSA
     */
    public Double getNumSA() {
        return numSA;
    }
    
    /**
     * set the number of fill in
     * @param numSA
     */
    public void setNumSA(Double numSA) {
        this.numSA = numSA;
    }
    
    /**
     * get the number of numCoding questions
     * @return numCoding
     */
    public Double getNumCoding() {
        return numCoding;
    }
    
    /**
     * set the number of numCoding questions
     * @param numCoding
     */
    public void setNumCoding(Double numCoding) {
        this.numCoding = numCoding;
    }
    
    public Double getNumLA() {
        return numLA;
    }
    
    public void setNumLA(Double numLA) {
        this.numLA = numLA;
    }
    
    public Double getNumMR() {
        return numMR;
    }
    
    public void setNumMR(Double numMR) {
        this.numMR = numMR;
    }
    
    public Double getNumM() {
        return numM;
    }
    
    public void setNumM(Double numM) {
        this.numM = numM;
    }
    
    public Double getNumFB() {
        return numFB;
    }
    
    public void setNumFB(Double numFB) {
        this.numFB = numFB;
    }
}
