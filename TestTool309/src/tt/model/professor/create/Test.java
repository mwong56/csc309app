package tt.model.professor.create;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tt.model.professor.Professor;
import tt.model.professor.questions.Question;
import tt.model.professor.admin.ReleaseTestSettings;

/**
 * 
 * @author Terrence Li, Michael Wong
 *
 */
public class Test implements Serializable{
    private final IntegerProperty questionNumber;
    private final ObjectProperty<LocalDate> dateCreated;

    private ObservableList<Question> questionList = FXCollections
            .observableArrayList();

    // MICHAEL'S STUFF
    // private final Collection<Question> questions; /* All the questions in the
    // test */
    private final StringProperty limit; /*
                                         * How much time is given/left in the
                                         * test
                                         */
    private final StringProperty close; /*
                                         * Determines when the test stops being
                                         * open
                                         */

    private final StringProperty title; /* The name of the test */
    private final StringProperty course; /* The class that this test is for */
    private final StringProperty professorString; /*
                                             * The professor who administered
                                             * the test
                                             */
    private final StringProperty type; /* The type of test */
    private Professor professor;
    
    private ReleaseTestSettings rts = new ReleaseTestSettings();
    private boolean set = false;

    /**
     * Default constructor.
     */
    public Test() {
        this(null, null);
    }

    /**
     * Constructor with some initial data.
     * 
     * @param testName
     */
    public Test(String className, String testName) {
        this.course = new SimpleStringProperty(
                className);
        this.title = new SimpleStringProperty(
                testName);

        this.questionNumber = new SimpleIntegerProperty();
        this.dateCreated = new SimpleObjectProperty<LocalDate>(
                LocalDate.now());

        this.limit = new SimpleStringProperty("");
        this.close = new SimpleStringProperty("");
        this.professorString = new SimpleStringProperty(
                "");
        this.type = new SimpleStringProperty("");
    }

    // MICHAEL'S STUFF
    public Test(Collection<Question> questions,
            String limit, String close,
            String title, String course,
            Professor professor, String type) {
        super();

        this.questionList.addAll(questions);
        this.limit = new SimpleStringProperty(
                limit);
        this.close = new SimpleStringProperty(
                close);
        this.title = new SimpleStringProperty(
                title);
        this.course = new SimpleStringProperty(
                course);
        this.professor = professor;
        this.professorString = new SimpleStringProperty(professor.getFirstName() + " " + professor.getLastName());
        this.type = new SimpleStringProperty(type);
        this.questionNumber = new SimpleIntegerProperty();
        this.dateCreated = new SimpleObjectProperty<LocalDate>(LocalDate.now());
    }

    public int getQuestionNumber() {
        return questionNumber.get();
    }

    public void setQuestionNumber(
            int questionNumber) {
        this.questionNumber.set(questionNumber);
    }

    public IntegerProperty questionNumberProperty() {
        return questionNumber;
    }

    public LocalDate getDateCreated() {
        return dateCreated.get();
    }

    public void setDateCreated(
            LocalDate dateCreated) {
        this.dateCreated.set(dateCreated);
    }

    public ObjectProperty<LocalDate> dateCreatedProperty() {
        return dateCreated;
    }

    public void addQuestion(Question question) {
        questionList.add(question);
    }

    public ObservableList<Question> getQuestionList() {
        return questionList;
    }

    // change to Question class later
    public Question questionProperty(int index) {
        return questionList.get(index);
    }

    // MICHAEL'S STUFF
    public StringProperty getLimit() {
        return limit;
    }

    public StringProperty getClose() {
        return close;
    }

    public StringProperty getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title.setValue(title);
    }

    public StringProperty getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course.setValue(course);
    }

    public StringProperty getProfessor() {
        return professorString;
    }

    public StringProperty getType() {
        return type;
    }

    public void setReleaseType(String type) {
        rts.setType(type);
    }
    
    public void setReleaseClosed(boolean closedBook) {
        rts.setClosed(closedBook);
    }
    
    public void setReleaseTries(int numTries) {
        rts.setNumTries(numTries);
    }
    
    public void setReleaseTime(int timeLimit) {
        rts.setTimeLimit(timeLimit);
    }
    
    public void setSetSettings(boolean set) {
        this.set = set;
    }
    
    public boolean getSet() {
        return set;
    }
    
    public void setRTS(ReleaseTestSettings rts) {
        this.rts = rts;
    }
}
