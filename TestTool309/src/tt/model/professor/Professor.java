package tt.model.professor;

import java.util.ArrayList;

import tt.model.Course;
import tt.model.professor.proctor.StudentQuestion;

/**
 * Professor model class representing a professor user
 * 
 * @author Dat Tran
 *
 */

public class Professor {
    /** Professor's first name */
    protected String firstName;

    /** Professor's last name */
    protected String lastName;

    /** Professor's username */
    protected String username;

    /** Professor's password */
    protected String password;

    /** Professor's id */
    protected long id;

    /** List of courses the professor is teaching */
    protected ArrayList<Course> courses;
        
    /** List of questions asked by students */
    public ArrayList<StudentQuestion> questions;  
    
    /**
     * Default constructor
     * 
     * @param firstName
     *            The first name to be given to a Professor
     * @param lastName
     *            The last name to be given to a Professor
     * @param username
     *            The username to be given to a Professor
     * @param password
     *            The password to be given to a Professor
     * @param id
     *            The id to be given to a Professor
     */
    public Professor(String firstName,
            String lastName, String username,
            String password, long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.id = id;
        courses = new ArrayList<Course>();
        questions = new ArrayList<StudentQuestion>();
    }
    
    /**
     * Constructor with list of courses parameter
     * 
     * @param firstName
     *              The first name to be given to a Professor
     * @param lastName
     *              The last name to be given to a Professor
     * @param username
     *              The username to be given to a Professor
     * @param password
     *              The password to be given to a Professor
     * @param courses 
     *              The list of courses to be given to a Professor
     */
    public Professor(String firstName,
            String lastName, String username,
            String password, long id, ArrayList<Course> newCourses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.id = id;
        courses = newCourses;
        questions = new ArrayList<StudentQuestion>();
    }
    
    /**
     * Returns the professor's first name
     * 
     * @return Returns the professor's first name
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * Returns the professor's last name
     * 
     * @return Returns the professor's last name
     */
    public String getLastName() {
        return lastName;
    }
    
    /**
     * Returns the professor's username
     * 
     * @return Returns the professor's username
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * Returns the professor's id
     * 
     * @return Returns the professor's id
     */
    public long getId() {
        return id;
    }
    
    /**
     * Return the courses the professor is teaching with sections
     * 
     * @return Returns the course the professor is teaching
     */
    public ArrayList<Course> getCourses() {
        return courses;
    }
    
    /**
     * Return the courses the professor is teaching without sections
     * 
     * @return Returns the course the professor is teaching
     */
    public ArrayList<Course> getDistinctCourses() {
        ArrayList<Course> temp = new ArrayList<Course>();
        ArrayList<Course> distinctCourses = new ArrayList<Course>();
        if (!courses.isEmpty())
            temp.add(courses.get(0));
        
        for (int i = 0; i < courses.size(); i++) {
            boolean distinctCourse = true;
            Course courseToCheck = courses.get(i);
            for (int j = 0; j < distinctCourses.size(); j++) {
                Course tempCourse = distinctCourses.get(j);
                String courseToCheckName = courseToCheck.getCourseAbbr() + courseToCheck.getNumber();
                String otherCourse = tempCourse.getCourseAbbr() + tempCourse.getNumber();
                if (courseToCheckName.equals(otherCourse))
                    distinctCourse = false;
            }
            
            if (distinctCourse)
                distinctCourses.add(courseToCheck);
        }
        
        return distinctCourses;
    }
    
    /**
     * Returns all the course abbreviations for courses the professor is teaching
     * 
     * @return Returns all the course abbreviations for courses the professor is teaching
     */
    public ArrayList<String> getCourseAbbrs() {
        ArrayList<String> ret = new ArrayList<String>();
        if (courses != null) {
            for (Course course : courses) {
                String temp = course.getCourseAbbr();
                if (!ret.contains(temp))
                    ret.add(temp);
            }
        }
        return ret;
    }
    
    /**
     * Returns all the course numbers for courses the professor is teaching
     * 
     * @return Returns all the course numbers for courses the professor is teaching
     */
    public ArrayList<Integer> getCourseNums() {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        for (Course course : courses) {
            Integer temp = course.getNumber();
            if (!ret.contains(temp))
                ret.add(temp);
        }
        return ret;
    }
    
    /**
     * Returns all the course numbers for courses the professor is teaching
     * 
     * @param course The course to get sections for
     * @return Returns all the course numbers for courses the professor is teaching
     */
    public ArrayList<Integer> getSections(Course course) {
        ArrayList<Integer> ret = new ArrayList<Integer>();
        for (Course c : courses) {
            if (course.getCourseAbbr().equals(c.getCourseAbbr()) &&
                course.getNumber() == c.getNumber()) {
                Integer temp = c.getSection();
                if (!ret.contains(temp))
                    ret.add(temp);
            }
        }
        return ret;
    }
    
    /**
     * Returns all the student questions 
     * 
     * @return Returns list of student questions
     */
    public ArrayList<StudentQuestion> getStudentQuestions() {
        return questions;
    }
}    
