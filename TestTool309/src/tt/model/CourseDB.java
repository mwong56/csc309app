package tt.model;

import java.util.ArrayList;

/**
 * Class to represent database of courses. This class was
 * created with intention as serving as the school's database of courses
 * available but eventually lost its purpose as we got further 
 * into development.
 * 
 * @author Dat Tran
 *
 */
public class CourseDB {
    /** List of courses */
    protected ArrayList<Course> courses;
    
    /**
     * Default constructor
     */
    public CourseDB() {
        courses = new ArrayList<Course>();
        
    }
    
    /**
     * Get courses
     * 
     * @return List of courses
     * 
     */
    public ArrayList<Course> getCourses() {
        return courses;
    }
}
