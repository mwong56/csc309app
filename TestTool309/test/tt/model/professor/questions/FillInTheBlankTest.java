package tt.model.professor.questions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Class QuestionTest is the companion testing class for Question. It implements
 * the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * </pre>
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class FillInTheBlankTest extends FillInTheBlankQuestion {

    public FillInTheBlankTest() {
        super("CPE101", 5, "3", "FB", "");
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */

    /**
     * Unit test the constructor by building one Question object. The other Question 
     * object constructor will be removed in a later update.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal Case      Proper init done
     *
     */
    @Test
    public void testQuestion() {
        FillInTheBlankQuestion q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "Hello?");
        assertEquals(q.className, "CPE 101");
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
    }
    
    /**
     * Unit test validate()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      ""
     *
     */
    @Test
    public void testValidate() {
        String noPrompt = "No question prompt!\n";
        String noBlanks = "No blanks!\n";
        String noAns = "No answer inside brackets for #0 blank!\n";
        String strayBracket = "Stray '[[' or ']]' found!\n";
        
        FillInTheBlankQuestion q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "Fill in the [[Blank]]");
        assertEquals(q.validate(), "");
        
        q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "[[Blank]]");
        assertEquals(q.validate(), noPrompt);
        
        q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "Fill in the");
        assertEquals(q.validate(), noBlanks);
        
        q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "Fill in the [[]]");
        assertEquals(q.validate(), noAns);
        
        q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "Fill in the [[sup]] [[Blank");
        assertEquals(q.validate(), strayBracket);
    }
    
    /**
     * Unit test getters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testGetters() {
        FillInTheBlankQuestion q = new FillInTheBlankQuestion("CPE 101", 3, "45", "MC", "Fill in the [[Blank]]");
        
        assertEquals(q.getCorrectAnswers(), q.correctAnswers);
    }
    
}
