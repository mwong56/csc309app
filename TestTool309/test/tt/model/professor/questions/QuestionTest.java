package tt.model.professor.questions;

import static org.junit.Assert.*;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import org.junit.Test;


/**
 * Class QuestionTest is the companion testing class for Question. It implements
 * the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * </pre>
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
@SuppressWarnings("serial")
public class QuestionTest extends Question {
    private static final long serialVersionUID = 1L;

    /**
     * Empty constructor, needed to placate the compiler, since parent Schedule
     * constructor takes two arguments.
     */
    public QuestionTest() {
        super("CPE101", 5, "1", "TF", "Sup");
    }

    /*-*
     * Individual unit testing methods for member methods
     */

    public QuestionTest(String classString,
            int difficulty, int time,
            String typeString, String prompt) {
        super(classString, difficulty, time, typeString,
                prompt);
    }

    /**
     * Unit test the constructor by building one Question object. The other Question 
     * object constructor will be removed in a later update.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal Case      Proper init done   
     *   2      Missing one      Proper init done
     *   3      Missing many     Proper init done
     *   4      null             Proper init done
     *
     */
    @Test
    public void testQuestion() {
        
        // New Question Constructor
        Question q = new Question("CPE 101", 3, "45", "MC", "Hello?");
        assertEquals(q.className, "CPE 101");
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        
        q = new Question(null, 3, "45", "MC", "Hello?");
        assertEquals(q.className, null);
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        
        q = new Question(null, 3, null, null, null);
        assertEquals(q.className, null);
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, null);
        assertEquals(q.type, null);
        assertEquals(q.prompt, null);
        
        q = new Question(null, -1, null, null, null);
        assertEquals(q.className, null);
        assertEquals(q.difficulty, -1);
        assertEquals(q.timeString, null);
        assertEquals(q.type, null);
        assertEquals(q.prompt, null);
        
        // Old Question Constructor
        q = new Question("CPE 101", 3, 45, "MC", "Hello?");
        assertEquals(q.className, "CPE 101");
        assertEquals(q.difficulty, 3);
        assertEquals(q.time, 45);
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        
        q = new Question(null, 3, 45, "MC", "Hello?");
        assertEquals(q.className, null);
        assertEquals(q.difficulty, 3);
        assertEquals(q.time, 45);
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        
        q = new Question(null, 3, -1, null, null);
        assertEquals(q.className, null);
        assertEquals(q.difficulty, 3);
        assertEquals(q.time, -1);
        assertEquals(q.type, null);
        assertEquals(q.prompt, null);
        
        q = new Question(null, -1, -1, null, null);
        assertEquals(q.className, null);
        assertEquals(q.difficulty, -1);
        assertEquals(q.time, -1);
        assertEquals(q.type, null);
        assertEquals(q.prompt, null);
    }
    
    /**
     * Unit test isNumeric()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      true   
     *   2      Empty            false
     *   3      Non-number       false
     *   4      Mixed input      false
     *
     */
    @Test
    public void testIsNumeric() {
        assertTrue(Question.isNumeric("3"));
        assertFalse(Question.isNumeric(""));
        assertFalse(Question.isNumeric("e"));
        assertFalse(Question.isNumeric("e3r"));
    }
    
    /**
     * Unit test validate()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      ""   
     *   2      No Course        No Course error
     *   3      Difficulty < 0   Difficulty error
     *   4      Difficulty > 5   Difficulty error
     *   5      No time          Time error
     *   6      Invalid time     Time error
     *   7      No type          Question Type error
     *   8      No Prompt        Prompt error
     *   9      No filled prompt Prompt error
     *
     */
    @Test
    public void testValidate() {
        String noClass = "No class selected!\n";
        String noDiff = "No difficulty selected!\n";
        String noValidTime = "No valid estimated time!\n";
        String noType = "No question type selected!\n";
        String noPrompt = "No question prompt!\n";
        
        Question q = new Question("CPE 101", 3, "45", "MC", "Hello?");
        assertEquals(q.validate(), "");
        
        q = new Question(null, 3, "45", "MC", "Hello?");
        assertEquals(q.validate(), noClass);
        
        q = new Question("CPE 101", -1, "45", "MC", "Hello?");
        assertEquals(q.validate(), noDiff);
        
        q = new Question("CPE 101", 6, "45", "MC", "Hello?");
        assertEquals(q.validate(), noDiff);
        
        q = new Question("CPE 101", 3, null, "MC", "Hello?");
        assertEquals(q.validate(), noValidTime);
        
        q = new Question("CPE 101", 3, "e3", "MC", "Hello?");
        assertEquals(q.validate(), noValidTime);
        
        q = new Question("CPE 101", 3, "45", null, "Hello?");
        assertEquals(q.validate(), noType);
        
        q = new Question("CPE 101", 3, "45", "MC", "");
        assertEquals(q.validate(), noPrompt);
        
        q = new Question("CPE 101", 3, "45", "MC", null);
        assertEquals(q.validate(), noPrompt);
    }
    
    /**
     * Unit test isGeneralInputValid()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      ""   
     *   2      No Course        No Course error
     *   3      Difficulty < 0   Difficulty error
     *   4      Difficulty > 5   Difficulty error
     *   5      No time          Time error
     *   6      Invalid time     Time error
     *   7      No type          Question Type error
     *   8      No Prompt        Prompt error
     *   9      No filled prompt Prompt error
     *
     */
    @Test
    public void testIsGeneralInputValid() {
        String noClass = "No class selected!\n";
        String noDiff = "No difficulty selected!\n";
        String noValidTime = "No valid estimated time!\n";
        String noType = "No question type selected!\n";
        String noPrompt = "No question prompt!\n";
        
        Question q = new Question("CPE 101", 3, "45", "MC", "Hello?");
        assertEquals(q.isGeneralInputValid(), "");
        
        q = new Question(null, 3, "45", "MC", "Hello?");
        assertEquals(q.isGeneralInputValid(), noClass);
        
        q = new Question("CPE 101", -1, "45", "MC", "Hello?");
        assertEquals(q.isGeneralInputValid(), noDiff);
        
        q = new Question("CPE 101", 6, "45", "MC", "Hello?");
        assertEquals(q.isGeneralInputValid(), noDiff);
        
        q = new Question("CPE 101", 3, null, "MC", "Hello?");
        assertEquals(q.isGeneralInputValid(), noValidTime);
        
        q = new Question("CPE 101", 3, "e3", "MC", "Hello?");
        assertEquals(q.isGeneralInputValid(), noValidTime);
        
        q = new Question("CPE 101", 3, "45", null, "Hello?");
        assertEquals(q.isGeneralInputValid(), noType);
        
        q = new Question("CPE 101", 3, "45", "MC", "");
        assertEquals(q.isGeneralInputValid(), noPrompt);
        
        q = new Question("CPE 101", 3, "45", "MC", null);
        assertEquals(q.isGeneralInputValid(), noPrompt);
    }
    
    /**
     * Unit test getters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testGetters() {
        Question q = new Question("CPE 101", 3, "45", "MC", "herp");
        assertEquals(q.getClassName(), q.className);
        assertEquals(q.classNameProperty().get(), q.className);
        assertEquals(q.difficultyProperty().get(), q.difficulty);
        assertEquals(q.getDifficulty(), String.valueOf(q.difficulty));
        assertEquals(q.getEstTime(), "-1");
        assertEquals(q.getType(), q.type);
        assertEquals(q.typeProperty().get(), q.type);
        assertEquals(q.getPrompt(), q.prompt);
        assertEquals(q.promptProperty().get(), q.prompt);
        assertEquals(q.getAnswer(), "");
        assertEquals(q.answerProperty().get(), q.answer);
        
        q = new Question(null, -1, null, null, null);
        assertNull(q.getClassName());
        assertNull(q.classNameProperty().get());
    }

    /**
     * Unit test setters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Sets given information
     *
     */
    @Test
    public void testSetters() {
        Question q = new Question("CPE 101", 3, "45", "MC", "");
        
        q.setClassName("CPE 212");
        q.setAnswer("der");
        q.setDifficulty(2);
        q.setPrompt("rawr");
        q.setTime("45");
        q.setType("MC");
        assertEquals(q.className, "CPE 212");
        assertEquals(q.answer, "der");
        assertEquals(q.prompt, "rawr");
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
    }
}
