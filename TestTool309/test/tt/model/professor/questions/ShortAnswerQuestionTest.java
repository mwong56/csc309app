package tt.model.professor.questions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Class QuestionTest is the companion testing class for Question. It implements
 * the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * </pre>
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class ShortAnswerQuestionTest extends ShortAnswerQuestion {
    
    public ShortAnswerQuestionTest() {
        super("CPE101", 5, "1", "TF", "Sup", "");
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */

    /**
     * Unit test the constructor by building one Question object. The other Question 
     * object constructor will be removed in a later update.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal Case      Proper init done   
     *   2      Missing one      Proper init done
     *   3      Missing many     Proper init done
     *   4      null             Proper init done
     *
     */
    @Test
    public void testQuestion() {
        ShortAnswerQuestion q = new ShortAnswerQuestion("CPE 101", 3, "45", "MC", "Hello?", "");
        assertEquals(q.className, "CPE 101");
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        assertEquals(q.answerNotes, "");
        
        q = new ShortAnswerQuestion("CPE 101", 3, "45", "MC", "Hello?", null);
        assertEquals(q.answerNotes, "");
    }
    
    /**
     * Unit test validate()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      ""
     *
     */
    @Test
    public void testValidate() {
        ShortAnswerQuestion q = new ShortAnswerQuestion("CPE 101", 3, "45", "MC", "Hello?", "");
        assertEquals(q.validate(), "");
    }
    
    /**
     * Unit test getters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testGetters() {
        ShortAnswerQuestion q = new ShortAnswerQuestion("CPE 101", 3, "45", "MC", "Hello?", "");
        
        assertEquals(q.getAnswerNotes(), q.answerNotes);
    }
}
