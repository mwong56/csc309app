package tt.model.professor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import org.junit.Test;

import tt.model.Course;

/**
 * Class ProfessorDBTest is the companion testing class for ProfessorDB. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get method.
 * 
 * Phase 3: Unit test authenticate method.
 * </pre>
 * 
 * @author Dat Tran
 */
public class ProfessorDBTest extends ProfessorDB {

    /**
     * Empty constructor, needed to placate the compiler, since parent
     * ProfessorDB constructor takes two arguments.
     */
    public ProfessorDBTest() {
        super();
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */

    /**
     * Unit test the constructor by building one ProfessorDB object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   null               proper init done    only case
     *   
     **/
    @Test
    public void testProfessorDB() {
        ProfessorDB db = new ProfessorDB();
        
        ArrayList<Course> courses = new ArrayList<Course>();
        courses.add(new Course("CPE", 308, 1, 69));
        courses.add(new Course("CPE", 308, 2, 70));
        courses.add(new Course("CPE", 309, 1, 71));
        
        ArrayList<Course> mammen = new ArrayList<Course>();
        mammen.add(new Course("CPE", 102, 1, 50));
        mammen.add(new Course("CPE", 102, 2, 51));
        mammen.add(new Course("CPE", 357, 1, 75));
        
        ArrayList<Professor> professors = new ArrayList<Professor>();
        professors.add(new Professor("Gene",
                "Fisher", "gfisher",
                "team1isthebest", 1, courses));
        professors.add(new Professor("Kurt",
                "Mammen", "kmammen",
                "ILOVEBEINGANASSHOLE", 2, mammen));
        professors.add(new Professor("easy", 
                "access", "a", "", 3, courses));
          
        for (int i = 0; i < db.professors.size(); i++) {
            assertEquals(db.professors.get(i).getFirstName(), 
                    professors.get(i).getFirstName());
            assertEquals(db.professors.get(i).getLastName(), 
                    professors.get(i).getLastName());
            assertEquals(db.professors.get(i).getUsername(), 
                    professors.get(i).getUsername());
            assertEquals(db.professors.get(i).getId(), 
                    professors.get(i).getId());
        }
    }
    
    
    /**
     * Unit test the authenticate method by calling authenticate with
     * data fields of an existent professor and with a non-existent professor.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   data fields of      true               existent professor
     *       existent professor
     *   2   data fields of      false              nonexistent professor
     *       non-existent professor
     **/
    @Test
    public void testAuthenticate() {
        /**
         * Set up
         */
        ProfessorDB db = new ProfessorDB();
        
        ArrayList<Course> courses = new ArrayList<Course>();
        courses.add(new Course("CPE", 308, 1, 69));
        courses.add(new Course("CPE", 308, 2, 70));
        courses.add(new Course("CPE", 309, 1, 71));
        
        ArrayList<Professor> professors = new ArrayList<Professor>();
        professors.add(new Professor("Gene",
                "Fisher", "gfisher",
                "team1isthebest", 0, courses));
        professors.add(new Professor("easy", 
                "access", "a", "", 1, courses));
        
        /**
         * Case 1
         */
        assertTrue(db.authenticate("gfisher", "team1isthebest"));
        
        /**
         * Case 2
         */
        assertFalse(db.authenticate(null, null));
    }
    
    /**
     * Unit test the getProfessor method by calling getProfessor
     * in a ProfessorDB with and without the professor .
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   Username of   Professor with username  existent professor
     *        existent professor
     *   2   Username of   NoSuchElementException   nonexistent professor
     *       non-existent professor
     **/
    @Test
    public void testGetProfessor() throws NoSuchElementException {
        /**
         * Set up
         */
        ProfessorDB db = new ProfessorDB();
        
        ArrayList<Course> courses = new ArrayList<Course>();
        courses.add(new Course("CPE", 308, 1, 69));
        courses.add(new Course("CPE", 308, 2, 70));
        courses.add(new Course("CPE", 309, 1, 71));
        
        ArrayList<Professor> professors = new ArrayList<Professor>();
        Professor test = new Professor("Gene",
                "Fisher", "gfisher",
                "team1isthebest", 0, courses);
        professors.add(test);
        professors.add(new Professor("easy", 
                "access", "a", "", 1, courses));
        
        /**
         * Case 1
         */
        assertEquals(db.getProfessor("gfisher").firstName, "Gene");
        assertEquals(db.getProfessor("gfisher").lastName, "Fisher");
        assertEquals(db.getProfessor("gfisher").username, "gfisher");
        assertEquals(db.getProfessor("gfisher").id, 1);
        
        /**
         * Case 2
         */
        try {
            db.getProfessor("blah");
        } catch (NoSuchElementException e) {
            assertTrue(true);
        }
    }
}
