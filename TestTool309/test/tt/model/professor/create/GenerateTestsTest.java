package tt.model.professor.create;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import tt.model.professor.questions.QuestionBank;

/**
 * Class GenerateTestsTest is the companion testing class for GenerateTest. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * 
 * Phase 3: Unit test add, saving the constructed results for 
 *          subsequent tests.
 *          
 * Phase 4: Unit test delete, deleting the items added in Phase 3.
 * </pre>
 * 
 * @author Terrence Li
 */
public class GenerateTestsTest extends GenerateTest{
    protected GenerateTest gt;
    protected QuestionBank qb;
    
    /**
     * Empty constructor, needed to placate the compiler, since parent GenerateTest
     * constructor takes two arguments.
     */
    protected GenerateTestsTest() {
        super();
    }
    
    @Test
    /**
     * Unit test the constructor by building two GenerateTest objects.  No further
     * constructor testing is necessary since only two GenerateTest objects are ever
     * constructed in the TestTool system.
     *                                                                    
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      null                                Null case
     *   2      "In Class"                          Non-null case
     *
     */
    public void testGenerateTest() {
        gt = new GenerateTest();
        qb = new QuestionBank();
        assertNotNull("should not be null", gt);
        TestSettings ts = new TestSettings(5, 3, 20.0, 10.0, 20.0, 10.0, 10.0, 10.0, 10.0, 10.0);
        gt = new GenerateTest(ts, 10, "CPE-101", "Final 1", qb);
        assertNotNull("should not be null", gt);
    }
}
