package tt.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tt.model.professor.Professor;

/**
 * Class CourseDBTest is the companion testing class for CourseDB. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * Phase 2: Unit test the getters.
 * 
 * </pre>
 * 
 * @author Dat Tran
 */
public class CourseDBTest extends CourseDB {

    /**
     * Empty constructor, needed to placate the compiler, since parent Professor
     * constructor takes five arguments.
     */
    public CourseDBTest() {
        super();
    }

    /*-*
     * Individual unit testing methods for constructors
     */

    /**
     * Unit test the constructor by building a CourseDB object
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output      Remarks
     * ====================================================================
     *   1      CourseDB        none        initialized courses field
     *          
     */
    @Test
    public void testCourseDB() {
        /**
         * Case 1
         */
        CourseDB db1 = new CourseDB();
        assertEquals(db1.courses.size(), 0);
    }
    
    /*-*
     * Individual unit testing methods for constructors
     */

    /**
     * Unit test the constructor by building a CourseDB object
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output                     Remarks
     * ====================================================================
     *   1      CourseDB    initialized courses field        initialized courses field
     *          
     */
    @Test
    public void testGetter() {
        /**
         * Case 1
         */
        CourseDB db1 = new CourseDB();
        assertEquals(db1.courses, db1.getCourses());
    }
}
