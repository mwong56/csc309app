package tt.model.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;

import tt.model.Course;
import tt.model.professor.Professor;
import tt.model.professor.ProfessorDB;

/**
 * Class ProfessorDBTest is the companion testing class for ProfessorDB. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get method.
 * 
 * Phase 3: Unit test authenticate method.
 * </pre>
 * 
 * @author Dat Tran
 */
public class StudentDBTest {

    /**
     * Empty constructor, needed to placate the compiler, since parent Schedule
     * constructor takes two arguments.
     */
    public StudentDBTest() {
        super();
    }
    
    /*-*
     * Individual unit testing methods for constructor
     */

    /**
     * Unit test the constructor by building one ProfessorDB object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   null               proper init done    only case
     *   
     **/
    @Test
    public void testStudentDB() {
        StudentDB db = new StudentDB();
        
        ArrayList<Student> students = new ArrayList<Student>();
        students.add(new Student("Dat", "Tran",
                "dattran", "abcde12345", 12345));
        students.add(new Student("easy", 
                "access", "a", "", 0));     
        for (int i = 0; i < db.students.size(); i++) {
            assertEquals(db.students.get(i).username, 
                    students.get(i).username);
        }
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */
    
    /**
     * Unit test the authenticate method by calling authenticate with
     * data fields of an existent student and with a non-existent student.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   data fields of      true               existent student
     *       existent student
     *   2   data fields of      false              nonexistent student
     *       non-existent student
     **/
    @Test
    public void testGetters() {
        /**
         * Set up
         */
        StudentDB db = new StudentDB();
        ArrayList<Student> students = new ArrayList<Student>();
        students.add(new Student("Dat", "Tran",
                "dattran", "abcde12345", 12345));
        students.add(new Student("easy", 
                "access", "a", "", 0));
        
        /**
         * Case 1
         */
        assertEquals(db.getStudents(), db.students);
        assertEquals(db.getStudent("dattran").username, "dattran");
        
        /**
         * Case 2
         */
        assertFalse(db.authenticate(null, null));
    }
    
    /**
     * Unit test the authenticate method by calling authenticate with
     * data fields of an existent student and with a non-existent student.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   data fields of      true               existent student
     *       existent student
     *   2   data fields of      false              nonexistent student
     *       non-existent student
     **/
    @Test
    public void testAuthenticate() {
        /**
         * Set up
         */
        StudentDB db = new StudentDB();
        
        ArrayList<Student> students = new ArrayList<Student>();
        students.add(new Student("Dat", "Tran",
                "dattran", "abcde12345", 12345));
        students.add(new Student("easy", 
                "access", "a", "", 0));
        
        /**
         * Case 1
         */
        assertTrue(db.authenticate("dattran", "abcde12345"));
        
        /**
         * Case 2
         */
        assertFalse(db.authenticate(null, null));
    }
}
