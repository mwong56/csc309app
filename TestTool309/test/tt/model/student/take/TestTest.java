package tt.model.student.take;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import tt.model.professor.Professor;
import tt.model.professor.questions.MultipleChoiceQuestion;
import tt.model.professor.questions.Question;

/****
 * 
 * @author Michael Wong
 *
 * Class TestTest is the companion testing class for class Test.
 * It implements the following module test plan:
 * 
 *     Phase 1: Unit test the constructor.
 *                                                                      
 *     Phase 2: Unit test the simple access method getType.
 *                                                                      
 *     Phase 3: Unit test the setType and saveSettings methods.
 *                                                                      
 *     Phase 4: Repeat phases 1 through 3.
 */

public class TestTest {
    
    /**
     * Unit test the constructor by building one test object. 
     *                                                                    
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      null                                Null case
     *
     */
    @Test
    public void testConstructor() {
        Collection<Question> questions = new ArrayList<Question>();
        questions.add(new Question("CPE 102", 3,
                5, "SA", "Test question 2"));
        questions.add(new Question("CPE 103", 1,
                4, "LA", "Test question 3"));
        questions.add(new Question("CPE 123", 3,
                3, "TF", "Test question 4"));
        
        tt.model.professor.create.Test sampleTest = new tt.model.professor.create.Test(questions, "N/A",
                "2/6/15", "Practice Midterm",
                "CSC 102", new Professor("test", "professor", "test professor", "asdf", 123),
                "Practice Exam");
        assertNotNull(sampleTest);
    }
    
    /**
     * Unit test get methods on the test.
     *                                                                    
     *  Test
     *  Case    Input                   Output          Remarks
     * ====================================================================
     *   1      sampleTest.getTitle().get() != null        Non-null case
     *
     *   2      sampleTest.getCourse().get() != null        Non-null case   
     *   
     *   3      sampleTest.getQuestionList().get() != null        Non-null case  
     *   
     *   4      sampleTest.getProfessor().get() != null        Non-null case
     *   
     *   5      sampleTest.getQuestionList().get() != null        Non-null case
     *   
     *   6      sampleTest.getQuestionNumber().get() != null        Non-null case
     */

    @Test
    public void testGets() {
        Collection<Question> questions = new ArrayList<Question>();
        questions.add(new Question("CPE 102", 3,
                5, "SA", "Test question 2"));
        questions.add(new Question("CPE 103", 1,
                4, "LA", "Test question 3"));
        questions.add(new Question("CPE 123", 3,
                3, "TF", "Test question 4"));
        
        tt.model.professor.create.Test sampleTest = new tt.model.professor.create.Test(questions, "N/A",
                "2/6/15", "Practice Midterm",
                "CSC 102", new Professor("test", "professor", "test professor", "asdf", 123),
                "Practice Exam");
        
        assertNotNull(sampleTest.getTitle().get());
        assertNotNull(sampleTest.getCourse().get());
        assertNotNull(sampleTest.getQuestionList());
        assertNotNull(sampleTest.getProfessor().get());
        assertNotNull(sampleTest.getQuestionList());
        assertNotNull(sampleTest.getQuestionNumber());
        
    }
    
    
    /**
     * Unit test setTitle by checking to see if the test tile changes after
     * setType is called.
     *
     *  Test
     *  Case    Input                Output                 Remarks
     * ====================================================================
     *   1      "PractMy Testice"                           Change test title
     */
    @Test
    public void testSets() {
        Collection<Question> questions = new ArrayList<Question>();
        questions.add(new Question("CPE 102", 3,
                5, "SA", "Test question 2"));
        questions.add(new Question("CPE 103", 1,
                4, "LA", "Test question 3"));
        questions.add(new Question("CPE 123", 3,
                3, "TF", "Test question 4"));
        
        tt.model.professor.create.Test sampleTest = new tt.model.professor.create.Test(questions, "N/A",
                "2/6/15", "Practice Midterm",
                "CSC 102", new Professor("test", "professor", "test professor", "asdf", 123),
                "Practice Exam");
        sampleTest.setTitle("My Test");
        assertEquals(sampleTest.getTitle().get(), "My Test");
    }
}
