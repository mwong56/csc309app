package tt.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tt.model.professor.Professor;

/**
 * Class CourseTest is the companion testing class for Course. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * Phase 2: Unit test the getters.
 * Phase 3: Unit test the overrided toString method.
 * 
 * </pre>
 * 
 * @author Dat Tran
 */
public class CourseTest extends Course {

    /**
     * Empty constructor, needed to placate the compiler, since parent Course
     * constructor takes four arguments.
     */
    public CourseTest() {
        super(null, 0, 0, 0);
    }

    /*-*
     * Individual unit testing methods for constructors
     */

    /**
     * Unit test the constructor by building a Course object
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   course.fields  same non-null value    non-null value
     *          == non-null
     *   2   course.fields  null                   null
     *          == null
     */
    @Test
    public void testCourse() {
        /**
         * Case 1
         */
        Course course1 = new Course("CPE", 309, 1, 1);
        assertEquals(course1.courseAbbr, "CPE");
        assertEquals(course1.number, 309);
        assertEquals(course1.section, 1);
        assertEquals(course1.id, 1);
        
        /**
         * Case 2
         */
        Course course2 = new Course(null, 0, 0, 0);
        assertEquals(course2.courseAbbr, null);
        assertEquals(course2.number, 0);
        assertEquals(course2.section, 0);
        assertEquals(course2.id, 0);
    }
    
    /*-*
     * Individual unit testing methods for getters
     */
    
    /**
     * Unit test the getters by building a Course object
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   course.fields  same non-null value    non-null value
     *          == non-null
     *   2   course.fields  null                   null
     *          == null
     */
    @Test
    public void testGetters() {
        /**
         * Case 1
         */
        Course course1 = new Course("CPE", 309, 1, 1);
        assertEquals(course1.getCourseAbbr(), "CPE");
        assertEquals(course1.getNumber(), 309);
        assertEquals(course1.getSection(), 1);
        assertEquals(course1.getId(), 1);
        
        /**
         * Case 2
         */
        Course course2 = new Course(null, 0, 0, 0);
        assertEquals(course2.getCourseAbbr(), null);
        assertEquals(course2.getNumber(), 0);
        assertEquals(course2.getSection(), 0);
        assertEquals(course2.getId(), 0);
    }
    
    /*-*
     * Individual unit testing methods for other methods
     */
    
    /**
     * Unit test the toString method by building a Course object
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output                      Remarks
     * ====================================================================
     *   1   course.fields  courseAbbr number-section (ID)   non-null value
     *          == non-null
     *   2   course.fields  null 0-0 (0)                   null
     *          == null
     */
    @Test
    public void testToString() {
        /**
         * Case 1
         */
        Course course1 = new Course("CPE", 309, 1, 1);
        assertEquals(course1.toString(), "CPE 309-1 (1)");
        /**
         * Case 2
         */
        Course course2 = new Course(null, 0, 0, 0);
        assertEquals(course2.toString(), null + " 0-0 (0)");
    }
}
